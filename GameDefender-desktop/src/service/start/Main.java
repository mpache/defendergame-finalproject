/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package service.start;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import persistence.DataAccesObjectJSON;
import service.data.AudioManager;
import service.data.IOperations;
import service.data.SettingManager;
import application.concretion.GameScreenController;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

/**
 * Main.java
 * 
 * @version Apr 19, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class Main implements IOperations {

    /**
     * Initialized application.
     * 
     * @param args
     *            is not used.
     */
    public static void main(String[] args) {
	// Configure application.
	LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
	cfg.resizable = true;
	cfg.title = "Slayer";
	cfg.useGL20 = false;
	cfg.width = 854;
	cfg.height = 480;
	// cfg.width = 1280;
	// cfg.height = 720;

	// Initialized application.
	new LwjglApplication(new GameScreenController(), cfg);

	// Set the settings
	SettingManager.myOperation = new Main();
	SettingManager.DAO = DataAccesObjectJSON.getInstance();
	SettingManager.isMobile = false;
	AudioManager.setFilesAudio();
    }

    @Override
    public void openUrl(String url) {
	try {
	    Desktop.getDesktop().browse(new URI(url));
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (URISyntaxException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }
}
