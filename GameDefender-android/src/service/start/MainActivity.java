/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package service.start;

import persistence.DataAccesObjectJSON;
import service.data.AudioManager;
import service.data.IOperations;
import service.data.SettingManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import application.concretion.GameScreenController;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

/**
 * MainActivity.java
 * 
 * @version Apr 19, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class MainActivity extends AndroidApplication implements IOperations {
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);

	AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
	cfg.useGL20 = false;

	initialize(new GameScreenController(), cfg);

	// Set the settings
	SettingManager.myOperation = this;
	SettingManager.DAO = DataAccesObjectJSON.getInstance();
	SettingManager.isMobile = true;
	AudioManager.setFilesAudio();
    }

    @Override
    public void openUrl(String url) {
	Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(myIntent);
    }
}