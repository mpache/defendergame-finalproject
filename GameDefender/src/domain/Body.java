/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package domain;

import com.badlogic.gdx.math.Vector2;

/**
 * Body.java
 * <p>
 * Class with fields and methods of a body without movement.
 * </p>
 * 
 * @version Apr 19, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public abstract class Body {

    /* Fields */
    public final Vector2 position = new Vector2();
    protected float width;
    protected float height;

    /**
     * Constructor of Body class.
     * 
     * @param posx
     *            initial position of X axis
     * @param posy
     *            initial position of Y axis
     * @param width
     *            the width of the body
     * @param height
     *            the height of the body
     */
    protected Body(float posx, float posy, float width, float height) {
	this.width = width;
	this.height = height;
	this.position.set(posx, posy);
    }

    // GETTERS & STTERS
    /**
     * Get width
     * 
     * @return the width of this body.
     */
    public float getWidth() {
	return this.width;
    }

    /**
     * Get height
     * 
     * @return the height of this body.
     */
    public float getHeight() {
	return this.height;
    }

    /**
     * Get the right x axis of this body.
     * 
     * @return the right x axis of this body.
     */
    public float getRightX() {
	return this.position.x + this.width;
    }

    /**
     * Get top y axis of this body.
     * 
     * @return top y axis of this body.
     */
    public float getTopY() {
	return this.position.y + this.height;
    }

    /**
     * Get middle x axis of this body.
     * 
     * @return middle x axis of this body.
     */
    public float getMiddleX() {
	return position.x + width / 2;
    }

    /**
     * Get middle y axis of this body
     * 
     * @return middle y axis of this body.
     */
    public float getMiddleY() {
	return position.y + height / 2;
    }
}
