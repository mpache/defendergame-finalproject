/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package domain.heros;

import service.data.StateManager.HeroState;
import domain.Attack;

/**
 * Hero1.java
 * 
 * @version Apr 19, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class HeroWarrior extends Hero {

    /* CONSTANTS */
    // Air Attacks
    private static final int AIR_ATTACK_DAMAGE = 5;
    // Ground Attacks
    private static final int GR_ATTACK_DAMAGE = 5;

    /**
     * HeroWarrior constructor.
     * 
     * @param posx
     *            is the x position of hero.
     * @param posy
     *            is the y position of hero.
     * @param width
     *            is the width of hero.
     * @param widthCollision
     *            is the width collision of hero.
     * @param height
     *            is the height of hero.
     * @param heightCollision
     *            is the height collision of hero.
     * @param life
     *            is the life of hero.
     * @param magic
     *            is the magic of hero.
     * @param creation
     *            is the creation of hero.
     * @param sp1
     *            is the magical spend of special 1.
     * @param sp2
     *            is the magical spend of special 2.
     * @param sp3
     *            is the magical spend of special 3.
     */
    public HeroWarrior(float posx, float posy, float width,
	    float widthCollision, float height, float heightCollision,
	    int life, int magic, int creation, int sp1, int sp2, int sp3) {
	super(posx, posy, width, widthCollision, height, heightCollision, life,
		magic, creation, sp1, sp2, sp3);
    }

    @Override
    public void atack() {
	if (!isDead()) {
	    if (!isAttacking()) {
		this.stateTimeAttack = 0;
		if (state == HeroState.AIR) {
		    this.state = HeroState.AIR_ATTACK;
		    this.durationAttack = 1f;
		    // Create and configure attack.
		    attack = new Attack(position.x, position.y,
			    getWidth() + 70, getHeight() + 20);
		    attack.setSettingsAttack(getWidth(), getHeight(), 1f,
			    isFaceRight(), false, true, true, 0,
			    AIR_ATTACK_DAMAGE);
		    attack.setState(state, null);
		} else if (!isReceiveDamage() && state != HeroState.AIR) {
		    this.stopMoveX();
		    this.state = HeroState.GR_ATTACK_STANCE;
		    this.durationAttack = 0.8f;
		    // Create and configure attack.
		    attack = new Attack(position.x, position.y,
			    getWidth() + 50, getHeight() + 8);
		    attack.setSettingsAttack(0, 0, 0.8f, isFaceRight(), false,
			    true, false, 0, GR_ATTACK_DAMAGE);
		    attack.setState(state, null);
		}
	    }
	}
    }

    @Override
    public void specialAttack1() {
	// TODO Auto-generated method stub

    }

    @Override
    public void specialAttack2() {
	// TODO Auto-generated method stub

    }

    @Override
    public void specialAttack3() {
	// TODO Auto-generated method stub

    }

    @Override
    public void addProyectile() {
	// TODO Auto-generated method stub

    }

    @Override
    public void fall() {
	if (!isAttacking()) {
	    super.fall();
	}
    }
}
