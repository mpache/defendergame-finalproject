/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package domain.heros;

import service.data.StateManager.HeroState;
import domain.Attack;

/**
 * HeroMage.java
 * 
 * @version May 26, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class HeroMage extends Hero {

    /* CONSTANTS */
    // Air Attacks.
    private static final int AIR_ATTACK_DAMAGE = 5;
    private static final int AIR_SP1_DAMAGE = 20;
    private static final int AIR_SP2_DAMAGE = 50;
    // Ground Attacks.
    private static final int GR_ATTACK_DAMAGE = 2;
    private static final int GR_SP1_DAMAGE = 20;
    private static final int GR_SP2_DAMAGE = 50;
    private static final int GR_SP3_DAMAGE = 100;
    // DURATION PROJECTILES
    private static final float AIR_SP1_PROJECTILE_DURATION = 2f;
    private static final float AIR_SP2_PROJECTILE_DURATION = 2f;
    private static final float GR_ATTACK_PROJECTILE_DURATION = 0.8f;
    private static final float GR_SP1_PROJECTILE_DURATION = 5f;
    private static final float GR_SP2_PROJECTILE_DURATION = 5f;

    /**
     * HeroMage constructor.
     * 
     * @param posx
     *            is the x position of hero.
     * @param posy
     *            is the y position of hero.
     * @param width
     *            is the width of hero.
     * @param widthCollision
     *            is the width collision of hero.
     * @param height
     *            is the height of hero.
     * @param heightCollision
     *            is the height collision of hero.
     * @param life
     *            is the life of hero.
     * @param magic
     *            is the magic of hero.
     * @param creation
     *            is the creation of hero.
     * @param sp1
     *            is the magical spend of special 1.
     * @param sp2
     *            is the magical spend of special 2.
     * @param sp3
     *            is the magical spend of special 3.
     */
    public HeroMage(float posx, float posy, float width, float widthCollision,
	    float height, float heightCollision, int life, int magic,
	    int creation, int sp1, int sp2, int sp3) {
	super(posx, posy, width, widthCollision, height, heightCollision, life,
		magic, creation, sp1, sp2, sp3);
    }

    @Override
    public void atack() {
	if (!isDead()) {
	    if (!isAttacking()) {
		this.stateTimeAttack = 0;
		if (state == HeroState.AIR) {
		    this.state = HeroState.AIR_ATTACK;
		    this.durationAttack = 1f;
		    // Create and configure attack.
		    attack = new Attack(position.x, position.y,
			    getWidth() + 15, getHeight() + 10);
		    attack.setSettingsAttack(getWidth(), getHeight(), 1f,
			    isFaceRight(), false, true, true, 0,
			    AIR_ATTACK_DAMAGE);
		    attack.setState(state, null);
		} else if (!isReceiveDamage() && state != HeroState.AIR) {
		    this.stopMoveX();
		    this.state = HeroState.GR_ATTACK_STANCE;
		    this.durationAttack = 0.7f;
		    // Create and configure attack.
		    attack = new Attack(position.x + 10, position.y + 20, 20,
			    20);
		    attack.setSettingsAttack(60, 60,
			    GR_ATTACK_PROJECTILE_DURATION, isFaceRight(), true,
			    true, false, 10, GR_ATTACK_DAMAGE);
		    attack.setState(state, null);
		}
	    }
	}
    }

    @Override
    public void specialAttack1() {
	if (!isDead()) {
	    if (!isAttacking()) {
		this.stateTimeAttack = 0;
		if (state == HeroState.AIR) {
		    this.state = HeroState.AIR_SP1;
		    this.durationAttack = 1f;
		    // Create and configure attack.
		    int x = isFaceRight() ? 50 : -50;
		    attack = new Attack(position.x + x, position.y + 20, 50, 50);
		    attack.setSettingsAttack(150, 150,
			    AIR_SP1_PROJECTILE_DURATION, isFaceRight(), true,
			    true, false, 15, AIR_SP1_DAMAGE);
		    attack.setState(state, null);
		} else if (!isReceiveDamage()) {
		    this.stopMoveX();
		    this.state = HeroState.GR_SP1;
		    this.durationAttack = 1.52f;
		    // Create and configure attack.
		    int x = isFaceRight() ? 100 : -100;
		    attack = new Attack(position.x + x, position.y + 1000, 20,
			    20);
		    float vel = attack.calculateVelocity(0, 2f, 20);
		    attack.setSettingsAttack(60, 60,
			    GR_SP1_PROJECTILE_DURATION, isFaceRight(), true,
			    false, true, vel, GR_SP1_DAMAGE);
		    attack.setState(state, null);
		}
	    }
	}
    }

    @Override
    public void specialAttack2() {
	if (!isDead()) {
	    if (!isAttacking()) {
		this.stateTimeAttack = 0;
		if (state == HeroState.AIR) {
		    this.stopMoveX();
		    this.stopMoveY();
		    this.state = HeroState.AIR_SP2;
		    this.durationAttack = 2.6f;
		    // Create and configure attack.
		    attack = new Attack(position.x - 70, position.y - 70, 180,
			    180);
		    attack.setSettingsAttack(180, 180,
			    AIR_SP2_PROJECTILE_DURATION, isFaceRight(), true,
			    false, false, 0f, AIR_SP2_DAMAGE);
		    attack.setState(state, null);
		    attack.setTimeToAdd(0.6f);
		} else if (!isReceiveDamage()) {
		    this.stopMoveX();
		    this.state = HeroState.GR_SP2;
		    this.durationAttack = 1.28f;
		    // Create and configure attack.
		    int x = isFaceRight() ? 0 : -120;
		    attack = new Attack(position.x + x, position.y - 30, 150,
			    150);
		    attack.setSettingsAttack(150, 150,
			    GR_SP2_PROJECTILE_DURATION, isFaceRight(), true,
			    true, false, 10f, GR_SP2_DAMAGE);
		    attack.setState(state, null);
		    attack.setTimeToAdd(1);
		}
	    }
	}
    }

    @Override
    public void specialAttack3() {
	if (!isDead()) {
	    if (!isAttacking() && !isReceiveDamage()) {
		this.stateTimeAttack = 0;
		stopMoveY();
		this.stopMoveX();
		this.state = HeroState.GR_SP3;
		this.durationAttack = 3.24f;
		// Create and configure attack.
		attack = new Attack(position.x, position.y, getWidth() + 30,
			getHeight() - 20);
		attack.setSettingsAttack(0, 0, 3.24f, isFaceRight(), false,
			true, false, 0f, GR_SP3_DAMAGE);
		attack.setState(state, null);
	    }
	}
    }

    @Override
    public void addProyectile() {
	world.addProyectile(attack);
    }

    @Override
    public void fall() {
	if (!isAttacking()) {
	    super.fall();
	}
    }

    @Override
    public void stopMoveX() {
	if (state == HeroState.GR_SP3) {
	    float vel = stateTimeAttack > 1 && stateTimeAttack < 2.5 ? this
		    .calculateVelocity(0, stateTimeAttack, 180) : 0;
	    vel = isFaceRight() ? vel : -vel;
	    if (stateTimeAttack >= 2 && stateTimeAttack <= 2.5)
		vel = -vel;
	    moveX(vel);
	} else {
	    super.stopMoveX();
	}
    }
}
