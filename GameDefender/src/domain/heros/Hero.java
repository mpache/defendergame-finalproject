/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package domain.heros;

import service.data.StateManager.HeroState;
import domain.Attack;
import domain.DynamicBody;
import domain.World;

/**
 * Hero.java
 * <p>
 * Hero class that has the movements of a generic Hero.
 * </p>
 * 
 * @version Apr 19, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public abstract class Hero extends DynamicBody {

    // Associations
    protected World world;

    // Constant of Movement
    public static final float WALK_VEL = 25f;
    public static final float JUMP_VEL = 50f;
    // Time in seconds of hero immunity
    public static final long IMMUNITY = 1;
    public static final long WIN_TIME = 1;

    // Push Fields
    public static final float PUSH_X = 30f;
    public static final float PUSH_Y = 30f;

    // Time to add creation
    private static final float TIME_TO_ADD_CREATION = 0f;

    // State fields
    public final int initialLife;
    protected int life;
    public final int initialMagic;
    protected int magic;
    public final int initialCreation;
    protected int creation;
    protected HeroState state;

    // Auxiliary fields
    private boolean isFaceRight;
    private boolean bodyBlocked;
    private boolean canJump;
    // MAGIC
    public static int MAGIC_SPEND_SP1;
    public static int MAGIC_SPEND_SP2;
    public static int MAGIC_SPEND_SP3;
    // CREATION
    private float timePasedToAddCreation;
    // Collision width
    protected float widthCollision;
    protected float heightCollision;

    /**
     * Hero constructor.
     * 
     * @param posx
     *            is the x position of hero.
     * @param posy
     *            is the y position of hero.
     * @param width
     *            is the width of hero.
     * @param widthCollision
     *            is the width collision of hero.
     * @param height
     *            is the height of hero.
     * @param heightCollision
     *            is the height collision of hero.
     * @param life
     *            is the life of hero.
     * @param magic
     *            is the magic of hero.
     * @param creation
     *            is the creation of hero.
     * @param sp1
     *            is the magical spend of special 1.
     * @param sp2
     *            is the magical spend of special 2.
     * @param sp3
     *            is the magical spend of special 3.
     */
    protected Hero(float posx, float posy, float width, float widthCollision,
	    float height, float heightCollision, int life, int magic,
	    int creation, int sp1, int sp2, int sp3) {
	super(posx, posy, width, height);
	this.widthCollision = widthCollision;
	this.heightCollision = heightCollision;
	this.initialLife = life;
	this.life = life;
	this.initialMagic = magic;
	this.magic = magic;
	this.initialCreation = creation;
	this.creation = creation;
	this.isFaceRight = true;
	this.state = HeroState.INACTIVE;
	this.bodyBlocked = false;
	MAGIC_SPEND_SP1 = sp1;
	MAGIC_SPEND_SP2 = sp2;
	MAGIC_SPEND_SP3 = sp3;
    }

    /**
     * Move hero to left
     * 
     * @param vel
     *            is the velocity to move hero.
     */
    public void moveLeft(final float vel) {
	moveXaxis(-vel);
    }

    /**
     * Move hero to right.
     * 
     * @param vel
     *            is the velocity to right.
     */
    public void moveRight(final float vel) {
	moveXaxis(vel);
    }

    private void moveXaxis(final float vel) {
	if (!this.bodyBlocked && !isDead() && !isAttacking()) {
	    // Move character.
	    this.initVelocity.y = this.calculateVelocity(initVelocity.y, time,
		    getGravity());
	    this.moveX(vel);
	    this.bodyBlocked = true;
	    this.isFaceRight = vel > 0 ? true : false;
	    this.state = isMovingY ? HeroState.AIR : HeroState.WALK;
	}
    }

    @Override
    public void stopMoveX() {
	if (isMovingX) {
	    super.stopMoveX();
	    this.bodyBlocked = false;
	    if (!this.isMovingY) {
		this.state = !isDead() ? HeroState.INACTIVE : HeroState.DEAD;
		this.state = !isDead() && state == HeroState.LOSE ? state
			: HeroState.INACTIVE;
	    }
	}
    }

    /**
     * Push hero.
     * 
     * @param vely
     *            is the velocity to push hero in y axis.
     * @param velx
     *            is the velocity to push hero in x axis.
     */
    public void push(final float vely, final float velx) {
	if (isFaceRight() && velx > 0)
	    this.state = HeroState.BEHIND_DAMAGE;
	else if (isFaceRight && velx < 0)
	    this.state = HeroState.FRONT_DAMAGE;
	else if (!isFaceRight && velx > 0)
	    this.state = HeroState.FRONT_DAMAGE;
	else
	    this.state = HeroState.BEHIND_DAMAGE;
	bodyBlocked = true;
	canJump = false;
	this.moveX(velx);
	this.moveY(vely);
    }

    /**
     * Jump hero.
     * 
     * @param vel
     *            is the velocity to jump hero.
     */
    public void jump(final float vel) {
	if (this.canJump && !isAttacking()) {
	    this.moveY(vel);
	    this.canJump = false;
	    this.state = HeroState.AIR;
	    if (initVelocity.x != 0) {
		this.bodyBlocked = true;
	    }
	}
    }

    /**
     * fall hero.
     */
    public void fall() {
	if (this.canJump && state != HeroState.WIN) {
	    moveY(0);
	    this.canJump = false;
	    this.bodyBlocked = false;
	    this.initVelocity.x = 0;
	    this.state = HeroState.AIR;
	}
    }

    @Override
    public void stopMoveY() {
	if (isMovingY) {
	    super.stopMoveY();
	    this.canJump = true;
	    this.bodyBlocked = false;
	    this.state = state == HeroState.LOSE ? state : HeroState.INACTIVE;
	}
    }

    /**
     * Check if hero is receive damage.
     * 
     * @return true if hero is receive damage, false otherwise.
     */
    public boolean isReceiveDamage() {
	if (state == HeroState.BEHIND_DAMAGE || state == HeroState.FRONT_DAMAGE) {
	    return true;
	}
	return false;
    }

    /**
     * Check if hero is dead.
     * 
     * @return true if hero is dead.
     */
    public boolean isDead() {
	if (this.life <= 0) {
	    this.state = HeroState.DEAD;
	    if (!isMovingY) {
		stopMoveY();
	    }
	    canJump = false;
	    return true;
	}
	return false;
    }

    /**
     * Check if hero is lock to right.
     * 
     * @return true if hero is lock to right, false otherwise.
     */
    public boolean isFaceRight() {
	return this.isFaceRight;
    }

    /**
     * Get state of hero.
     * 
     * @return the actual state of hero.
     */
    public HeroState getState() {
	return this.state;
    }

    /**
     * Get life of hero.
     * 
     * @return the life of hero.
     */
    public int getLife() {
	if (!isDead())
	    return this.life;
	return 0;
    }

    /**
     * Add life of hero.
     * 
     * @param life
     *            is the life to be add.
     */
    public void addLife(int life) {
	if (!isDead())
	    this.life = this.life + life >= this.initialLife ? this.initialLife
		    : this.life + life;
    }

    /**
     * Remove life.
     * 
     * @param life
     *            is the life to be remove.
     */
    public void removeLife(int life) {
	if (!isDead())
	    this.life = this.life - life <= 0 ? 0 : this.life - life;
    }

    /**
     * Get magic.
     * 
     * @return the magic of hero.
     */
    public int getMagic() {
	return this.magic;
    }

    /**
     * Add magic to hero.
     * 
     * @param magic
     *            is the magic to add.
     */
    public void addMagic(int magic) {
	if (!isDead())
	    this.magic = this.magic + magic > this.initialMagic ? this.initialMagic
		    : this.magic + magic;
    }

    /**
     * Remove magic
     * 
     * @param magicToRemove
     *            is the magic to remove to hero.
     */
    public void removeMagic(int magicToRemove) {
	if (!isDead() && !isAttacking()) {
	    this.magic = this.magic - magicToRemove <= 0 ? 0 : this.magic
		    - magicToRemove;
	}
    }

    /**
     * Get creation.
     * 
     * @return the creation of hero.
     */
    public int getCreation() {
	return this.creation;
    }

    /**
     * Add creation to hero.
     * 
     * @param creation
     *            is the creation to add.
     */
    public void addCreation(int creation, float delta) {
	timePasedToAddCreation += delta;
	if (!isDead())
	    if (timePasedToAddCreation >= TIME_TO_ADD_CREATION) {
		this.creation = this.creation + creation > this.initialCreation ? this.initialCreation
			: this.creation + creation;
		timePasedToAddCreation = 0;
	    }
    }

    /**
     * Remove creation
     * 
     * @param creationToRemove
     *            is the creation to remove to hero.
     */
    public void removeCreation(int creationToRemove) {
	if (!isDead()) {
	    this.creation = this.creation - creationToRemove <= 0 ? 0
		    : this.creation - creationToRemove;
	}
    }

    /**
     * Normal attack of hero.
     */
    public abstract void atack();

    /**
     * Special 1 attack of hero.
     */
    public abstract void specialAttack1();

    /**
     * Special 2 attack of hero.
     */
    public abstract void specialAttack2();

    /**
     * Special 3 attack of hero.
     */
    public abstract void specialAttack3();

    /**
     * Add proyectile if attack using it.
     */
    public abstract void addProyectile();

    // CONTROL ATTACKS.
    protected Attack attack;
    protected float stateTimeAttack;
    protected float durationAttack;
    protected boolean isAttackingWithProyectile;

    /**
     * Get attack.
     * 
     * @return the attack.
     */
    public Attack getAttack() {
	return attack;
    }

    /**
     * Check if animation attack is complete.
     * 
     * @param delta
     *            is the time passed.
     */
    public void checkAnimationAttack(float delta) {
	stateTimeAttack += delta;
	if (attack.isProjectile() && stateTimeAttack >= attack.getTimeToAdd()) {
	    addProyectile();
	}
	if (stateTimeAttack >= durationAttack) {
	    this.state = HeroState.INACTIVE;

	}
    }

    /**
     * Check if hero is attacking.
     * 
     * @return true if hero is attacking, false otherwise.
     */
    public boolean isAttacking() {
	if (state == HeroState.GR_ATTACK_STANCE
		|| state == HeroState.AIR_ATTACK || state == HeroState.GR_SP1
		|| state == HeroState.AIR_SP1 || state == HeroState.GR_SP2
		|| state == HeroState.AIR_SP2 || state == HeroState.GR_SP3
		|| state == HeroState.AIR_SP3) {
	    return true;
	}
	return false;
    }

    /**
     * Check if hero is attacking with proyectiles.
     * 
     * @return true if hero is attacking with proyectiles, false otherwise.
     */
    public boolean isAttackingWithProyectile() {
	return attack.isProjectile();
    }

    /**
     * Set world associated with this hero.
     * 
     * @param newWorld
     *            is the new world to add to this hero.
     * @return true if world has be changed, false otherwise.
     */
    public boolean setWorld(World newWorld) {
	if (world == newWorld)
	    return false;

	World oldWorld = this.world;
	world = newWorld;
	if (oldWorld != null) {
	    oldWorld.setAlly(null);
	}
	if (world != null) {
	    world.setHero(this);
	}
	return true;
    }

    /**
     * Get actual world associated to this hero.
     * 
     * @return the actual world associated to this hero.
     */
    public World getSystemWorld() {
	return world;
    }

    private boolean win;
    private float timeToWin;

    /**
     * Check if hero has win game.
     * 
     * @param delta
     *            is the time passed.
     */
    public void win(final float delta) {
	if (win) {
	    timeToWin += delta;
	    if (timeToWin >= WIN_TIME) {
		this.state = HeroState.WIN;
	    }
	}
    }

    /**
     * Set win.
     * 
     * @param win
     *            indicate if hero has win game or not.
     */
    public void setWin(boolean win) {
	this.win = win;
	timeToWin = 0;
    }

    private boolean lose;

    /**
     * If hero has lose game, change your state.
     */
    public void lose() {
	if (lose) {
	    this.state = HeroState.LOSE;
	}
    }

    /**
     * Set to lose.
     * 
     * @param lose
     *            indicate if hero has lose game or not.
     */
    public void setLose(boolean lose) {
	this.lose = lose;
    }

}
