/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package domain.items;

import service.data.TypeManager.ItemType;
import domain.DynamicBody;
import domain.heros.Hero;

/**
 * Item.java
 * 
 * @version Apr 19, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public abstract class Item extends DynamicBody {

    // jumps to stop
    private static final int MAX_JUMPS = 3;

    private float radius;
    private float timeLife;
    protected ItemType type;

    // Auxiliary Fields
    private boolean canJump;
    private int nJumps;
    private float timeToRemove;

    /**
     * Item constructor.
     * 
     * @param posx
     *            is the x position of item.
     * @param posy
     *            is the y position of item.
     * @param radius
     *            is the radius of item.
     * @param timeLife
     *            is the timeLife of item.
     */
    protected Item(float posx, float posy, float radius, float timeLife) {
	super(posx, posy, radius * 2, radius * 2);
	this.radius = radius;
	this.timeLife = timeLife;
	this.canJump = false;
	this.nJumps = 0;
	this.timeToRemove = 0;
    }

    /**
     * push when item collides with wall in X axis
     */
    public void bounceX() {
	this.isMovingX = false;
	moveX(-1 * velocity.x / 2);
	this.initPosition.x = position.x;
	if (velocity.y < 0) {
	    velocity.y = 0;
	    initVelocity.y = 0;
	}
    }

    @Override
    public void stopMoveX() {
	if (isMovingX) {
	    super.stopMoveX();
	} else {
	    this.isMovingX = false;
	    moveX(velocity.x / 2);
	    this.initPosition.x = position.x;
	}
    };

    /**
     * Jump item.
     * 
     * @param vel
     *            is the velocity to jump item.
     */
    public void jump(final float vel) {
	if (this.canJump) {
	    this.moveY(vel);
	    this.canJump = false;
	}
    }

    /**
     * fall item.
     */
    public void fall() {
	if (this.canJump) {
	    this.moveY(0);
	    this.canJump = false;
	    this.initVelocity.x = 0;
	}
    }

    @Override
    public void stopMoveY() {
	if (isMovingY) {
	    if (nJumps < MAX_JUMPS) {
		this.canJump = true;
		jump(Math.abs(velocity.y / 2));
		nJumps++;
		isMovingX = false;
	    } else {
		super.stopMoveY();
	    }
	}
    }

    /**
     * Apply the effect to the hero
     * 
     * @param hero
     *            is the hero to add effect.
     */
    public abstract void applyEffect(Hero hero);

    /**
     * Get radius of this item.
     * 
     * @return radius of this item.
     */
    public float getRadius() {
	return this.radius;
    }

    /**
     * Check if item is done for remove.
     * 
     * @param delta
     *            is the time passed.
     * 
     * @return true if item is done for remove.
     */
    public boolean isDoneForRemove(float delta) {
	timeToRemove += delta;
	if (timeToRemove > timeLife) {
	    return true;
	}
	return false;
    }

    /**
     * Get type of item.
     * 
     * @return the type of item.
     */
    public ItemType getType() {
	return type;
    }

}
