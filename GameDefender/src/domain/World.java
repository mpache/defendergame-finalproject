/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package domain;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;

import domain.allies.Ally;
import domain.enemies.Enemy;
import domain.heros.Hero;
import domain.items.Item;
import domain.utils.Utils;
import domain.walls.Ground;
import domain.walls.TemporalWall;
import domain.walls.Tower;

/**
 * SystemWorld.java
 * <p>
 * Class where interact elements of the world game
 * </p>
 * 
 * @version Apr 19, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class World {

    /* Fields */
    // Class Fields
    public final float WIDTH;

    // Assosiations
    private Hero hero;
    private Tower tower;
    private Set<Ground> grounds;
    private Set<Enemy> enemies;
    private float floor;
    private Ally ally;
    private Set<Item> items;
    private Set<Attack> proyectiles;
    private Set<TemporalWall> tempWalls;

    // Auxiliary fields
    private boolean heroCollisionEnemy = false;
    private float elapsedCollitionTime;

    /**
     * World constructor.
     * 
     * @param hero
     *            is a hero.
     * @param tower
     *            is a tower.
     * @param ally
     *            is a ally.
     * @param floor
     *            is the floor of stage.
     * @param width
     *            is the width of stage.
     */
    public World(Hero hero, Tower tower, Ally ally, float floor, float width) {
	this.setHero(hero);
	this.tower = tower;
	this.floor = floor;
	this.WIDTH = width;
	this.setAlly(ally);
	grounds = new HashSet<Ground>();
	enemies = new HashSet<Enemy>();
	items = new HashSet<Item>();
	proyectiles = new HashSet<Attack>();
	tempWalls = new HashSet<TemporalWall>();
    }

    /**
     * Initialized world.
     */
    public void initWorld() {
	for (Iterator<Ground> it = grounds.iterator(); it.hasNext();) {
	    Ground ground = it.next();
	    ground.isBodyAbove.put(hero, true);
	}
    }

    /**
     * Update items.
     * 
     * @param delta
     *            is the time passing.
     */
    public void itemsUpdate(float delta) {
	Rectangle heroRect = Utils.obtainRectangle();
	Circle itemCircle = Utils.obtainCircle();
	// Obtain rectangle of hero.
	heroRect.set(hero.position.x, hero.position.y, hero.getWidth(),
		hero.getHeight());
	for (Iterator<Item> it = items.iterator(); it.hasNext();) {
	    Item item = it.next();
	    if (!item.isDoneForRemove(delta)) {
		item.calculatePositionAndVelocity(delta);

		// Y axis bounds
		if (!bodyGroundCollision(item, delta)) {
		    item.fall();
		}

		// World bounds
		// Floor bound
		if (item.position.y <= floor) {
		    item.position.y = floor;
		    item.stopMoveY();
		    item.stopMoveX();
		}
		// X axis bounds
		if (item.position.x <= 0) {
		    item.position.x = 0;
		    item.bounceX();
		} else if (item.getRightX() >= WIDTH) {
		    item.position.x = WIDTH - item.getWidth();
		    item.bounceX();
		}
		// Collisions with Hero
		if (!hero.isDead()) {
		    itemCircle.set(item.getMiddleX(), item.getMiddleY(),
			    item.getRadius());
		    if (Intersector.overlaps(itemCircle, heroRect)) {
			item.applyEffect(hero);
			it.remove();
			removeBodyFromGounds(item);
		    }
		}
	    } else {
		it.remove();
		removeBodyFromGounds(item);
	    }
	}

    }

    /**
     * Update ally.
     * 
     * @param delta
     *            is the time passed.
     */
    public void allyUpdate(float delta) {
	if (ally != null) {
	    ally.move(delta);
	}
    }

    /**
     * Update proyectiles.
     * 
     * @param delta
     *            is the time passed.
     */
    public void proyectilesUpdate(float delta) {
	Rectangle proyectileRect = Utils.obtainRectangle();
	Rectangle enemyRect = Utils.obtainRectangle();
	Rectangle heroRect = Utils.obtainRectangle();
	Rectangle towerRect = Utils.obtainRectangle();
	for (Iterator<Attack> it = proyectiles.iterator(); it.hasNext();) {
	    Attack a = it.next();
	    if (!a.isDoneForRemove(delta)) {
		a.calculatePositionAndVelocity(delta);
		a.move();
		proyectileRect.set(a.position.x, a.position.y, a.getWidth(),
			a.getHeight());
		// Calculate collision with enemies.
		if (a.getHeroState() != null) {
		    for (Iterator<Enemy> en = enemies.iterator(); en.hasNext();) {
			Enemy enemy = en.next();
			enemyRect.set(enemy.position.x, enemy.position.y,
				enemy.getWidth(), enemy.getHeight());
			if (!enemy.isDead()
				&& proyectileRect.overlaps(enemyRect)) {
			    enemy.removeLife(a.getDamage());
			    if (a.getMiddleX() > enemy.getMiddleX()) {
				enemy.push(Enemy.PUSH_Y, -Enemy.PUSH_X);
			    } else if (a.getMiddleX() < enemy.getMiddleX()) {
				enemy.push(Enemy.PUSH_Y, Enemy.PUSH_X);
			    }
			}
		    }
		}
		// Calculate collision with hero or tower
		else {
		    heroRect.set(hero.position.x, hero.position.y,
			    hero.getWidth(), hero.getHeight());
		    towerRect.set(tower.position.x, tower.position.y,
			    tower.getWidth(), tower.getHeight());
		    // Overlaps hero.
		    if (!hero.isDead() && heroRect.overlaps(proyectileRect)) {
			hero.removeLife(a.getDamage());
			if (a.getMiddleX() > hero.getMiddleX()) {
			    hero.push(Hero.PUSH_Y, -Hero.PUSH_X);
			} else if (a.getMiddleX() < hero.getMiddleX()) {
			    hero.push(Hero.PUSH_Y, Hero.PUSH_X);
			}
			it.remove();
		    }
		    // overlaps with tower.
		    if (!tower.isDead() && towerRect.overlaps(proyectileRect)) {
			tower.removeLife(a.getDamage());
			it.remove();
		    }
		}
	    } else {
		it.remove();
	    }
	}
    }

    /**
     * Temporal wall update.
     * 
     * @param delta
     *            is the time passed.
     */
    public void temporalWallUpdate(float delta) {
	for (Iterator<TemporalWall> it = tempWalls.iterator(); it.hasNext();) {
	    TemporalWall wall = it.next();
	    if (wall.isDoneForRemove(delta)) {
		it.remove();
	    }
	}
    }

    /**
     * Enemies update.
     * 
     * @param delta
     *            is the time passed.
     */
    public void enemiesUpdate(final float delta) {
	Rectangle towerRect = Utils.obtainRectangle();
	Rectangle heroRect = Utils.obtainRectangle();
	Rectangle enemyRect = Utils.obtainRectangle();
	// Obtain rectangle of hero.
	heroRect.set(hero.position.x, hero.position.y, hero.getWidth(),
		hero.getHeight());
	towerRect.set(tower.position.x, tower.position.y, tower.getWidth(),
		tower.getHeight());
	// moves of each enemy
	for (Iterator<Enemy> it = enemies.iterator(); it.hasNext();) {
	    Enemy enemy = it.next();
	    if (enemy.isDead()) {
		if (enemy.isDoneForRemove(delta)) {
		    it.remove();
		    removeBodyFromGounds(enemy);
		}
	    } else {
		// Check attacks of enemies.
		enemy.checkAnimationAttack(delta);
		if (enemy.getAttack() != null) {
		    // Check if attack of enemy is a proyectile.
		    if (enemy.isAttacking() && enemy.getAttack().isProjectile()) {
			this.proyectiles.add(enemy.getAttack());
			enemy.setAttack(null);
		    }
		    // Check if attack of enemy is standing.
		    else if (enemy.isAttacking()
			    && !enemy.getAttack().isProjectile()) {
			Attack a = enemy.getAttack();
			if (a.isMoveInXAxis() || a.isMoveInYAxis())
			    a.putPositionOfBody(enemy);
			Rectangle attack = Utils.obtainRectangle();
			attack.set(a.position.x, a.position.y, a.getWidth(),
				a.getHeight());
			// Overlaps hero.
			if (!hero.isDead() && !hero.isAttacking()
				&& attack.overlaps(heroRect)) {
			    hero.removeLife(a.getDamage());
			    if (enemy.isFaceRight()) {
				hero.push(Hero.PUSH_Y, Hero.PUSH_X);
			    } else {
				hero.push(Hero.PUSH_Y, -Hero.PUSH_X);
			    }
			    enemy.setAttack(null);
			}
			// OverlapsTower.
			if (!tower.isDead() && attack.overlaps(towerRect)) {
			    tower.removeLife(a.getDamage());
			    enemy.setAttack(null);
			}
		    }
		}
		// Enemy moves
		enemy.enemyMove(delta, tower);

		// Y axis grounds
		if (!bodyGroundCollision(enemy, delta)) {
		    enemy.fall();
		}

		// World bounds
		worldBounds(enemy);

		// Collisions with hero
		enemyRect.set(enemy.position.x, enemy.position.y,
			enemy.getWidth(), enemy.getHeight());

		// If hero is attacking and not using projectiles in attack.
		if (hero.isAttacking() && !hero.isAttackingWithProyectile()) {
		    Attack a = hero.getAttack();
		    if (a.isMoveInXAxis() || a.isMoveInYAxis())
			a.putPositionOfBody(hero);
		    Rectangle attack = Utils.obtainRectangle();
		    attack.set(a.position.x, a.position.y, a.getWidth(),
			    a.getHeight());
		    // Overlaps.
		    if (!enemy.isDead() && attack.overlaps(enemyRect)) {
			enemy.removeLife(a.getDamage());
			if (hero.isFaceRight()) {
			    enemy.push(Enemy.PUSH_Y, Enemy.PUSH_X);
			} else {
			    enemy.push(Enemy.PUSH_Y, -Enemy.PUSH_X);
			}

		    }
		}
		// If Hero and enemy are not attacking.
		else if (!hero.isAttacking() && heroRect.overlaps(enemyRect)
			&& !heroCollisionEnemy) {
		    heroCollisionEnemy = true;
		    hero.removeLife(enemy.getDamage());
		    elapsedCollitionTime = 0;
		    if (hero.getMiddleX() > enemy.getMiddleX()) {
			enemy.push(Enemy.PUSH_Y, -Enemy.PUSH_X);
			hero.push(Hero.PUSH_Y, Hero.PUSH_X);
		    } else if (hero.getMiddleX() < enemy.getMiddleX()) {
			enemy.push(Enemy.PUSH_Y, Enemy.PUSH_X);
			hero.push(Hero.PUSH_Y, -Hero.PUSH_X);
		    } else {
			if (Utils.randomBoolean()) {
			    enemy.push(Enemy.PUSH_Y, -Enemy.PUSH_X);
			    hero.push(Hero.PUSH_Y, Hero.PUSH_X);
			} else {
			    enemy.push(Enemy.PUSH_Y, Enemy.PUSH_X);
			    hero.push(Hero.PUSH_Y, -Hero.PUSH_X);
			}
		    }
		}
	    }
	}
    }

    /**
     * Hero update.
     * 
     * @param delta
     *            is the time passed.
     */
    public void heroUpdate(final float delta) {
	// if the hero win or lose he changes his state itself.
	hero.win(delta);
	hero.lose();

	// Add creation
	hero.addCreation(10, delta);
	
	// Check if hero is attacking
	if (hero.isAttacking()) {
	    hero.checkAnimationAttack(delta);
	}
	hero.calculatePositionAndVelocity(delta);
	// Y axis bounds
	if (!bodyGroundCollision(hero, delta)) {
	    hero.fall();
	}
	// world bounds
	worldBounds(hero);
	// check the hero immunity
	if (!hero.isDead()) {
	    if (heroCollisionEnemy) {
		elapsedCollitionTime += delta;
		if (elapsedCollitionTime >= Hero.IMMUNITY) {
		    heroCollisionEnemy = false;
		}
	    }
	}
    }

    /**
     * Check the body collision with grounds in stage.
     * 
     * @param body
     *            is a body.
     * @param delta
     *            is a time passed.
     * @return true if body collision with ground.
     */
    private boolean bodyGroundCollision(DynamicBody body, float delta) {
	for (Iterator<Ground> it = grounds.iterator(); it.hasNext();) {
	    Ground ground = it.next();
	    // collision if the hero is falling or not moving
	    if (ground.checkCollision(body)) {
		body.position.y = ground.getTopY();
		body.stopMoveY();
		body.stopMoveX();
		return true;
	    }
	    // check position
	    if (ground.getTopY() <= body.position.y) {
		ground.isBodyAbove.put(body, true);
	    } else {
		ground.isBodyAbove.put(body, false);
	    }
	}
	return false;
    }

    /**
     * Limits of world.
     * 
     * @param body
     *            is a body.
     */
    private void worldBounds(DynamicBody body) {
	// Floor bound
	if (body.position.y <= floor) {
	    body.position.y = floor;
	    body.stopMoveY();
	    body.stopMoveX();
	}

	// X axis bounds
	if (body.position.x <= 0) {
	    body.position.x = 0;
	    body.stopMoveX();
	} else if (body.getRightX() >= WIDTH) {
	    body.position.x = WIDTH - body.getWidth();
	    body.stopMoveX();
	}
    }

    // -----------------------------------------
    // Management of Associations
    // -----------------------------------------

    /**
     * Get floor of stage.
     * 
     * @return the floor of the stage.
     */
    public float getFloor() {
	return this.floor;
    }

    /**
     * Get hero.
     * 
     * @return the hero.
     */
    public Hero getHero() {
	return hero;
    }

    /**
     * Change hero
     * 
     * @param newHero
     *            is the new hero to add.
     * @return true if hero has changed, false otherwise.
     */
    public boolean setHero(Hero newHero) {
	if (hero == newHero)
	    return false;

	Hero oldHero = this.hero;
	hero = newHero;
	if (oldHero != null) {
	    oldHero.setWorld(null);
	}
	if (hero != null) {
	    hero.setWorld(this);
	}
	return true;
    }

    /**
     * Get tower
     * 
     * @return the tower.
     */
    public Tower getTower() {
	return tower;
    }

    /**
     * Set tower.
     * 
     * @param tower
     *            is the tower to changed.
     */
    public void setTower(Tower tower) {
	this.tower = tower;
    }

    /**
     * Get ally
     * 
     * @return the ally.
     */
    public Ally getAlly() {
	return this.ally;
    }

    /**
     * Set ally.
     * 
     * @param newAlly
     *            is the ally to changed.
     * @return true if ally has changed, false otherwise.
     */
    public boolean setAlly(Ally newAlly) {
	if (ally == newAlly)
	    return false;

	Ally oldAlly = this.ally;
	ally = newAlly;
	if (oldAlly != null) {
	    oldAlly.setWorld(null);
	}
	if (ally != null) {
	    ally.setWorld(this);
	}
	return true;
    }

    /**
     * Get grounds.
     * 
     * @return a set that contain the grounds of this world.
     */
    public Set<Ground> getGrounds() {
	return Collections.unmodifiableSet(this.grounds);
    }

    /**
     * Add grounds to this world
     * 
     * @param ground
     *            is the ground to add
     * @return true if ground has be added, false otherwise.
     */
    public boolean addGround(Ground ground) {
	if (ground != null) {
	    return this.grounds.add(ground);
	}
	return false;
    }

    /**
     * Delete ground of this world.
     * 
     * @param ground
     *            is the ground to removed.
     * @return true if ground has be removed, false otherwise.
     */
    public boolean removeGround(Ground ground) {
	if (ground != null) {
	    return this.grounds.remove(ground);
	}
	return false;
    }

    /**
     * Remove body situated in a specific ground.
     * 
     * @param body
     *            is the body to deleted.
     */
    public void removeBodyFromGounds(DynamicBody body) {
	for (Iterator<Ground> it = grounds.iterator(); it.hasNext();) {
	    Ground ground = it.next();
	    ground.isBodyAbove.remove(body);
	}
    }

    /**
     * Get enemies.
     * 
     * @return the enemies.
     */
    public Set<Enemy> getEnemies() {
	return Collections.unmodifiableSet(this.enemies);
    }

    /**
     * Add enemy.
     * 
     * @param enemy
     *            is the enemy to add.
     * @return true if enemy has be added, false otherwise.
     */
    public boolean addEnemy(Enemy enemy) {
	if (enemy != null) {
	    for (Iterator<Ground> it = grounds.iterator(); it.hasNext();) {
		Ground ground = it.next();
		ground.isBodyAbove.put(enemy, false);
	    }
	    return this.enemies.add(enemy);
	}
	return false;
    }

    /**
     * Remove a specific enemy.
     * 
     * @param enemy
     *            is the enemy to remove.
     * @return true if enemy has be removed, false otherwise.
     */
    public boolean removeEnemy(Enemy enemy) {
	if (enemy != null) {
	    return this.grounds.remove(enemy);
	}
	return false;
    }

    /**
     * Calculate the number of enemies that in world.
     * 
     * @return the number of enemies that in world.
     */
    public int enemiesSize() {
	return this.enemies.size();
    }

    /**
     * Get items.
     * 
     * @return items.
     */
    public Set<Item> getItems() {
	return Collections.unmodifiableSet(this.items);
    }

    /**
     * Add item.
     * 
     * @param item
     *            is the item to add.
     * @return true if item has be added, false otherwise.
     */
    public boolean addItem(Item item) {
	if (item != null) {
	    for (Iterator<Ground> it = grounds.iterator(); it.hasNext();) {
		Ground ground = it.next();
		ground.isBodyAbove.put(item, true);
	    }
	    return this.items.add(item);
	}
	return false;
    }

    /**
     * Remove item.
     * 
     * @param item
     *            is the item to removed.
     * @return true if item has be removed, false otherwise.
     */
    public boolean removeItem(Ground item) {
	if (item != null) {
	    return this.items.remove(item);
	}
	return false;
    }

    /**
     * Get proyectiles.
     * 
     * @return the proyectiles.
     */
    public Set<Attack> getProyectiles() {
	return Collections.unmodifiableSet(this.proyectiles);
    }

    /**
     * Add proyectile.
     * 
     * @param proyectile
     *            is the proyectile to add.
     * @return true if proyectiles has be added.
     */
    public boolean addProyectile(Attack proyectile) {
	if (proyectile != null) {
	    return this.proyectiles.add(proyectile);
	}
	return false;
    }

    /**
     * Remove proyectile.
     * 
     * @param proyectile
     *            is the proyectile to removed.
     * @return true if proyectiles has be removed.
     */
    public boolean removeProyectile(Attack proyectile) {
	if (proyectile != null) {
	    return this.proyectiles.remove(proyectile);
	}
	return false;
    }

    /**
     * Get temporal wall.
     * 
     * @return the temporal walls.
     */
    public Set<TemporalWall> getTemporalWalls() {
	return Collections.unmodifiableSet(this.tempWalls);
    }

    /**
     * Add temporal wall.
     * 
     * @param tWall
     *            is a temporal wall to add.
     * 
     * @return true if temporal wall is added, false otherwise.
     */
    public boolean addTemporalWall(TemporalWall tWall) {
	if (tWall != null) {
	    return this.tempWalls.add(tWall);
	}
	return false;
    }

    /**
     * Remove temporal wall.
     * 
     * @param tWall
     *            is a temporal wall to remove.
     * @return true if temporal wall is removed, false otherwise.
     */
    public boolean removeTemporalWall(TemporalWall tWall) {
	if (tWall != null) {
	    return this.tempWalls.remove(tWall);
	}
	return false;
    }
}
