/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package domain.allies;

import domain.items.Item;
import domain.items.LifeItem;
import domain.items.MagicItem;
import domain.utils.Utils;

/**
 * AllyNeutral.java
 * 
 * @version Apr 19, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class AllyNeutral extends Ally {
    
    /* CONSTANTS */
    // Time Life Items in Seconds
    private static final int TIMELIFE_LIFE_ITEM = 10;
    private static final int TIMELIFE_MAGIC_ITEM = 10;
    private static final int LIFE_BONUS = 7;
    private static final int MAGIC_BONUS = 7;

    /**
     * AllyNeutral constructor.
     * 
     * @param width
     *            is the width of ally.
     * @param height
     *            is the height of ally.
     * @param xLeftLimit
     *            is the limit to move into left.
     * @param xRightLimit
     *            is the limit to move into right.
     * @param yBottomLimit
     *            is the limit y to moved.
     */
    public AllyNeutral(float width, float height, float xLeftLimit,
	    float xRightLimit, float yBottomLimit) {
	super(width, height, xLeftLimit, xRightLimit, yBottomLimit);
    }

    @Override
    protected Item getNewItem() {
	final float posx = isFaceRight() ? getRightX() : position.x;
	final float posy = getTopY();
	// Create Item
	if (Utils.randomBoolean()) {
	    return new LifeItem(posx, posy, ITEM_RADIUS, TIMELIFE_LIFE_ITEM,
		    LIFE_BONUS);
	} else {
	    return new MagicItem(posx, posy, ITEM_RADIUS, TIMELIFE_MAGIC_ITEM,
		    MAGIC_BONUS);
	}
    }

}
