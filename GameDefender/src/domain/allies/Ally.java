/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package domain.allies;

import service.data.StateManager.AllyState;
import domain.DynamicBody;
import domain.World;
import domain.items.Item;
import domain.utils.Utils;

/**
 * AAlly.java
 * 
 * @version Apr 19, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public abstract class Ally extends DynamicBody {

    /* Fields */
    // random time to move between two values in seconds
    private static final int FROM = 5;
    private static final int TO = 6;
    // The percentage of probability of throw and item
    private static final int ITEM_RANDOM = 100;
    // velocity
    private static final float WALK_VEL = 5;
    // Item throw Fields velocity and radius
    private static final int ITEMTROW_FROM_VX = 20;
    private static final int ITEMTROW_TO_VX = 60;
    private static final int ITEMTROW_FROM_VY = 20;
    private static final int ITEMTROW_TO_VY = 70;
    protected static final int ITEM_RADIUS = 8;

    // Limits of the Ally
    private AllyState state;
    private float xLeftLimit;
    private float xRightLimit;
    private float yBottomLimit;

    // Auxiliary fields
    private boolean isFaceRight;
    private boolean inMiddle;
    private float middlePoint;
    // time random to throw item
    private boolean canThrowItem;
    private float timeToThrow;
    private float randomDelayThrow;
    // time random to move ally
    private float timeToMove;
    private float randomDelayMove;
    private float creationTime;

    // Asosiations
    protected World world;

    /**
     * Ally contructor.
     * 
     * @param width
     *            is the width of ally.
     * @param height
     *            is the height of ally.
     * @param xLeftLimit
     *            is the limit to move into left.
     * @param xRightLimit
     *            is the limit to move into right.
     * @param yBottomLimit
     *            is the limit y to moved.
     */
    public Ally(float width, float height, float xLeftLimit, float xRightLimit,
	    float yBottomLimit) {
	super(xLeftLimit + ((xRightLimit - xLeftLimit) / 2) - width / 2,
		yBottomLimit, width, height);
	this.middlePoint = xLeftLimit + ((xRightLimit - xLeftLimit) / 2);
	this.xLeftLimit = xLeftLimit;
	this.xRightLimit = xRightLimit;
	this.yBottomLimit = yBottomLimit;
	this.isFaceRight = true;
	this.inMiddle = true;
	this.timeToMove = 0;
	this.randomDelayMove = 0;
	this.creationTime = 0;
	this.state = AllyState.BORN;
    }

    /**
     * Move ally.
     * 
     * @param delta
     *            is the time passed.
     */
    public void move(final float delta) {
	// Creation not move
	if (state == AllyState.BORN) {
	    this.creationTime += delta;
	    this.timeToMove = -1;
	    if (this.creationTime >= FROM) {
		state = AllyState.FRONT;
	    }
	}
	// move
	timeToMove += delta;
	if (timeToMove >= randomDelayMove) {
	    randomMove();
	}
	// calculate position and velocity
	calculatePositionAndVelocity(delta);
	// Y axis bounds
	if (position.y <= yBottomLimit) {
	    position.y = yBottomLimit;
	    stopMoveY();
	}

	// Middle point
	boolean cond1 = velocity.x > 0 && position.x + width / 2 >= middlePoint;
	boolean cond2 = velocity.x < 0 && position.x + width / 2 <= middlePoint;
	if ((cond1 || cond2) && !inMiddle) {
	    position.x = middlePoint - width / 2;
	    stopMoveX();
	    stopMoveY();
	    inMiddle = true;
	}

	// X axis bounds
	if (position.x <= xLeftLimit) {
	    position.x = xLeftLimit;
	    stopMoveX();
	    stopMoveY();
	    inMiddle = false;
	} else if (getRightX() >= xRightLimit) {
	    position.x = xRightLimit - getWidth();
	    stopMoveX();
	    stopMoveY();
	    inMiddle = false;
	}
	// throw item if can
	if (canThrowItem) {
	    timeToThrow += delta;
	    if (timeToThrow > randomDelayThrow) {
		world.addItem(throwItem());
		canThrowItem = false;
	    }
	}
    }

    private void randomMove() {
	// fields
	timeToMove = 0;
	randomDelayMove = Utils.randomInt(TO - FROM + 1) + FROM;
	// move
	if (inMiddle) {
	    if (Utils.randomBoolean()) {
		moveX(-WALK_VEL);
		isFaceRight = false;
	    } else {
		moveX(WALK_VEL);
		isFaceRight = true;
	    }
	} else {
	    if (isFaceRight) {
		moveX(-WALK_VEL);
		isFaceRight = false;
		if (Utils.randomBoolean()) {
		    inMiddle = true;
		}
	    } else {
		moveX(WALK_VEL);
		isFaceRight = true;
		if (Utils.randomBoolean()) {
		    inMiddle = true;
		}
	    }
	}
	// state
	state = AllyState.WALK;
    }

    @Override
    public void stopMoveX() {
	if (isMovingX) {
	    super.stopMoveX();
	    if (position.x != middlePoint - width / 2) {
		if (Utils.randomInt(100) < ITEM_RANDOM) {
		    canThrowItem = true;
		    timeToThrow = 0;
		    randomDelayThrow = Utils.randomInt(FROM);
		}
	    }
	    // state
	    if (inMiddle) {
		state = AllyState.INACTIVE;
	    } else {
		state = AllyState.FRONT;
	    }
	}
    }

    /**
     * Throw item, Throw Item depends of the ally type.
     * 
     * @return the item that thrower by ally.
     */
    public Item throwItem() {
	// Calculate position and velocity
	float aux = Utils.randomInt(ITEMTROW_TO_VX - ITEMTROW_FROM_VX)
		+ ITEMTROW_FROM_VX;
	final float velocity_x = isFaceRight() ? aux : -aux;
	final float velocity_y = Utils.randomInt(ITEMTROW_TO_VY
		- ITEMTROW_FROM_VY)
		+ ITEMTROW_FROM_VY;
	// Create Item
	Item item = getNewItem();
	item.moveX(velocity_x);
	item.moveY(velocity_y);
	return item;
    }

    /**
     * Obtain new item.
     * 
     * @return the new item.
     */
    protected abstract Item getNewItem();

    /**
     * Determine if ally is lock to right or left.
     * 
     * @return true if ally is lock to right, false otherwise.
     */
    public boolean isFaceRight() {
	return this.isFaceRight;
    }

    /**
     * Change world associated to this ally.
     * 
     * @param newWorld
     *            is the newWorld to associated to this ally.
     * @return true if world has be changed, false otherwise.
     */
    public boolean setWorld(World newWorld) {
	if (world == newWorld)
	    return false;

	World oldWorld = this.world;
	world = newWorld;
	if (oldWorld != null) {
	    oldWorld.setAlly(null);
	}
	if (world != null) {
	    world.setAlly(this);
	}
	return true;
    }

    /**
     * Obtain the world associated to this ally.
     * 
     * @return the world associated to this ally.
     */
    public World getSystemWorld() {
	return world;
    }

    /**
     * Obtain the state of ally.
     * 
     * @return the state of ally.
     */
    public AllyState getState() {
	return state;
    }

    /**
     * Change state of ally.
     * 
     * @param state
     *            is the new state to put to this ally.
     */
    public void setState(AllyState state) {
	this.state = state;
    }
}
