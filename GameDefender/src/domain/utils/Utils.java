/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package domain.utils;

import java.util.Random;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Pool;

/**
 * DomainUtils.java
 * 
 * @version Apr 19, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class Utils {

    // Rectangle Pool
    private static final Pool<Rectangle> rectranglePool = new Pool<Rectangle>() {
	@Override
	protected Rectangle newObject() {
	    return new Rectangle();
	}
    };

    /**
     * Obtain rectangle.
     * 
     * @return the rectangle.
     */
    public static Rectangle obtainRectangle() {
	return rectranglePool.obtain();
    }

    // Circle Pool
    private static final Pool<Circle> circlePool = new Pool<Circle>() {
	@Override
	protected Circle newObject() {
	    return new Circle();
	}
    };

    /**
     * Obtain circle.
     * 
     * @return the circle.
     */
    public static Circle obtainCircle() {
	return circlePool.obtain();
    }

    // Random Object

    private static final Random random = new Random();

    public static boolean randomBoolean() {
	return random.nextBoolean();
    }

    public static double randomDouble() {
	return random.nextDouble();
    }

    public static float randomFloat() {
	return random.nextFloat();
    }

    public static int randomInt() {
	return random.nextInt();
    }

    public static int randomInt(int n) {
	return random.nextInt(n);
    }

}
