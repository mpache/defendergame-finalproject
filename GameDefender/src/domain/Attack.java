/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package domain;

import service.data.StateManager.EnemyState;
import service.data.StateManager.HeroState;

/**
 * Proyectile.java
 * 
 * @version May 8, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class Attack extends DynamicBody {

    /* Fields */
    // Fields to render only used in proyectile.
    private float width;
    private float height;

    // State of hero and enemy only used if attack is a projectile.
    private HeroState heroState;
    private EnemyState enemyState;

    // Auxiliary fields.
    private boolean isFaceRight;
    private boolean isProyectile;
    private boolean moveX, moveY;
    private float velocity;

    // Time of attack
    private boolean explode; // Field to put animation explosion (proyectiles).
    private float timeToAdd;
    private float timeLife;
    private float timeToRemove;

    // Damage that causing this attack.
    private int damage;

    /**
     * Create an attack.
     * 
     * @param posx
     *            is the initial position x.
     * @param posy
     *            is the initial position y.
     * @param widthCollision
     *            is the width of attack to detect collisions of this.
     * @param heightCollision
     *            is the height of attack to detect collisions of this.
     */
    public Attack(float posx, float posy, float widthCollision,
	    float heightCollision) {
	super(posx, posy, widthCollision, heightCollision);
    }

    /**
     * Configure attack.
     * 
     * @param width
     *            the width of attack only used if attack is projectile to
     *            render it.
     * @param height
     *            the height of attack only used if attack is projectile to
     *            render it.
     * @param timeLife
     *            the duration of attack.
     * @param isFaceRight
     *            if attack is to right or to left.
     * @param isProyectile
     *            if attack is a projectile.
     * @param moveX
     *            if attack is moved in x axis.
     * @param moveY
     *            if attack is moved in y axis.
     * @param velocity
     *            only used if attack is a projectile, indicate the velocity to
     *            move projectile.
     * @param damage
     *            the damage that causing this attack.
     */
    public void setSettingsAttack(float width, float height, float timeLife,
	    boolean isFaceRight, boolean isProyectile, boolean moveX,
	    boolean moveY, float velocity, int damage) {
	this.width = width;
	this.height = height;
	this.timeLife = timeLife;
	this.timeToRemove = 0;
	this.explode = false;
	this.isFaceRight = isFaceRight;
	this.isProyectile = isProyectile;
	this.moveX = moveX;
	this.moveY = moveY;
	this.velocity = velocity;
	this.damage = damage;
    }

    /**
     * Change time to add this attack, only used if attack is a projectile
     * 
     * @param time
     *            is the time that pased to add.
     */
    public void setTimeToAdd(float timeToAdd) {
	this.timeToAdd = timeToAdd;
    }

    /**
     * Indicate, using a state, if this attack is hero or enemy, never a attack
     * contain two states, one per hero other per enemy.
     * 
     * @param heroState
     *            is the actual state of hero.
     * @param enemyState
     *            is the actual state of enemy
     */
    public void setState(HeroState heroState, EnemyState enemyState) {
	this.heroState = heroState;
	this.enemyState = enemyState;
    }

    /**
     * Move attack, only used in projectile.
     * 
     * @param vel
     *            is the velocity of projectile.
     */
    public void move() {
	if (this.moveX && !this.moveY) {
	    super.stopMoveY();
	    if (isFaceRight) {
		moveX(velocity);
	    } else {
		moveX(-velocity);
	    }
	} else if (!this.moveX && this.moveY) {
	    moveY(-velocity);
	} else if (this.moveX && this.moveY) {
	    if (isFaceRight) {
		moveX(velocity);
	    } else {
		moveX(-velocity);
	    }
	    moveY(-velocity);
	} else {
	    this.stopMoveX();
	    this.stopMoveY();
	}
    }

    /**
     * Put positions of body, only used if attack is NOT a projectile.
     * 
     * @param body
     *            is the body to catch positions.
     */
    public void putPositionOfBody(DynamicBody body) {
	if (!isProyectile) {
	    float add = this.getWidth() - body.getWidth();
	    add = isFaceRight ? 0 : -add;
	    this.position.x = body.position.x + add;
	    this.position.y = body.position.y
		    + Math.abs(body.getHeight() - this.getHeight());
	}
    }

    /**
     * Check if attack has finished.
     * 
     * @param delta
     *            is the time that is passing.
     * @return true if attack has finished.
     */
    public boolean isDoneForRemove(float delta) {
	timeToRemove += delta;
	if (timeToRemove > timeLife) {
	    explode = isProyectile ? true : false;
	    return true;
	}
	return false;
    }

    // Getters && SETTERS
    // WIDTH AND HEIGHT TO RENDER PROJECTILE
    /**
     * Width to render if attack if is a projectile.
     */
    public float getWidthToRender() {
	return this.width;
    }

    /**
     * Height to render attack if is a projectile.
     */
    public float getHeightToRender() {
	return this.height;
    }

    // CHECK IF ATTACK IS A PROJECTILE
    /**
     * Get if attack is a projectile
     * 
     * @return true if attack is a projectile.
     */
    public boolean isProjectile() {
	return this.isProyectile;
    }

    // CHECK IF ATTACK IS EXPLODE
    /**
     * Check if attack (only if a projectile).
     * 
     * @return true if projectile has explode.
     */
    public boolean isExplode() {
	return explode;
    }

    // CHECK DAMAGE OF THIS ATTACK
    /**
     * Get damage that causing this attack.
     * 
     * @return the damage that causing this attack.
     */
    public int getDamage() {
	return this.damage;
    }

    // STATE OF HERO OR ENEMY THAT MAKE THIS ATTACK
    /**
     * @return the heroState
     */
    public HeroState getHeroState() {
	return heroState;
    }

    /**
     * @return the enemyState
     */
    public EnemyState getEnemyState() {
	return enemyState;
    }

    // CHECK IF PROYECTILE IS TO LEFT OR TO RIGHT
    /**
     * @return the isFaceRight
     */
    public boolean isFaceRight() {
	return isFaceRight;
    }

    // CHECK TIME TO ADD
    /**
     * Get time to add this attack.
     * 
     * @return the time that pass to add attack.
     */
    public float getTimeToAdd() {
	return this.timeToAdd;
    }

    // CHECK IF ATTACK IS MOVE
    /**
     * Get if this attack is move in x axis.
     * 
     * @return true if this attack is move in x axis.
     */
    public boolean isMoveInXAxis() {
	return this.moveX;
    }

    /**
     * Get if this attack is move in y axis.
     * 
     * @return true if this attack is move in y axis.
     */
    public boolean isMoveInYAxis() {
	return this.moveY;
    }
}
