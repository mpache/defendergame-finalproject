/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package domain.walls;

/**
 * TemporalWall.java
 *
 * @version May 27, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 *
 */
public class TemporalWall {

    /* Fields */
    private float posAntX, posAntY, posX, posY;
    private float timeLife, timeToRemove;

    public TemporalWall(float posAntX, float posAntY, float posx, float posy,
	    float timeLife) {
	this.posAntX = posAntX;
	this.posAntY = posAntY;
	this.posX = posx;
	this.posY = posy;
	this.timeLife = timeLife;
	this.timeToRemove = 0;
    }

    // GETTERS AND SETTERS.
    public float getPosX() {
	return this.posX;
    }

    public float getPosY() {
	return this.posY;
    }

    public float getHeight() {
	return 10;
    }

    public float getWidth() {
	return 10;
    }
    
    public float getDistanceWalls() {
	float width = 0;
	if (posX > posAntX) {
	    width = (float) Math.sqrt(Math.pow((double) (posX - posAntX), 2)
		    + Math.pow((double) (posY - posAntY), 2));
	} else {
	    width = (float) Math.sqrt(Math.pow((double) (posAntX - posX), 2)
		    + Math.pow((double) (posAntY - posY), 2));
	}
	return width;
    }

    public boolean isDoneForRemove(float delta) {
	timeToRemove += delta;
	if (timeToRemove > timeLife) {
	    return true;
	}
	return false;
    }
}
