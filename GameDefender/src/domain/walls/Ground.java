/* 
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 *                Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package domain.walls;

import java.util.HashMap;

import domain.Body;
import domain.DynamicBody;

/**
 * Ground.java
 * 
 * @version Apr 19, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class Ground extends Body {

    /* Fields */
    public final HashMap<DynamicBody, Boolean> isBodyAbove;

    /**
     * Ground constructor.
     * 
     * @param posx
     *            is the x position of ground.
     * @param posy
     *            is the y position of ground.
     * @param width
     *            is the width of ground.
     * @param height
     *            is the height of ground.
     */
    public Ground(float posx, float posy, float width, float height) {
	super(posx, posy, width, height);
	isBodyAbove = new HashMap<DynamicBody, Boolean>();
    }

    /**
     * Method that check the collision of the body with the ground.<br>
     * The body only collides if the body velocity is negative or zero, for more
     * precision the "diff" parameter is the maximum movement the body will be
     * do in the next frame.
     * 
     * @param body
     *            the body to check
     * @param diff
     *            the approximate value of maximum distance that hero will go in
     *            the next frame
     * @return true if the body is treading the ground, false otherwise.
     */
    public boolean checkCollision(DynamicBody body) {

	boolean cond1 = body.position.x >= this.position.x
		&& body.position.x <= this.getRightX();
	boolean cond2 = body.getRightX() >= this.position.x
		&& body.getRightX() <= this.getRightX();

	return (cond1 || cond2) && body.position.y <= this.getTopY()
		&& this.isBodyAbove.get(body);
    }
}
