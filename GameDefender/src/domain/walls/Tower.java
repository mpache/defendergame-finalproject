/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package domain.walls;

import domain.Body;

/**
 * Tower.java
 * 
 * @version Apr 19, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class Tower extends Body {

    /* Fields */
    public final int initialLife;
    private int life;

    /**
     * Tower constructor.
     * 
     * @param posx
     *            is x position of tower.
     * @param posy
     *            is the y position of tower.
     * @param width
     *            is the width of tower.
     * @param height
     *            is the height of tower.
     * @param life
     *            is the life of tower.
     */
    public Tower(float posx, float posy, float width, float height, int life) {
	super(posx, posy, width, height);
	this.initialLife = life;
	this.life = life;
    }

    /**
     * Get life of tower.
     * 
     * @return the life of tower.
     */
    public int getLife() {
	return this.life;
    }

    /**
     * Add life of tower.
     * 
     * @param life
     *            is the life to add.
     */
    public void addLife(int life) {
	this.life = this.life + life >= this.initialLife ? this.initialLife
		: this.life + life;
    }

    /**
     * Remove life.
     * 
     * @param life
     *            is the life to remove.
     */
    public void removeLife(int life) {
	this.life = this.life - life <= 0 ? 0 : this.life - life;
    }

    /**
     * Check if tower is destroyed.
     * 
     * @return true if tower is destroyed, false otherwise.
     */
    public boolean isDead() {
	if (this.life <= 0) {
	    return true;
	}
	return false;
    }

}
