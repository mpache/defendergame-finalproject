/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package domain.enemies;

import service.data.StateManager.EnemyState;
import service.data.TypeManager.EnemyType;
import domain.Attack;
import domain.walls.Tower;

/**
 * GroundEnemy.java
 * 
 * @version Apr 19, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class GroundEnemy extends Enemy {

    /* CONSTANTS */
    private static final float TIME_BETWEEN_ATTACKS = 1f;

    // Auxiliary fields
    private boolean canJump;

    /**
     * GroundEnemy constructor.
     * 
     * @param posx
     *            is the x position of enemy.
     * @param posy
     *            is the y position of enemy.
     * @param width
     *            is the width of enemy.
     * @param widthCollision
     *            is the widthCollision of enemy.
     * @param height
     *            is the height of enemy.
     * @param heightCollision
     *            is the height collision of enemy.
     * @param moveVelocity
     *            is the velocity that enemy is moved.
     * @param damage
     *            is the damage that produced enemy.
     * @param type
     *            is the type of enemy.
     */
    public GroundEnemy(float posx, float posy, float width,
	    float widthCollision, float height, float heightCollision,
	    int life, float moveVelocity, int damage, EnemyType type) {
	super(posx, posy, width, widthCollision, height, heightCollision,
		moveVelocity, damage, type);
	this.life = life;
	this.isFaceRight = true;
	this.canJump = false;
	this.bodyBlocked = false;
    }

    @Override
    public void enemyMove(float delta, Tower tower) {
	// Move the enemy
	if (getRightX() < tower.position.x) {
	    moveX(moveVelocity);
	} else if (position.x > tower.getRightX()) {
	    moveX(-moveVelocity);
	}
	// calculate positions and velocity of the enemy
	calculatePositionAndVelocity(delta);
	// tower bounds
	if (getRightX() >= tower.position.x && getRightX() < tower.getRightX()) {
	    position.x = tower.position.x - getWidth();
	    stopMoveX();
	    attack();
	} else if (position.x <= tower.getRightX()
		&& position.x > tower.position.x) {
	    position.x = tower.getRightX();
	    stopMoveX();
	    attack();
	}
    }

    @Override
    public void push(final float vely, final float velx) {
	this.canJump = false;
	this.moveX(velx);
	this.moveY(vely);
	this.bodyBlocked = true;
    }

    @Override
    public void fall() {
	if (this.canJump) {
	    this.moveY(0);
	    this.canJump = false;
	    this.initVelocity.x = 0;
	}
    }

    @Override
    public void stopMoveY() {
	if (isMovingY) {
	    super.stopMoveY();
	    this.canJump = true;
	    this.bodyBlocked = false;
	}
    }

    @Override
    public void attack() {
	// Create and configure attack.
	if (!isAttacking() && stateTimeAttack >= TIME_BETWEEN_ATTACKS) {
	    state = EnemyState.ATTACK;
	    stateTimeAttack = 0;
	    durationAttack = 0.5f;
	    attack = new Attack(position.x, position.y, getWidth() + 20, getHeight());
	    attack.setSettingsAttack(getWidth(), getHeight(), 1f,
		    isFaceRight(), false, true, false, 0, damage);
	    attack.setState(null, state);
	}
    }

}
