/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package domain.enemies;

import service.data.StateManager.EnemyState;
import service.data.TypeManager.EnemyType;
import domain.Attack;
import domain.walls.Tower;

/**
 * FlyEnemy.java
 * 
 * @version Apr 19, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class FlyEnemy extends Enemy {

    /* CONSTANTS */
    private static final float TIME_BETWEEN_ATTACKS = 1f;

    // Interval of high fly
    public static final float HIGH = 10f;

    // Auxiliary fields
    private float direction;
    private boolean isFalling;

    /**
     * FlyEnemy constructor.
     * 
     * @param posx
     *            is the x position of enemy.
     * @param posy
     *            is the y position of enemy.
     * @param width
     *            is the width of enemy.
     * @param widthCollision
     *            is the widthCollision of enemy.
     * @param height
     *            is the height of enemy.
     * @param heightCollision
     *            is the height collision of enemy.
     * @param moveVelocity
     *            is the velocity that enemy is moved.
     * @param damage
     *            is the damage that produced enemy.
     * @param type
     *            is the type of enemy.
     */
    public FlyEnemy(float posx, float posy, float width, float widthCollision,
	    float height, float heightCollision, int life, float moveVelocity,
	    int damage, EnemyType type) {
	super(posx, posy + HIGH * 3, width, widthCollision, height,
		heightCollision, moveVelocity, damage, type);
	this.life = life;
	this.direction = posy;
    }

    @Override
    public void enemyMove(float delta, Tower tower) {
	if (bodyBlocked) {
	    if (position.y >= direction + HIGH * 3 && !isFalling) {
		bodyBlocked = false;
	    }
	}
	// Move the enemy
	if (getRightX() < tower.position.x) {
	    moveX(moveVelocity);
	} else if (position.x > tower.getRightX()) {
	    moveX(-moveVelocity);
	}

	// calculate positions and velocity of the enemy
	calculatePositionAndVelocity(delta);

	// tower bounds
	if (getRightX() >= tower.position.x && getRightX() < tower.getRightX()) {
	    position.x = tower.position.x - getWidth();
	    stopMoveX();
	    if (!bodyBlocked) {
		super.stopMoveY();
	    }
	    attack();
	} else if (position.x <= tower.getRightX()
		&& position.x > tower.position.x) {
	    position.x = tower.getRightX();
	    stopMoveX();
	    if (!bodyBlocked) {
		super.stopMoveY();
	    }
	    attack();
	}
    }

    // Jump and fall moves
    @Override
    public void push(final float vely, final float velx) {
	this.moveX(velx / 2);
	this.moveY(vely / 2);
	setGravity(DEFAULT_GRAVITY);
	bodyBlocked = true;
	isFalling = true;
    }

    @Override
    public void fall() {
	// fly instead of falling
	if (!bodyBlocked) {
	    if (position.y < direction - HIGH) {
		setGravity(moveVelocity / 2);
	    } else if (position.y > direction + HIGH) {
		setGravity(-moveVelocity / 2);
	    } else {
		setGravity(0);
	    }
	}
    }

    @Override
    public void stopMoveY() {
	if (!bodyBlocked) {
	    super.stopMoveY();
	    setGravity(0);
	} else {
	    if (isFalling) {
		setGravity(0);
		isFalling = false;
		moveY(moveVelocity * 2);
	    }
	}
    }

    /**
     * Obtain the direction move of this enemy.
     * 
     * @return is the direction.
     */
    public float getDirection() {
	return direction;
    }

    /**
     * Change the direction move of this enemy.
     * 
     * @param direction
     *            is the new direction.
     */
    public void setDirection(final float direction) {
	this.direction = direction;
    }

    @Override
    public void attack() {
	// Create and configure attack.
	if (!isAttacking() && stateTimeAttack >= TIME_BETWEEN_ATTACKS) {
	    state = EnemyState.ATTACK;
	    stateTimeAttack = 0;
	    durationAttack = 1f;
	    attack = new Attack(position.x, position.y, 20, 20);
	    attack.setSettingsAttack(20, 20, 1f, isFaceRight(), true, true,
		    true, 0.5f, damage);
	    attack.setState(null, state);
	}
    }
}
