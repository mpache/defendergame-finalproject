/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package domain.enemies;

import service.data.StateManager.EnemyState;
import service.data.TypeManager.EnemyType;
import domain.Attack;
import domain.DynamicBody;
import domain.walls.Tower;

/**
 * AEnemy.java
 * 
 * @version Apr 19, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public abstract class Enemy extends DynamicBody {

    // Time to remove when die
    private static final float TIME_TO_REMOVE = 1.5f;
    // Code
    private static int CODE = 0;
    // Push Fields
    public static final float PUSH_X = 30f;
    public static final float PUSH_Y = 30f;

    // State fields
    public final int code;
    public final EnemyType type;
    protected int life;
    protected EnemyState state;

    // Aux fields
    protected int damage;
    protected float moveVelocity;
    protected boolean isFaceRight;
    protected boolean bodyBlocked;
    protected float timeToRemove;
    private boolean beforeRemove;
    protected float widthCollision;
    protected float heightCollision;

    /**
     * Enemy constructor.
     * 
     * @param posx
     *            is the x position of enemy.
     * @param posy
     *            is the y position of enemy.
     * @param width
     *            is the width of enemy.
     * @param widthCollision
     *            is the widthCollision of enemy.
     * @param height
     *            is the height of enemy.
     * @param heightCollision
     *            is the height collision of enemy.
     * @param moveVelocity
     *            is the velocity that enemy is moved.
     * @param damage
     *            is the damage that produced enemy.
     * @param type
     *            is the type of enemy.
     */
    protected Enemy(float posx, float posy, float width, float widthCollision,
	    float height, float heightCollision, float moveVelocity,
	    int damage, EnemyType type) {
	super(posx, posy, width, height);
	this.code = CODE++;
	this.widthCollision = widthCollision;
	this.heightCollision = heightCollision;
	this.moveVelocity = moveVelocity;
	this.damage = damage;
	this.state = EnemyState.INACTIVE;
	this.timeToRemove = 0;
	this.beforeRemove = false;
	this.type = type;
    }

    @Override
    public void moveX(float vel) {
	if (!bodyBlocked) {
	    state = EnemyState.WALK;
	    this.initVelocity.y = this.calculateVelocity(initVelocity.y, time,
		    getGravity());
	    super.moveX(vel);
	    this.isFaceRight = vel > 0 ? true : false;
	}
    }

    /**
     * Move enemy
     * 
     * @param delta
     *            is the time passed.
     * @param tower
     *            is the tower that enemy would destroy.
     */
    public abstract void enemyMove(float delta, Tower tower);

    /**
     * Push enemy.
     * 
     * @param vely
     *            is the velocity to push in y axis.
     * @param velx
     *            is the velocity to push in x axis.
     */
    public abstract void push(final float vely, final float velx);

    /**
     * fall enemy.
     */
    public abstract void fall();

    /**
     * Check if enemy is dead.
     * 
     * @return true if enemy is dead, false otherwise.
     */
    public boolean isDead() {
	if (this.life <= 0) {
	    state = EnemyState.DEAD;
	    return true;
	}
	return false;
    }

    /**
     * Check if enemy is lock to right.
     * 
     * @return true if enemy is lock to right, false otherwise.
     */
    public boolean isFaceRight() {
	return this.isFaceRight;
    }

    /**
     * Get damage of enemy.
     * 
     * @return the damage that produced enemy.
     */
    public int getDamage() {
	return this.damage;
    }

    /**
     * Change the damage that produced enemy.
     * 
     * @param damage
     *            is the damage to changed.
     */
    public void setDamage(int damage) {
	this.damage = damage;
    }

    /**
     * Get life of enemy.
     * 
     * @return the life of enemy.
     */
    public int getLife() {
	return this.life;
    }

    /**
     * Add life to enemy.
     * 
     * @param life
     *            is the life to add.
     */
    public void addLife(int life) {
	this.life += life;
    }

    /**
     * Remove life to enemy.
     * 
     * @param life
     *            is the life to removed.
     */
    public void removeLife(int life) {
	state = EnemyState.KICKED;
	this.life -= life;
    }

    /**
     * Check if enemy is done for removed.
     * 
     * @param delta
     *            is the time passed.
     * @return true if enemy is done for removed, false otherwise.
     */
    public boolean isDoneForRemove(final float delta) {
	timeToRemove += delta;
	if (timeToRemove >= TIME_TO_REMOVE) {
	    if (beforeRemove) {
		return true;
	    } else {
		beforeRemove = true;
	    }
	}
	return false;
    }

    /**
     * Check if enemy is before remove.
     * 
     * @return true if enemy is before remove, false otherwise.
     */
    public boolean beforeRemove() {
	return beforeRemove;
    }

    /**
     * Get state of enemy.
     * 
     * @return the state of enemy.
     */
    public EnemyState getState() {
	return state;
    }

    /**
     * The Attack of enemy.
     */
    public abstract void attack();

    // CONTROL ATTACKS
    protected Attack attack;
    protected float stateTimeAttack;
    protected float durationAttack;

    /**
     * Obtain the attack of enemy.
     * 
     * @return the attack of enemy.
     */
    public Attack getAttack() {
	return this.attack;
    }

    /**
     * Change attack of enemy
     * 
     * @param attack is the new attack.
     */
    public void setAttack(Attack attack) {
	this.attack = attack;
    }
    
    /**
     * Check if enemy is attacking.
     * 
     * @return true if enemy is attacking, false otherwise.
     */
    public boolean isAttacking() {
	if (state == EnemyState.ATTACK) {
	    return true;
	}
	return false;
    }

    /**
     * Check if animation attack is end.
     * 
     * @param delta
     *            is the time passed.
     */
    public void checkAnimationAttack(float delta) {
	stateTimeAttack += delta;
	if (stateTimeAttack >= durationAttack && durationAttack != 0) {
	    this.state = EnemyState.INACTIVE;
	    durationAttack = 0;
	    stateTimeAttack = 0;
	}
    }
}
