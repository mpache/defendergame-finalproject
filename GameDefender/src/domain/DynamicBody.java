/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package domain;

import com.badlogic.gdx.math.Vector2;

/**
 * DynamicBody.java
 * <p>
 * Class with fields and methods of a body with accelerated movement around the
 * world.
 * </p>
 * 
 * @version Apr 19, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public abstract class DynamicBody extends Body {

    /* Fields */
    // Gravity Y axis
    public static final float DEFAULT_GRAVITY = -15f;
    public static final float MAX_GRAVITY = 100f;
    public static final float MIN_GRAVITY = -100f;
    private float gravity;
    // Acceleration X axis
    public static final float DEFAULT_ACCELERATION = 0f;
    public static final float MAX_ACCELERATION = 100f;
    public static final float MIN_ACCELERATION = -100f;
    private float acceleration;
    // Time world change
    public static final float WORLD_TIME = 10f;

    // Move Fields
    public final Vector2 velocity = new Vector2();
    public final Vector2 initPosition = new Vector2();
    public final Vector2 initVelocity = new Vector2();
    protected boolean isMovingX;
    protected boolean isMovingY;
    protected float time;

    /**
     * Constructor of DynamicBody class.
     * 
     * @param posx
     *            initial position of X axis
     * @param posy
     *            initial position of Y axis
     * @param width
     *            the width of the body
     * @param height
     *            the height of the body
     */
    protected DynamicBody(float posx, float posy, float width, float height) {
	super(posx, posy, width, height);
	this.velocity.set(0, 0);
	this.initVelocity.set(0, 0);
	this.initPosition.set(posx, posy);
	this.time = 0;
	this.isMovingX = false;
	this.isMovingY = true;
	this.gravity = DEFAULT_GRAVITY;
	this.acceleration = DEFAULT_ACCELERATION;
    }

    /**
     * Start the movement of X axis, set initial values for calculate the
     * position and velocity.
     * 
     * @param vel
     *            the velocity to apply at the move
     */
    public void moveX(final float vel) {
	this.isMovingX = true;
	this.initPosition.x = position.x;
	this.initPosition.y = position.y;
	this.initVelocity.x = vel;
	this.time = 0;
    }

    /**
     * If the body is moving stop the X axis move. Set the values of the stop
     * movement.
     */
    public void stopMoveX() {
	if (isMovingX) {
	    this.isMovingX = false;
	    this.velocity.x = 0;
	    this.initVelocity.x = 0;
	    this.initPosition.x = position.x;
	}
    }

    /**
     * Start the movement of Y axis, set initial values for calculate the
     * position and velocity.
     * 
     * @param vel
     *            the velocity to apply at the move
     */
    public void moveY(final float vel) {
	this.isMovingY = true;
	this.initPosition.x = position.x;
	this.initPosition.y = position.y;
	this.initVelocity.y = vel;
	this.time = 0;
    }

    /**
     * If the body is moving stop the Y axis move. Set the values of the stop
     * movement.
     */
    public void stopMoveY() {
	if (isMovingY) {
	    this.isMovingY = false;
	    this.velocity.y = 0;
	    this.initVelocity.y = 0;
	    this.initPosition.y = position.y;
	}
    }

    /**
     * Method for calculate and set the position and velocity of the body. <br>
     * The axis movements are an accelerated movements.
     * 
     * @param delta
     *            is the time elapsed since the last calculation
     */
    public void calculatePositionAndVelocity(float delta) {
	// If any axis is move calculate it
	if (isMovingX || isMovingY) {
	    // Get time for calculate
	    time += delta * DynamicBody.WORLD_TIME;

	    // Calculate the velocity and position of X axis
	    if (isMovingX) {
		position.x = calculatePosition(initPosition.x, initVelocity.x,
			time, acceleration);
		velocity.x = calculateVelocity(initVelocity.x, time,
			acceleration);
	    }

	    // Calculate the velocity and position of Y axis
	    if (isMovingY) {
		position.y = calculatePosition(initPosition.y, initVelocity.y,
			time, gravity);
		velocity.y = calculateVelocity(initVelocity.y, time, gravity);
	    }
	}
	// Test prints
	// System.out.println(velocity.x + " - " + velocity.y + "  **  "
	// + position.x + " - " + position.y);
    }

    /**
     * Function for calculate the position of body with accelerated movement.
     * 
     * @param initialPosition
     *            initial position when the movement start
     * @param initialVelocity
     *            initial velocity when the movement start
     * @param currentTime
     *            the time difference when the movement start
     * @param acceleration
     *            the acceleration of the body
     * 
     * @return the current position of the body
     */
    public float calculatePosition(final float initialPosition,
	    final float initialVelocity, final float currentTime,
	    final float acceleration) {
	// Movement with acceleration
	// R = Ro + Vo * (t - to) + 1/2a(t - to)^2
	final double result = initialPosition + initialVelocity * currentTime
		+ (acceleration / 2) * Math.pow(currentTime, 2);
	return (float) result;
    }

    /**
     * Function for calculate the velocity of body with accelerated movement.
     * 
     * @param initialVelocity
     *            initial velocity when the movement start
     * @param currentTime
     *            the time difference when the movement start
     * @param acceleration
     *            the acceleration of the body
     * 
     * @return the current velocity of the body
     */
    public float calculateVelocity(final float initialVelocity,
	    final float currentTime, final float acceleration) {
	// Movement with acceleration
	// V = Vo + a (t - to)
	final double result = initialVelocity + acceleration * currentTime;
	return (float) result;
    }

    /**
     * Set the gravity to a body. Gravity by default
     * {@link DynamicBody#DEFAULT_GRAVITY}
     * 
     * @param gravity
     *            the gravity to set.
     * @return true if the value of gravity is valid and has changed, false
     *         otherwise.
     */
    public boolean setGravity(float gravity) {
	if (gravity >= MIN_GRAVITY && gravity <= MAX_GRAVITY) {
	    this.gravity = gravity;
	    return true;
	}
	return false;
    }

    /**
     * Get gravity.
     * 
     * @return the actual gravity.
     */
    public float getGravity() {
	return this.gravity;
    }

    /**
     * Set the acceleration to a body. Acceleration by default
     * {@link DynamicBody#DEFAULT_ACCELERATION}
     * 
     * @param acceleration
     *            the acceleration for X axis to set.
     * @return true if the value of acceleration is valid and has changed, false
     *         otherwise.
     */
    public boolean setAcceleration(float acceleration) {
	if (acceleration >= MIN_ACCELERATION
		&& acceleration <= MAX_ACCELERATION) {
	    this.acceleration = acceleration;
	    return true;
	}
	return false;
    }

    /**
     * Get acceleration.
     * 
     * @return actual acceleration.
     */
    public float getAcceleration() {
	return this.acceleration;
    }

    /**
     * Get time.
     * 
     * @return time.
     */
    public float getTime() {
	return this.time;
    }
}
