/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package application.abstraction;


/**
 * IScreen.java
 * 
 * @version Apr 19, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public interface IScreen {

    /**
     * Called when the screen should update itself, e.g. continue a simulation
     * etc.
     * 
     * @param delta
     *            is a float.
     */
    public abstract void update(float delta);

    /**
     * Called when a screen should render itself.
     * 
     * @param delta
     *            is a float.
     */
    public abstract void draw(float delta);

    /**
     * Resize screen.
     * 
     * @param width
     *            is the width of this screen.
     * @param height
     *            is the height of this screen.
     */
    public abstract void resize(float width, float height);

    /**
     * Pause this screen.
     */
    public abstract void pause();

    /**
     * Resume this screen.
     */
    public abstract void resume();

    /**
     * Dispose this screen.
     */
    public abstract void dispose();

    /**
     * Called to check whether the screen is done.
     * 
     * @return true when the screen is done, false otherwise
     */
    public abstract IScreen nextScreen();

    /**
     * Process specific action
     * 
     * @param action
     *            is the action to process.
     * @return true if action is processed, false otherwise.
     */
    public abstract boolean processAction(final int action);

    /**
     * Check if push button back, only in mobile phones.
     * 
     * @return true if button back of mobile phones is pressed, false otherwise.
     */
    public abstract boolean onBackPressed();
}
