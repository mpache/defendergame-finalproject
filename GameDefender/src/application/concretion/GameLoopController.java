/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package application.concretion;

import java.util.Iterator;

import persistence.FactoryObject;
import persistence.entities.AllyEntity;
import persistence.entities.HeroEntity;
import persistence.entities.WorldEntity;
import presentation.abstraction.IUIGameLoop;
import presentation.concretion.game.UIGameLoop;
import service.InputGame;
import service.data.StateManager.GameScore;
import service.data.StateManager.GameState;
import application.abstraction.IScreen;

import com.badlogic.gdx.assets.AssetManager;

import domain.Attack;
import domain.World;
import domain.enemies.Enemy;
import domain.enemies.FlyEnemy;
import domain.heros.Hero;
import domain.items.Item;
import domain.utils.Utils;
import domain.walls.Ground;
import domain.walls.TemporalWall;

/**
 * GameLoop.java
 * 
 * @version Apr 19, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class GameLoopController implements IScreen {

    /* Fields */
    // Game
    private World world;
    private GameState state;
    // UI
    private IUIGameLoop uiGameLoop;
    // Auxiliary fields
    private AssetManager manager;
    private float nextTimeEnemy;
    private float timeEnemy;
    private WorldEntity worldEntity;

    /**
     * GameLoopController constructor.
     * 
     * @param manager
     *            is the asset manager.
     * @param heroEntity
     *            is the entity of hero.
     * @param allyEntity
     *            is the entity of ally.
     * @param worldEntity
     *            is the entity of world.
     */
    public GameLoopController(AssetManager manager, HeroEntity heroEntity,
	    AllyEntity allyEntity, WorldEntity worldEntity) {
	// World
	this.worldEntity = worldEntity;
	this.world = FactoryObject
		.getWorld(worldEntity, heroEntity, allyEntity);
	// Initialized UI
	this.uiGameLoop = new UIGameLoop(this, world.WIDTH);
	// Manager of Assets
	this.manager = manager;
	// Time
	this.nextTimeEnemy = worldEntity.enemyInitialDelay;
	this.timeEnemy = 0;
	state = GameState.PLAYING;
	this.world.initWorld();
    }

    @Override
    public void update(float delta) {
	if (state != GameState.PAUSED) {
	    // Update elements of gameLoop.
	    world.heroUpdate(delta);
	    world.enemiesUpdate(delta);
	    world.allyUpdate(delta);
	    world.itemsUpdate(delta);
	    world.proyectilesUpdate(delta);
	    world.temporalWallUpdate(delta);

	    // create enemies
	    timeEnemy += delta;
	    if (timeEnemy >= nextTimeEnemy && worldEntity.enemies.size() > 0) {
		// calculate next delay
		if (nextTimeEnemy - worldEntity.enemyDelay <= worldEntity.enemyMinimumDelay) {
		    nextTimeEnemy = worldEntity.enemyMinimumDelay;
		} else {
		    nextTimeEnemy -= worldEntity.enemyDelay;
		}
		timeEnemy = 0;
		// add random enemy
		Enemy enemy = FactoryObject.getEnemy(worldEntity);
		if (enemy != null) {
		    randomEnemyPosition(enemy);
		    world.addEnemy(enemy);
		}
	    }

	    // If win
	    if (worldEntity.enemies.size() <= 0 && world.enemiesSize() == 0
		    && state == GameState.PLAYING) {
		// remaining life of the tower and hero
		final float heroRemainingLife = this.world.getHero().getLife()
			* 100 / this.world.getHero().initialLife;
		final float towerRemainingLife = this.world.getTower()
			.getLife() * 100 / this.world.getTower().initialLife;
		// Score
		GameScore score;
		if (heroRemainingLife > 90 && towerRemainingLife > 90) {
		    score = GameScore.STAR3;
		} else if (heroRemainingLife > 60 && towerRemainingLife > 60) {
		    score = GameScore.STAR2;
		} else {
		    score = GameScore.STAR1;
		}
		// win and save the score
		this.uiGameLoop.win(score);
		world.getHero().setWin(true);
		state = GameState.WIN;
	    }

	    // If lose
	    if ((world.getHero().isDead() || world.getTower().isDead())
		    && state == GameState.PLAYING) {
		this.uiGameLoop.gameOver();
		if (!world.getHero().isDead())
		    world.getHero().setLose(true);
		state = GameState.GAME_OVER;
	    }
	}
    }

    private void randomEnemyPosition(Enemy enemy) {
	// side
	if (Utils.randomBoolean()) {
	    enemy.position.x = 0;
	} else {
	    enemy.position.x = worldEntity.width - enemy.getWidth();
	}
	// If is fly enemy set his direction
	if (enemy instanceof FlyEnemy) {
	    ((FlyEnemy) enemy).setDirection(world.getTower().getTopY());
	    enemy.position.y = world.getTower().getTopY() + enemy.getHeight()
		    / 2;
	}

    }

    @Override
    public boolean processAction(final int action) {
	boolean actionProcesed = false;
	if (state == GameState.PLAYING) {
	    switch (action) {
	    case InputGame.Keys.LEFT:
		world.getHero().moveLeft(Hero.WALK_VEL);
		actionProcesed = true;
		break;
	    case InputGame.Keys.RIGHT:
		world.getHero().moveRight(Hero.WALK_VEL);
		actionProcesed = true;
		break;
	    case InputGame.Keys.ATTACK:
		world.getHero().atack();
		break;
	    case InputGame.Keys.JUMP:
		world.getHero().jump(Hero.JUMP_VEL);
		actionProcesed = true;
		break;
	    case InputGame.Keys.SP1:
		if (!world.getHero().isReceiveDamage()) {
		    world.getHero().removeMagic(Hero.MAGIC_SPEND_SP1);
		    world.getHero().specialAttack1();
		    actionProcesed = true;
		}
		break;
	    case InputGame.Keys.SP2:
		if (!world.getHero().isReceiveDamage()) {
		    world.getHero().removeMagic(Hero.MAGIC_SPEND_SP2);
		    world.getHero().specialAttack2();
		    actionProcesed = true;
		}
		break;
	    case InputGame.Keys.SP3:
		if (!world.getHero().isReceiveDamage()) {
		    world.getHero().removeMagic(Hero.MAGIC_SPEND_SP3);
		    world.getHero().specialAttack3();
		    actionProcesed = true;
		}
		break;
	    case InputGame.Keys.PAUSE:
		state = GameState.PAUSED;
		this.uiGameLoop.pauseGame();
		actionProcesed = true;
		break;
	    }
	} else if (action == InputGame.Keys.CONTINUE) {
	    state = GameState.PLAYING;
	    this.uiGameLoop.resumeGame();
	    actionProcesed = true;
	}
	return actionProcesed;
    }

    @Override
    public void draw(float delta) {
	// Render background.
	this.uiGameLoop.renderBackground();

	// Render Grounds.
	for (Iterator<Ground> it = world.getGrounds().iterator(); it.hasNext();) {
	    Ground ground = it.next();
	    this.uiGameLoop.renderGrounds(ground.position.x, ground.position.y,
		    ground.getWidth(), ground.getHeight());
	}
	// Render Tower.
	this.uiGameLoop.renderTower(world.getTower().position.x, world
		.getTower().position.y, world.getTower().getWidth(), world
		.getTower().getHeight());

	// Render ally.
	this.uiGameLoop.renderAlly(world.getAlly().position.x,
		world.getAlly().position.y, world.getAlly().getWidth(), world
			.getAlly().getHeight(), world.getAlly().isFaceRight(),
		world.getAlly().getState(), delta, state);
	// Render enemies.
	for (Iterator<Enemy> it = world.getEnemies().iterator(); it.hasNext();) {
	    Enemy enemy = it.next();
	    this.uiGameLoop.renderEnemies(enemy.position.x, enemy.position.y,
		    enemy.getWidth(), enemy.getHeight(), enemy.isFaceRight(),
		    delta, enemy.getState(), enemy.code, enemy.beforeRemove(),
		    enemy.type, state);
	}
	// Render items.
	for (Iterator<Item> it = world.getItems().iterator(); it.hasNext();) {
	    Item item = it.next();
	    this.uiGameLoop.renderItems(item.getMiddleX(), item.getMiddleY(),
		    item.getWidth(), item.getHeight(), item.getType());
	}
	// Draw stage.
	this.uiGameLoop.draw(delta);
	// update life, magic and creation bar.
	this.uiGameLoop.updateLife(world.getHero().getLife());
	this.uiGameLoop.updateMagic(world.getHero().getMagic());
	this.uiGameLoop.updateCreation(world.getHero().getCreation());
	// update tower.
	this.uiGameLoop.updateTower(world.getTower().getLife());
	// Render hero.
	this.uiGameLoop.renderHero(world.getHero().getState(),
		world.getHero().position.x, world.getHero().position.y, world
			.getHero().getWidth(), world.getHero().getHeight(),
		world.getHero().isFaceRight(), state);
	// Render proyectiles.
	for (Iterator<Attack> it = world.getProyectiles().iterator(); it
		.hasNext();) {
	    Attack p = it.next();
	    this.uiGameLoop.renderProyectiles(p.getHeroState(),
		    p.getEnemyState(), p.position.x, p.position.y,
		    p.getWidth(), p.getHeight(), p.getWidthToRender(),
		    p.getHeightToRender(), delta);
	}
	// Render temporal wall.
	for (Iterator<TemporalWall> it = world.getTemporalWalls().iterator(); it
		.hasNext();) {
	    TemporalWall wall = it.next();
	    this.uiGameLoop.renderTemporalWalls(world.getHero().position.x,
		    wall.getPosX(), wall.getPosY(), wall.getWidth());
	}
    }

    @Override
    public void resize(float width, float height) {
	this.uiGameLoop.resize(width, height);
    }

    @Override
    public void pause() {
	if (state == GameState.PLAYING) {
	    state = GameState.PAUSED;
	    this.uiGameLoop.pauseGame();
	}

    }

    @Override
    public void resume() {
	while (!manager.update()) {
	}
    }

    @Override
    public void dispose() {
	this.uiGameLoop.dispose();
	this.manager.dispose();
    }

    @Override
    public IScreen nextScreen() {
	return this.uiGameLoop.nextScreen();
    }

    @Override
    public boolean onBackPressed() {
	return this.uiGameLoop.onBackPressed();
    }

}
