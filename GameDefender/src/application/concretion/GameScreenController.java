/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package application.concretion;

import persistence.DataAccesObjectCSV;
import presentation.concretion.LoadingScreen;
import service.InputGame;
import service.data.SettingManager;
import service.resources.ResourcesCommos;
import application.abstraction.IScreen;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;

/**
 * GameScreenController.java
 * 
 * @version Apr 19, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class GameScreenController implements ApplicationListener {

    /* Fields */
    private IScreen currentScreen;
    private IScreen nextScreen;
    // FPS logger to testing velocity of application.
//    private FPSLogger log;

    /**
     * GameScreenController.
     * 
     */
    public GameScreenController() {
//	log = new FPSLogger();
    }

    @Override
    public void create() {
	// If application is launch in html5 put your specific DAO.
	if (Gdx.app.getType() == ApplicationType.WebGL) {
	    SettingManager.DAO = DataAccesObjectCSV.getInstance();
	}
	// Initialized common resources and loading screen.
	Gdx.input.setInputProcessor(InputGame.getInputMultiplexer());
	ResourcesCommos.initResources();
	currentScreen = new LoadingScreen(false);
    }

    @Override
    public void dispose() {
	if (currentScreen != null)
	    currentScreen.dispose();
    }

    @Override
    public void render() {
//	log.log();
	// Clean the screen
	Gdx.gl.glClearColor(1, 1, 1, 1);
	Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT
		| GL10.GL_STENCIL_BUFFER_BIT);

	// Update and draw the screen
	currentScreen.update(Gdx.graphics.getDeltaTime());
	currentScreen.draw(Gdx.graphics.getDeltaTime());

	// Check if the current screen is done and set the next screen if return
	// null means that is not done
	nextScreen = currentScreen.nextScreen();
	if (nextScreen != null) {
	    // Dispose the resources of the current screen
	    currentScreen.dispose();
	    // Set the next screen
	    currentScreen = nextScreen;
	}
    }

    @Override
    public void resize(int width, int height) {
	if (currentScreen != null)
	    currentScreen.resize(width, height);
    }

    @Override
    public void pause() {
	if (currentScreen != null)
	    currentScreen.pause();
    }

    @Override
    public void resume() {
	if (currentScreen != null)
	    currentScreen.resume();
    }
}
