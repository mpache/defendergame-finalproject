/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package presentation.concretion;

import java.util.Iterator;

import presentation.abstraction.AStage;
import service.InputGame;
import service.OnBackPressedInput;
import service.data.SettingManager;
import service.data.StateManager.GameScore;
import service.resources.ResourcesMenus;
import application.abstraction.IScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * LevelMenu.java
 * 
 * @version May 26, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class LevelMenu extends AStage implements IScreen {

    /* CONSTANTS */
    private static final float DURATION_FADE_IN = 0.5f;
    private static final float DURATION_FADE_OUT = 0.5f;
    private static final float DURATION_MOVE = 0.5f;
    private static final float TIME_TO_CHANGE_SCREEN = 1f;

    /* ELEMENTS OF THIS SCREEN TYPE */
    private enum ElementsType {
	BACK, LEVEL
    }

    /* Fields */
    private ImageButton buttonBack;
    // LEVELS
    private Table tLevels;
    private ImageButton buttonLevel;
    private GameScore gameScore;

    // Auxiliary fields
    private int level;
    private IScreen tmpNextScreen;
    private float timeToClearAllElements;

    /**
     * CreditsMenu Constructor.
     */
    public LevelMenu() {
	super.initStage();
	this.initComponents();
	this.registerListeners();
	// on Back pressed
	InputGame.getInputMultiplexer().addProcessor(
		new OnBackPressedInput(this));
	Gdx.input.setCatchBackKey(true);
    }

    @Override
    public void update(float delta) {
	updateNextScreen(delta);
    }

    @Override
    public void draw(float delta) {
	stage.act(delta);
	super.draw(delta);
    }

    @Override
    public void pause() {
	// TODO Auto-generated method stub

    }

    @Override
    public void resume() {
	while (!MainMenu.MANAGER.update()) {
	}
    }

    @Override
    public boolean processAction(int action) {
	return false;
    }

    // ===============
    // PRIVATE METHODS
    // ===============
    private void initComponents() {
	// Put back button.
	Table tBack = new Table();
	tBack.setFillParent(true);
	stage.addActor(tBack);
	createElement(ElementsType.BACK);
	tBack.bottom().left().padBottom(20).padLeft(20);
	tBack.add(buttonBack);

	// Put levels
	tLevels = new Table();
	tLevels.setFillParent(true);
	stage.addActor(tLevels);
	tLevels.setX(-stage.getWidth());
	// Create levels.
	for (int i = 0; i < SettingManager.DAO.getWorldsLength(); i++) {
	    level = i;
	    gameScore = SettingManager.DAO.getScore(i);
	    createElement(ElementsType.LEVEL);
	    buttonLevel.setName(i + "");
	    if (level < 10)
		tLevels.add(buttonLevel).pad(20, 20, 20, 20);
	    else
		tLevels.add(buttonLevel).pad(20, 0, 20, 0);
	    if (tLevels.getChildren().size % 6 == 0) {
		tLevels.row();
	    }
	}

	// Create actions of actors.
	this.createActions(ElementsType.BACK);
    }

    @Override
    protected void registerListeners() {
	// BACK
	this.buttonBack.addListener(new InputListener() {

	    @Override
	    public boolean touchDown(InputEvent event, float x, float y,
		    int pointer, int button) {
		// Put sound.
		SettingManager.startSound(SettingManager.back);
		// Create action and put next screen.
		buttonBack.setDisabled(true);
		createActions(ElementsType.BACK);
		tmpNextScreen = new MainMenu(MainMenu.MANAGER);
		return true;
	    }
	});
	// LEVELS
	for (Iterator<Actor> it = tLevels.getChildren().iterator(); it
		.hasNext();) {
	    Actor level = it.next();
	    level.addListener(new ChangeListener() {

		@Override
		public void changed(ChangeEvent event, Actor actor) {
		    // Put sound.
		    SettingManager.startSound(SettingManager.next);
		    SettingManager.temporalWorld = Integer.parseInt(actor
			    .getName());
		    createActions(ElementsType.LEVEL);
		    tmpNextScreen = new CharacterMenu();
		}
	    });
	}
    }

    private void createElement(ElementsType type) {
	Drawable imageUp, imageDown;
	switch (type) {
	case BACK:
	    imageUp = new TextureRegionDrawable(
		    ResourcesMenus.buttonArrowBackUp);
	    imageDown = new TextureRegionDrawable(
		    ResourcesMenus.buttonArrowBackDown);
	    buttonBack = new ImageButton(imageUp, imageDown);
	    break;
	case LEVEL:
	    switch (gameScore) {
	    case BLOCKED:
		imageUp = new TextureRegionDrawable(
			ResourcesMenus.buttonLevelBloqued);
		buttonLevel = new ImageButton(imageUp);
		buttonLevel.setDisabled(true);
		break;
	    case UNBLOCKED:
		imageUp = new TextureRegionDrawable(
			ResourcesMenus.buttonLevelUp);
		imageDown = new TextureRegionDrawable(
			ResourcesMenus.buttonLevelDown);
		buttonLevel = new ImageButton(imageUp, imageDown, imageDown);
		buttonLevel.add(putlevel()).padTop(-120);
		buttonLevel.row();
		buttonLevel.add(putStars(0)).padTop(-45);
		break;
	    case STAR1:
		imageUp = new TextureRegionDrawable(
			ResourcesMenus.buttonLevelUp);
		imageDown = new TextureRegionDrawable(
			ResourcesMenus.buttonLevelDown);
		buttonLevel = new ImageButton(imageUp, imageDown, imageDown);
		buttonLevel.add(putlevel()).padTop(-120);
		buttonLevel.row();
		buttonLevel.add(putStars(1)).padTop(-45);
		break;
	    case STAR2:
		imageUp = new TextureRegionDrawable(
			ResourcesMenus.buttonLevelUp);
		imageDown = new TextureRegionDrawable(
			ResourcesMenus.buttonLevelDown);
		buttonLevel = new ImageButton(imageUp, imageDown);
		buttonLevel.add(putlevel()).padTop(-120);
		buttonLevel.row();
		buttonLevel.add(putStars(2)).padTop(-45);
		break;
	    case STAR3:
		imageUp = new TextureRegionDrawable(
			ResourcesMenus.buttonLevelUp);
		imageDown = new TextureRegionDrawable(
			ResourcesMenus.buttonLevelDown);
		buttonLevel = new ImageButton(imageUp, imageDown);
		buttonLevel.add(putlevel()).padTop(-120);
		buttonLevel.row();
		buttonLevel.add(putStars(3)).padTop(-45);
		break;
	    }
	    break;
	}
    }

    private void createActions(ElementsType type) {
	switch (type) {
	case BACK:
	    if (!buttonBack.isPressed()) {
		buttonBack.addAction(Actions.fadeOut(0f));
		buttonBack.addAction(Actions.fadeIn(DURATION_FADE_IN));
		tLevels.addAction(Actions.moveTo(0, 0, DURATION_MOVE));
	    } else {
		tLevels.addAction(Actions.moveTo(-stage.getWidth(), 0,
			DURATION_MOVE));
		buttonBack.addAction(Actions.fadeOut(DURATION_FADE_OUT));
	    }
	    break;
	case LEVEL:
	    buttonBack.addAction(Actions.fadeOut(DURATION_FADE_OUT));
	    tLevels.addAction(Actions.moveTo(stage.getWidth(), 0, DURATION_MOVE));
	    break;
	}
    }

    private Actor putlevel() {
	Image image = null;
	Table t = new Table();
	if (level > 10)
	    level = (int) level / 10;
	// Calculate first number.
	image = level == 0 ? new Image(ResourcesMenus.num0) : image;
	image = level == 1 ? new Image(ResourcesMenus.num1) : image;
	image = level == 2 ? new Image(ResourcesMenus.num2) : image;
	image = level == 3 ? new Image(ResourcesMenus.num3) : image;
	image = level == 4 ? new Image(ResourcesMenus.num4) : image;
	image = level == 5 ? new Image(ResourcesMenus.num5) : image;
	image = level == 6 ? new Image(ResourcesMenus.num6) : image;
	image = level == 7 ? new Image(ResourcesMenus.num7) : image;
	image = level == 8 ? new Image(ResourcesMenus.num8) : image;
	image = level == 9 ? new Image(ResourcesMenus.num9) : image;
	if (level < 10) {
	    t.add(image);
	} else {
	    // Calculate second number.
	    double l2 = Math.round((level / 10. - (int) (level / 10)) * 10);
	    Image image2 = null;
	    image2 = l2 == 0 ? new Image(ResourcesMenus.num0) : image2;
	    image2 = l2 == 1 ? new Image(ResourcesMenus.num1) : image2;
	    image2 = l2 == 2 ? new Image(ResourcesMenus.num2) : image2;
	    image2 = l2 == 3 ? new Image(ResourcesMenus.num3) : image2;
	    image2 = l2 == 4 ? new Image(ResourcesMenus.num4) : image2;
	    image2 = l2 == 5 ? new Image(ResourcesMenus.num5) : image2;
	    image2 = l2 == 6 ? new Image(ResourcesMenus.num6) : image2;
	    image2 = l2 == 7 ? new Image(ResourcesMenus.num7) : image2;
	    image2 = l2 == 8 ? new Image(ResourcesMenus.num8) : image2;
	    image2 = l2 == 9 ? new Image(ResourcesMenus.num9) : image2;
	    t.add(image).padLeft(10);
	    t.add(image2).padLeft(-20);
	}
	return t;
    }

    private Actor putStars(int stars) {
	Table t = new Table();
	Image star;
	switch (stars) {
	case 0:
	    star = new Image(ResourcesMenus.noStar);
	    t.add(star);
	    star = new Image(ResourcesMenus.noStar);
	    t.add(star);
	    star = new Image(ResourcesMenus.noStar);
	    t.add(star);
	    break;
	case 1:
	    star = new Image(ResourcesMenus.star);
	    t.add(star);
	    star = new Image(ResourcesMenus.noStar);
	    t.add(star);
	    star = new Image(ResourcesMenus.noStar);
	    t.add(star);
	    break;
	case 2:
	    star = new Image(ResourcesMenus.star);
	    t.add(star);
	    star = new Image(ResourcesMenus.star);
	    t.add(star);
	    star = new Image(ResourcesMenus.noStar);
	    t.add(star);
	    break;
	case 3:
	    star = new Image(ResourcesMenus.star);
	    t.add(star);
	    star = new Image(ResourcesMenus.star);
	    t.add(star);
	    star = new Image(ResourcesMenus.star);
	    t.add(star);
	    break;
	default:
	    break;
	}
	return t;
    }

    private void updateNextScreen(float delta) {
	timeToClearAllElements += tmpNextScreen != null ? delta : 0;
	if (tmpNextScreen != null
		&& timeToClearAllElements >= TIME_TO_CHANGE_SCREEN) {
	    this.clearElements();
	    super.nextScreen = tmpNextScreen;
	}
    }

    private void clearElements() {
	this.buttonBack.clear();
	this.tLevels.clear();
    }

    @Override
    public boolean onBackPressed() {
	tLevels.addAction(Actions.moveTo(-stage.getWidth(), 0, DURATION_MOVE));
	buttonBack.addAction(Actions.fadeOut(DURATION_FADE_OUT));
	tmpNextScreen = new MainMenu(MainMenu.MANAGER);
	return true;
    }
}
