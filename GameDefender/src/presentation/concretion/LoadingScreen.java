/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package presentation.concretion;

import java.util.HashSet;
import java.util.Iterator;

import persistence.entities.AllyEntity;
import persistence.entities.EnemyEntity;
import persistence.entities.HeroEntity;
import persistence.entities.WorldEntity;
import service.data.SettingManager;
import service.data.TypeManager.EnemyType;
import service.resources.ResourceAlly;
import service.resources.ResourceHeros;
import service.resources.ResourcesEnemies;
import service.resources.ResourcesHUD;
import service.resources.ResourcesManager;
import service.resources.ResourcesMenus;
import service.resources.ResourcesWorld;
import application.abstraction.IScreen;
import application.concretion.GameLoopController;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * LoadingScreen.java
 * 
 * @version May 26, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class LoadingScreen implements IScreen {

    private boolean isModeGameLoop;
    private AssetManager manager;
    private IScreen nextScreen;
    // Entities
    private HeroEntity hero;
    private AllyEntity ally;
    private WorldEntity world;
    // Draw
    private SpriteBatch batch;
    private OrthographicCamera camera;
    private Texture splash;

    /**
     * Loading screen for load the assets of a screen
     * 
     * @param isModeGameLoop
     *            the screen to load if true load the Game Loop Screen, false
     *            load the Main Menu screen.
     */
    public LoadingScreen(boolean isModeGameLoop) {
	Gdx.input.setCatchBackKey(false);
	this.isModeGameLoop = isModeGameLoop;
	SettingManager.disposeAudio();
	initResources();
	if (isModeGameLoop) {
	    // Get Manager
	    hero = SettingManager.DAO
		    .getHeroEntity(SettingManager.temporalHero);
	    ally = SettingManager.DAO
		    .getAllyEntity(SettingManager.temporalAlly);
	    world = SettingManager.DAO
		    .getWorldEntity(SettingManager.temporalWorld);
	    manager = ResourcesManager.getGameLoopAssetManager(hero, ally,
		    world);
	} else {
	    manager = ResourcesManager.getMenuAssetManager();
	}

    }

    private void initResources() {
	splash = new Texture(Gdx.files.internal("data/loading.png"));
	batch = new SpriteBatch();
	camera = new OrthographicCamera(480, 320);
	camera.setToOrtho(false, 480, 320);

    }

    @Override
    public void update(float delta) {
	if (manager.update()) {
	    if (isModeGameLoop) {
		startGameLoop();
	    } else {
		startMenus();
	    }
	}
    }

    /*
     * MAIN MENU
     */

    private void startMenus() {
	ResourcesMenus.initResources(manager);
	nextScreen = new MainMenu(manager);
    }

    /*
     * GAME LOOP
     */

    private void startGameLoop() {
	ResourcesHUD.initResources(manager);
	ResourcesWorld.initResources(manager);
	ResourceHeros.initResource(manager, hero.type);
	ResourceAlly.initResources(manager);
	initEnemiesResources();
	nextScreen = new GameLoopController(manager, hero, ally, world);
    }

    private void initEnemiesResources() {
	HashSet<EnemyType> types = new HashSet<EnemyType>();
	for (Iterator<EnemyEntity> it = world.enemies.iterator(); it.hasNext();) {
	    EnemyEntity entity = it.next();
	    if (types.add(entity.type)) {
		switch (entity.type) {
		case GHOST_ARMOR:
		    ResourcesEnemies.initGhostArmorResources(manager);
		    break;
		case ELF:
		    break;
		case FIRE_LION:
		    ResourcesEnemies.initFireLionResources(manager);
		    break;
		case BAT:
		    ResourcesEnemies.initBatResources(manager);
		    break;

		}
	    }
	}

    }

    @Override
    public void draw(float delta) {
	// System.out.println("LOADING... " + progress);
	camera.update();
	batch.setProjectionMatrix(camera.combined);
	batch.begin();
	batch.draw(splash, 0, 0, 480, 320);
	batch.end();
    }

    @Override
    public void resize(float width, float height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
	batch.dispose();
	splash.dispose();
    }

    @Override
    public IScreen nextScreen() {
	return nextScreen;
    }

    @Override
    public boolean processAction(int action) {
	return false;
    }

    @Override
    public boolean onBackPressed() {
	return false;
    }
}
