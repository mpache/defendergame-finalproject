/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package presentation.concretion;

import presentation.abstraction.AStage;
import service.InputGame;
import service.OnBackPressedInput;
import service.data.SettingManager;
import service.data.TypeManager.AllyType;
import service.data.TypeManager.HeroType;
import service.resources.ResourcesMenus;
import application.abstraction.IScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * CharacterMenu.java
 *
 * @version May 26, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 *
 */
public class CharacterMenu extends AStage implements IScreen {

    /* CONSTANTS */
    private static final float DURATION_FADE_IN = 0.5f;
    private static final float DURATION_FADE_OUT = 0.5f;
    private static final float DURATION_MOVE = 0.5f;
    private static final float TIME_TO_CHANGE_SCREEN = 1f;

    /* ELEMENTS OF THIS SCREEN TYPE */
    private enum ElementsType {
	BUTTON_HEROS, BUTTON_ALLIES, MENU_HERO, MENU_ALLIES, DESCRIPTION_HERO, DESCRIPTION_ALLIES, BACK, NEXT, PLAY
    }

    /* Fields */
    private ImageButton buttonBack, buttonNext, buttonPlay;
    // Heros
    private Table tButtonHeros, tMenuHero, tDescriptionHero;
    private ImageButton buttonHero1, buttonHero2, buttonHero3, buttonHero4;
    private Image menuHero1, menuHero2, menuHero3, menuHero4;
    private Image descriptionHero1, descriptionHero2, descriptionHero3,
	    descriptionHero4;
    // Allies
    private Table tButtonAllies, tMenuAllies, tDescriptionAllies;
    private ImageButton buttonAllyAttack, buttonAllyNeutral, buttonAllyDefense;
    private Image menuAllyAttack, menuAllyNeutral, menuAllyDefense;
    private Image descriptionAllyAttack, descriptionAllyNeutral,
	    descriptionAllyDefense;

    // Auxiliary fields
    private IScreen tmpNextScreen;
    private float timeToClearAllElements;

    /**
     * CreditsMenu Constructor.
     */
    public CharacterMenu() {
	super.initStage();
	this.initComponentsHeroSelected();
	this.registerListenersHeroSelected();
	// set input
	InputGame.getInputMultiplexer().addProcessor(
		new OnBackPressedInput(this));
	Gdx.input.setCatchBackKey(true);
    }

    @Override
    public void update(float delta) {
	updateNextScreen(delta);
	clearActorsUnUsedInStage(delta);
    }

    @Override
    public void draw(float delta) {
	stage.act(delta);
	super.draw(delta);
    }

    @Override
    public void pause() {
	// TODO Auto-generated method stub
    }

    @Override
    public void resume() {
	while (!MainMenu.MANAGER.update()) {
	}
    }

    @Override
    public boolean processAction(int action) {
	return false;
    }

    @Override
    protected void registerListeners() {

    }
    
    // ===============
    // PRIVATE METHODS
    // ===============
    private void initComponentsHeroSelected() {
	Table tmpTable;
	// create and put button Back
	tmpTable = new Table();
	tmpTable.setFillParent(true);
	stage.addActor(tmpTable);
	this.createElement(ElementsType.BACK);
	tmpTable.bottom().left().padBottom(20).padLeft(20);
	tmpTable.add(this.buttonBack);

	// create and put button next
	tmpTable = new Table();
	tmpTable.setFillParent(true);
	stage.addActor(tmpTable);
	this.createElement(ElementsType.NEXT);
	tmpTable.bottom().right().padBottom(20).padRight(20);
	tmpTable.add(this.buttonNext);

	// Create Table of menu hero.
	tMenuHero = new Table();
	tMenuHero.setFillParent(true);
	stage.addActor(tMenuHero);
	this.createElement(ElementsType.MENU_HERO);
	tMenuHero.bottom().left().padLeft(180);

	// Create Table of description hero.
	tDescriptionHero = new Table();
	tDescriptionHero.setFillParent(true);
	stage.addActor(tDescriptionHero);
	this.createElement(ElementsType.DESCRIPTION_HERO);
	tDescriptionHero.center().right();

	// Create Buttons of hero.
	float i = tButtonHeros != null
		&& tButtonHeros.getX() >= stage.getWidth() ? stage.getWidth()
		: -stage.getWidth();
	tButtonHeros = new Table();
	tButtonHeros.setFillParent(true);
	stage.addActor(tButtonHeros);
	tButtonHeros.setX(i);
	this.createElement(ElementsType.BUTTON_HEROS);
	tButtonHeros.top().toFront();
	tButtonHeros.add(buttonHero1);
	tButtonHeros.add(buttonHero2);
	tButtonHeros.add(buttonHero3);
	tButtonHeros.add(buttonHero4);

	// Initialized first actions
	this.createActions(ElementsType.BACK);
	this.createActions(ElementsType.BUTTON_HEROS);
    }

    private void initComponenstsAllySelected() {
	Table tmpTable;
	// create and put button Back
	tmpTable = new Table();
	tmpTable.setFillParent(true);
	stage.addActor(tmpTable);
	buttonBack.clear();
	this.createElement(ElementsType.BACK);
	tmpTable.bottom().left().padBottom(20).padLeft(20);
	tmpTable.add(this.buttonBack);

	// create and put button next
	tmpTable = new Table();
	tmpTable.setFillParent(true);
	stage.addActor(tmpTable);
	this.createElement(ElementsType.PLAY);
	tmpTable.bottom().right().padBottom(20).padRight(20);
	tmpTable.add(this.buttonPlay);

	// Create Table of menu allies.
	tMenuAllies = new Table();
	tMenuAllies.setFillParent(true);
	stage.addActor(tMenuAllies);
	this.createElement(ElementsType.MENU_ALLIES);
	tMenuAllies.bottom().left().padLeft(180);

	// Create Table of description allies.
	tDescriptionAllies = new Table();
	tDescriptionAllies.setFillParent(true);
	stage.addActor(tDescriptionAllies);
	this.createElement(ElementsType.DESCRIPTION_ALLIES);
	tDescriptionAllies.center().padTop(150).padLeft(300);

	// Create Buttons of allies.
	tButtonAllies = new Table();
	tButtonAllies.setFillParent(true);
	stage.addActor(tButtonAllies);
	tButtonAllies.setX(-stage.getWidth());
	this.createElement(ElementsType.BUTTON_ALLIES);
	tButtonAllies.top().toFront();
	tButtonAllies.add(buttonAllyAttack);
	tButtonAllies.add(buttonAllyNeutral);
	tButtonAllies.add(buttonAllyDefense);

	// Initialized first actions
	this.createActions(ElementsType.BUTTON_ALLIES);
    }
    
    private void registerListenersHeroSelected() {
	// BACK
	this.buttonBack.addListener(new InputListener() {

	    @Override
	    public boolean touchDown(InputEvent event, float x, float y,
		    int pointer, int button) {
		SettingManager.startSound(SettingManager.back);
		createActions(ElementsType.BUTTON_HEROS);
		createActions(ElementsType.MENU_HERO);
		createActions(ElementsType.DESCRIPTION_HERO);
		createActions(ElementsType.BACK);
		createActions(ElementsType.NEXT);
		tmpNextScreen = new LevelMenu();
		return true;
	    }
	});
	// NEXT
	this.buttonNext.addListener(new InputListener() {

	    @Override
	    public boolean touchDown(InputEvent event, float x, float y,
		    int pointer, int button) {
		if (!buttonNext.isDisabled()) {
		    SettingManager.startSound(SettingManager.next);
		    buttonNext.setDisabled(true);
		    // Put actions of actors.
		    createActions(ElementsType.NEXT);
		    createActions(ElementsType.BUTTON_HEROS);
		    createActions(ElementsType.MENU_HERO);
		    createActions(ElementsType.DESCRIPTION_HERO);
		    // initialized Components of allies.
		    initComponenstsAllySelected();
		    registerListenersAllySelected();
		}
		return true;
	    }
	});
	// BUTTON HERO 1.
	this.buttonHero1.addListener(new ChangeListener() {

	    @Override
	    public void changed(ChangeEvent event, Actor actor) {
		if (!menuHero1.isVisible()) {
		    SettingManager.startSound(SettingManager.select);
		    buttonHero1.setDisabled(true);
		    buttonHero2.setDisabled(false);
		    buttonHero3.setDisabled(false);
		    buttonHero4.setDisabled(false);
		    // Re-push heros.
		    if (menuHero2.isVisible())
			buttonHero2.toggle();
		    if (menuHero3.isVisible())
			buttonHero3.toggle();
		    if (menuHero4.isVisible())
			buttonHero4.toggle();
		    // Menu Hero.
		    menuHero1.setVisible(true);
		    menuHero2.setVisible(false);
		    menuHero3.setVisible(false);
		    menuHero4.setVisible(false);
		    tMenuHero.clear();
		    tMenuHero.add(menuHero1);
		    // Description
		    descriptionHero1.setVisible(true);
		    descriptionHero2.setVisible(false);
		    descriptionHero3.setVisible(false);
		    descriptionHero4.setVisible(false);
		    tDescriptionHero.clear();
		    tDescriptionHero.add(descriptionHero1);
		    // Create actions.
		    createActions(ElementsType.MENU_HERO);
		    createActions(ElementsType.DESCRIPTION_HERO);
		    // Define hero type
		    SettingManager.temporalHero = HeroType.MAGE;
		    // Put button next.
		    buttonNext.setVisible(true);
		    buttonNext.setDisabled(false);
		    createActions(ElementsType.NEXT);
		}
	    }
	});
	// BUTTON HERO 2.
	this.buttonHero2.addListener(new ChangeListener() {

	    @Override
	    public void changed(ChangeEvent event, Actor actor) {
		if (!menuHero2.isVisible()) {
		    SettingManager.startSound(SettingManager.select);
		    buttonHero1.setDisabled(false);
		    buttonHero2.setDisabled(true);
		    buttonHero3.setDisabled(false);
		    buttonHero4.setDisabled(false);
		    // Re-push heros.
		    if (menuHero1.isVisible())
			buttonHero1.toggle();
		    if (menuHero3.isVisible())
			buttonHero3.toggle();
		    if (menuHero4.isVisible())
			buttonHero4.toggle();
		    // Menu hero.
		    menuHero1.setVisible(false);
		    menuHero2.setVisible(true);
		    menuHero3.setVisible(false);
		    menuHero4.setVisible(false);
		    tMenuHero.clear();
		    tMenuHero.add(menuHero2);
		    // Description
		    descriptionHero1.setVisible(false);
		    descriptionHero2.setVisible(true);
		    descriptionHero3.setVisible(false);
		    descriptionHero4.setVisible(false);
		    tDescriptionHero.clear();
		    tDescriptionHero.add(descriptionHero2);
		    // Create actions.
		    createActions(ElementsType.MENU_HERO);
		    createActions(ElementsType.DESCRIPTION_HERO);
		    // Define hero type
		    SettingManager.temporalHero = HeroType.WARRIOR;
		    // Put button next.
		    buttonNext.setVisible(true);
		    buttonNext.setDisabled(false);
		    createActions(ElementsType.NEXT);
		}
	    }
	});
	// BUTTON HERO 3.
	this.buttonHero3.addListener(new ChangeListener() {

	    @Override
	    public void changed(ChangeEvent event, Actor actor) {
		if (!menuHero3.isVisible()) {
		    SettingManager.startSound(SettingManager.select);
		    buttonHero1.setDisabled(false);
		    buttonHero2.setDisabled(false);
		    buttonHero3.setDisabled(true);
		    buttonHero4.setDisabled(false);
		    // Re-push heros.
		    if (menuHero1.isVisible())
			buttonHero1.toggle();
		    if (menuHero2.isVisible())
			buttonHero2.toggle();
		    if (menuHero4.isVisible())
			buttonHero4.toggle();
		    // Menu Hero
		    menuHero1.setVisible(false);
		    menuHero2.setVisible(false);
		    menuHero3.setVisible(true);
		    menuHero4.setVisible(false);
		    tMenuHero.clear();
		    tMenuHero.add(menuHero3);
		    // Description
		    descriptionHero1.setVisible(false);
		    descriptionHero2.setVisible(false);
		    descriptionHero3.setVisible(true);
		    descriptionHero4.setVisible(false);
		    tDescriptionHero.clear();
		    tDescriptionHero.add(descriptionHero3);
		    // Create actions.
		    createActions(ElementsType.MENU_HERO);
		    createActions(ElementsType.DESCRIPTION_HERO);
		    // Define hero type
		    SettingManager.temporalHero = HeroType.MAGE;
		    // Put button next.
		    buttonNext.setVisible(true);
		    buttonNext.setDisabled(false);
		    createActions(ElementsType.NEXT);
		}
	    }
	});
	// BUTTON HERO 4.
	this.buttonHero4.addListener(new ChangeListener() {

	    @Override
	    public void changed(ChangeEvent event, Actor actor) {
		if (!menuHero4.isVisible()) {
		    SettingManager.startSound(SettingManager.select);
		    buttonHero1.setDisabled(false);
		    buttonHero2.setDisabled(false);
		    buttonHero3.setDisabled(false);
		    buttonHero4.setDisabled(true);
		    // Re-push heros.
		    if (menuHero1.isVisible())
			buttonHero1.toggle();
		    if (menuHero2.isVisible())
			buttonHero2.toggle();
		    if (menuHero3.isVisible())
			buttonHero3.toggle();
		    // Menu heros.
		    menuHero1.setVisible(false);
		    menuHero2.setVisible(false);
		    menuHero3.setVisible(false);
		    menuHero4.setVisible(true);
		    tMenuHero.clear();
		    tMenuHero.add(menuHero4);
		    // Description
		    descriptionHero1.setVisible(false);
		    descriptionHero2.setVisible(false);
		    descriptionHero3.setVisible(false);
		    descriptionHero4.setVisible(true);
		    tDescriptionHero.clear();
		    tDescriptionHero.add(descriptionHero4);
		    // Create actions.
		    createActions(ElementsType.MENU_HERO);
		    createActions(ElementsType.DESCRIPTION_HERO);
		    // Define hero type
		    SettingManager.temporalHero = HeroType.MAGE;
		    // Put button next.
		    buttonNext.setVisible(true);
		    buttonNext.setDisabled(false);
		    createActions(ElementsType.NEXT);
		}
	    }
	});
    }

    private void registerListenersAllySelected() {
	// BACK
	this.buttonBack.addListener(new InputListener() {

	    @Override
	    public boolean touchDown(InputEvent event, float x, float y,
		    int pointer, int button) {
		SettingManager.startSound(SettingManager.back);
		buttonPlay.setDisabled(true);
		// Create actions of actors.
		createActions(ElementsType.NEXT);
		createActions(ElementsType.PLAY);
		createActions(ElementsType.BUTTON_ALLIES);
		createActions(ElementsType.MENU_ALLIES);
		createActions(ElementsType.DESCRIPTION_ALLIES);
		// Initialized components of heros
		tButtonHeros.setX(stage.getWidth());
		initComponentsHeroSelected();
		registerListenersHeroSelected();
		return true;
	    }
	});
	// PLAY
	this.buttonPlay.addListener(new InputListener() {

	    @Override
	    public boolean touchDown(InputEvent event, float x, float y,
		    int pointer, int button) {
		if (!buttonPlay.isDisabled()) {
		    SettingManager.startSound(SettingManager.play);
		    createActions(ElementsType.BACK);
		    createActions(ElementsType.PLAY);
		    createActions(ElementsType.BUTTON_ALLIES);
		    createActions(ElementsType.MENU_ALLIES);
		    createActions(ElementsType.DESCRIPTION_ALLIES);
		    tmpNextScreen = new LoadingScreen(true);
		}
		return true;
	    }
	});
	// BUTTON ALLY ATTACK
	this.buttonAllyAttack.addListener(new ChangeListener() {

	    @Override
	    public void changed(ChangeEvent event, Actor actor) {
		if (!menuAllyAttack.isVisible()) {
		    SettingManager.startSound(SettingManager.select);
		    buttonAllyAttack.setDisabled(true);
		    buttonAllyNeutral.setDisabled(false);
		    buttonAllyDefense.setDisabled(false);
		    // Re-push heros.
		    if (menuAllyNeutral.isVisible())
			buttonAllyNeutral.toggle();
		    if (menuAllyDefense.isVisible())
			buttonAllyDefense.toggle();
		    // Menu heros.
		    menuAllyAttack.setVisible(true);
		    menuAllyNeutral.setVisible(false);
		    menuAllyDefense.setVisible(false);
		    tMenuAllies.clear();
		    tMenuAllies.add(menuAllyAttack);
		    // Description
		    descriptionAllyAttack.setVisible(true);
		    descriptionAllyNeutral.setVisible(false);
		    descriptionAllyDefense.setVisible(false);
		    tDescriptionAllies.clear();
		    tDescriptionAllies.add(descriptionAllyAttack);
		    // Create actions.
		    createActions(ElementsType.MENU_ALLIES);
		    createActions(ElementsType.DESCRIPTION_ALLIES);
		    // Define hero type
		    SettingManager.temporalAlly = AllyType.ATTACK;
		    // Put button next.
		    buttonPlay.setVisible(true);
		    buttonPlay.setDisabled(false);
		    createActions(ElementsType.PLAY);
		}
	    }
	});
	// BUTTON ALLY NEUTRAL
	this.buttonAllyNeutral.addListener(new ChangeListener() {

	    @Override
	    public void changed(ChangeEvent event, Actor actor) {
		if (!menuAllyNeutral.isVisible()) {
		    SettingManager.startSound(SettingManager.select);
		    buttonAllyAttack.setDisabled(false);
		    buttonAllyNeutral.setDisabled(true);
		    buttonAllyDefense.setDisabled(false);
		    // Re-push heros.
		    if (menuAllyAttack.isVisible())
			buttonAllyAttack.toggle();
		    if (menuAllyDefense.isVisible())
			buttonAllyDefense.toggle();
		    // Menu heros.
		    menuAllyAttack.setVisible(false);
		    menuAllyNeutral.setVisible(true);
		    menuAllyDefense.setVisible(false);
		    tMenuAllies.clear();
		    tMenuAllies.add(menuAllyNeutral);
		    // Description
		    descriptionAllyAttack.setVisible(false);
		    descriptionAllyNeutral.setVisible(true);
		    descriptionAllyDefense.setVisible(false);
		    tDescriptionAllies.clear();
		    tDescriptionAllies.add(descriptionAllyNeutral);
		    // Create actions.
		    createActions(ElementsType.MENU_ALLIES);
		    createActions(ElementsType.DESCRIPTION_ALLIES);
		    // Define hero type
		    SettingManager.temporalAlly = AllyType.NEUTRAL;
		    // Put button next.
		    buttonPlay.setVisible(true);
		    buttonPlay.setDisabled(false);
		    createActions(ElementsType.PLAY);
		}
	    }
	});
	// BUTTON ALLY DEFENSE
	this.buttonAllyDefense.addListener(new ChangeListener() {

	    @Override
	    public void changed(ChangeEvent event, Actor actor) {
		if (!menuAllyDefense.isVisible()) {
		    SettingManager.startSound(SettingManager.select);
		    buttonAllyAttack.setDisabled(false);
		    buttonAllyNeutral.setDisabled(false);
		    buttonAllyDefense.setDisabled(true);
		    // Re-push heros.
		    if (menuAllyAttack.isVisible())
			buttonAllyAttack.toggle();
		    if (menuAllyNeutral.isVisible())
			buttonAllyNeutral.toggle();
		    // Menu heros.
		    menuAllyAttack.setVisible(false);
		    menuAllyNeutral.setVisible(false);
		    menuAllyDefense.setVisible(true);
		    tMenuAllies.clear();
		    tMenuAllies.add(menuAllyDefense);
		    // Description
		    descriptionAllyAttack.setVisible(false);
		    descriptionAllyNeutral.setVisible(false);
		    descriptionAllyDefense.setVisible(true);
		    tDescriptionAllies.clear();
		    tDescriptionAllies.add(descriptionAllyDefense);
		    // Create actions.
		    createActions(ElementsType.MENU_ALLIES);
		    createActions(ElementsType.DESCRIPTION_ALLIES);
		    // Define hero type
		    SettingManager.temporalAlly = AllyType.DEFENSE;
		    // Put button next.
		    buttonPlay.setVisible(true);
		    buttonPlay.setDisabled(false);
		    createActions(ElementsType.PLAY);
		}
	    }
	});
    }

    // -------------------------------------------
    // ELEMENTS
    // -------------------------------------------
    private void createElement(ElementsType type) {
	Drawable imageUp, imageDown;
	switch (type) {
	case BACK:
	    imageUp = new TextureRegionDrawable(
		    ResourcesMenus.buttonArrowBackUp);
	    imageDown = new TextureRegionDrawable(
		    ResourcesMenus.buttonArrowBackDown);
	    buttonBack = new ImageButton(imageUp, imageDown);
	    break;
	case NEXT:
	    imageUp = new TextureRegionDrawable(
		    ResourcesMenus.buttonArrowNextUp);
	    imageDown = new TextureRegionDrawable(
		    ResourcesMenus.buttonArrowNextDown);
	    buttonNext = new ImageButton(imageUp, imageDown);
	    buttonNext.setDisabled(true);
	    buttonNext.setVisible(false);
	    break;
	case PLAY:
	    imageUp = new TextureRegionDrawable(ResourcesMenus.buttonPlayUp);
	    imageDown = new TextureRegionDrawable(ResourcesMenus.buttonPlayDown);
	    buttonPlay = new ImageButton(imageUp, imageDown);
	    buttonPlay.setDisabled(true);
	    buttonPlay.setVisible(false);
	    break;
	case BUTTON_HEROS:
	    // Hero 1
	    imageUp = new TextureRegionDrawable(ResourcesMenus.buttonHero1Up);
	    imageDown = new TextureRegionDrawable(
		    ResourcesMenus.buttonHero1Down);
	    buttonHero1 = new ImageButton(imageUp, imageDown, imageDown);
	    // Hero 2
	    imageUp = new TextureRegionDrawable(ResourcesMenus.buttonHero2Up);
	    imageDown = new TextureRegionDrawable(
		    ResourcesMenus.buttonHero2Down);
	    buttonHero2 = new ImageButton(imageUp, imageDown, imageDown);
	    // Hero 3
	    imageUp = new TextureRegionDrawable(ResourcesMenus.buttonHero3Up);
	    imageDown = new TextureRegionDrawable(
		    ResourcesMenus.buttonHero3Down);
	    buttonHero3 = new ImageButton(imageUp, imageDown, imageDown);
	    // Hero 4
	    imageUp = new TextureRegionDrawable(ResourcesMenus.buttonHero4Up);
	    imageDown = new TextureRegionDrawable(
		    ResourcesMenus.buttonHero4Down);
	    buttonHero4 = new ImageButton(imageUp, imageDown, imageDown);
	    break;
	case DESCRIPTION_HERO:
	    // Hero 1
	    imageUp = new TextureRegionDrawable(ResourcesMenus.descriptionHero1);
	    descriptionHero1 = new Image(imageUp);
	    descriptionHero1.setVisible(false);
	    // Hero 2
	    imageUp = new TextureRegionDrawable(ResourcesMenus.descriptionHero2);
	    descriptionHero2 = new Image(imageUp);
	    descriptionHero2.setVisible(false);
	    // Hero 3
	    imageUp = new TextureRegionDrawable(ResourcesMenus.descriptionHero3);
	    descriptionHero3 = new Image(imageUp);
	    descriptionHero3.setVisible(false);
	    // Hero 4
	    imageUp = new TextureRegionDrawable(ResourcesMenus.descriptionHero4);
	    descriptionHero4 = new Image(imageUp);
	    descriptionHero4.setVisible(false);
	    break;
	case MENU_HERO:
	    // Hero 1
	    imageUp = new TextureRegionDrawable(ResourcesMenus.menuHero1);
	    menuHero1 = new Image(imageUp);
	    menuHero1.setVisible(false);
	    // Hero 2
	    imageUp = new TextureRegionDrawable(ResourcesMenus.menuHero2);
	    menuHero2 = new Image(imageUp);
	    menuHero2.setVisible(false);
	    // Hero 3
	    imageUp = new TextureRegionDrawable(ResourcesMenus.menuHero3);
	    menuHero3 = new Image(imageUp);
	    menuHero3.setVisible(false);
	    // Hero 4
	    imageUp = new TextureRegionDrawable(ResourcesMenus.menuHero4);
	    menuHero4 = new Image(imageUp);
	    menuHero4.setVisible(false);
	    break;
	case BUTTON_ALLIES:
	    // Ally Attack
	    imageUp = new TextureRegionDrawable(
		    ResourcesMenus.buttonAllyAttackUp);
	    imageDown = new TextureRegionDrawable(
		    ResourcesMenus.buttonAllyAttackDown);
	    buttonAllyAttack = new ImageButton(imageUp, imageDown, imageDown);
	    // Ally Neutral
	    imageUp = new TextureRegionDrawable(
		    ResourcesMenus.buttonAllyNeutralUp);
	    imageDown = new TextureRegionDrawable(
		    ResourcesMenus.buttonAllyNeutralDown);
	    buttonAllyNeutral = new ImageButton(imageUp, imageDown, imageDown);
	    // Ally Defense
	    imageUp = new TextureRegionDrawable(
		    ResourcesMenus.buttonAllyDefenseUp);
	    imageDown = new TextureRegionDrawable(
		    ResourcesMenus.buttonAllyDefenseDown);
	    buttonAllyDefense = new ImageButton(imageUp, imageDown, imageDown);
	    break;
	case MENU_ALLIES:
	    // Ally Attack
	    imageUp = new TextureRegionDrawable(ResourcesMenus.menuAllyAttack);
	    menuAllyAttack = new Image(imageUp);
	    menuAllyAttack.setVisible(false);
	    // Ally Neutral
	    imageUp = new TextureRegionDrawable(ResourcesMenus.menuAllyNeutral);
	    menuAllyNeutral = new Image(imageUp);
	    menuAllyNeutral.setVisible(false);
	    // Ally Defense
	    imageUp = new TextureRegionDrawable(ResourcesMenus.menuAllyDefense);
	    menuAllyDefense = new Image(imageUp);
	    menuAllyDefense.setVisible(false);
	    break;
	case DESCRIPTION_ALLIES:
	    // Ally Attack
	    imageUp = new TextureRegionDrawable(
		    ResourcesMenus.descriptionAllyAttack);
	    descriptionAllyAttack = new Image(imageUp);
	    descriptionAllyAttack.setVisible(false);
	    // Ally Neutral
	    imageUp = new TextureRegionDrawable(
		    ResourcesMenus.descriptionAllyNeutral);
	    descriptionAllyNeutral = new Image(imageUp);
	    descriptionAllyNeutral.setVisible(false);
	    // Ally defense
	    imageUp = new TextureRegionDrawable(
		    ResourcesMenus.descriptionAllyDefense);
	    descriptionAllyDefense = new Image(imageUp);
	    descriptionAllyDefense.setVisible(false);
	    break;
	}
    }

    private void createActions(ElementsType type) {
	switch (type) {
	case BACK:
	    if (buttonBack.isPressed()
		    || (buttonPlay != null && buttonPlay.isPressed())) {
		buttonBack.addAction(Actions.fadeOut(DURATION_FADE_OUT));
	    } else if (!buttonBack.isPressed()) {
		buttonBack.addAction(Actions.fadeOut(0f));
		buttonBack.addAction(Actions.fadeIn(DURATION_FADE_IN));
	    }
	    break;
	case NEXT:
	    if (buttonBack.isPressed() || buttonNext.isPressed()) {
		buttonNext.addAction(Actions.fadeOut(DURATION_FADE_OUT));
	    } else if (!buttonNext.isPressed()
		    && buttonNext.getActions() == null) {
		buttonNext.addAction(Actions.fadeOut(0f));
		buttonNext.addAction(Actions.fadeIn(DURATION_FADE_IN));
	    }
	    break;
	case PLAY:
	    if (buttonBack.isPressed() || buttonPlay.isPressed()) {
		buttonPlay.addAction(Actions.fadeOut(DURATION_FADE_OUT));
	    } else if (!buttonPlay.isPressed()
		    && buttonPlay.getActions() == null) {
		buttonPlay.addAction(Actions.fadeOut(0f));
		buttonPlay.addAction(Actions.fadeIn(DURATION_FADE_IN));
	    }
	    buttonPlay.toFront();
	    break;
	case BUTTON_HEROS:
	    if (buttonBack.isPressed()) {
		if (tButtonHeros.getX() >= stage.getWidth())
		    tButtonHeros.addAction(Actions.moveTo(0, 0, DURATION_MOVE));
		else
		    tButtonHeros.addAction(Actions.moveTo(-stage.getWidth(), 0,
			    DURATION_MOVE));
	    } else if (buttonNext.isPressed()) {
		tButtonHeros.addAction(Actions.moveTo(stage.getWidth(), 0,
			DURATION_MOVE));
	    } else {
		tButtonHeros.addAction(Actions.fadeOut(0f));
		tButtonHeros.addAction(Actions.fadeIn(0.5f));
		tButtonHeros.addAction(Actions.moveTo(0, 0, DURATION_MOVE));
	    }
	    break;
	case DESCRIPTION_HERO:
	    if (buttonBack.isPressed() || buttonNext.isPressed()) {
		tDescriptionHero.addAction(Actions.fadeOut(DURATION_FADE_OUT));
	    } else {
		tDescriptionHero.addAction(Actions.fadeOut(0f));
		tDescriptionHero.addAction(Actions.fadeIn(DURATION_FADE_IN));
	    }
	    break;
	case MENU_HERO:
	    if (buttonBack.isPressed() || buttonNext.isPressed()) {
		tMenuHero.addAction(Actions.fadeOut(DURATION_FADE_OUT));
	    } else {
		tMenuHero.addAction(Actions.fadeOut(0f));
		tMenuHero.addAction(Actions.fadeIn(DURATION_FADE_IN));
	    }
	    break;
	case BUTTON_ALLIES:
	    if (buttonBack.isPressed()) {
		tButtonAllies.addAction(Actions.moveTo(-stage.getWidth(), 0,
			DURATION_MOVE));
	    } else if (buttonPlay.isPressed()) {
		tButtonAllies.addAction(Actions.moveTo(stage.getWidth(), 0,
			DURATION_MOVE));
	    } else {
		tButtonAllies.addAction(Actions.fadeOut(0f));
		tButtonAllies.addAction(Actions.fadeIn(0.5f));
		tButtonAllies.addAction(Actions.moveTo(0, 0, DURATION_MOVE));
	    }
	    break;
	case MENU_ALLIES:
	    if (buttonBack.isPressed() || buttonPlay.isPressed()) {
		tMenuAllies.addAction(Actions.fadeOut(DURATION_FADE_OUT));
	    } else {
		tMenuAllies.addAction(Actions.fadeOut(0f));
		tMenuAllies.addAction(Actions.fadeIn(DURATION_FADE_IN));
	    }
	    break;
	case DESCRIPTION_ALLIES:
	    if (buttonBack.isPressed() || buttonPlay.isPressed()) {
		tDescriptionAllies
			.addAction(Actions.fadeOut(DURATION_FADE_OUT));
	    } else {
		tDescriptionAllies.addAction(Actions.fadeOut(0f));
		tDescriptionAllies.addAction(Actions.fadeIn(DURATION_FADE_IN));
	    }
	    break;
	}
    }

    private void updateNextScreen(float delta) {
	timeToClearAllElements += tmpNextScreen != null ? delta : 0;
	if (tmpNextScreen != null
		&& timeToClearAllElements >= TIME_TO_CHANGE_SCREEN) {
	    this.clearElements();
	    super.nextScreen = tmpNextScreen;
	}
    }

    private void clearActorsUnUsedInStage(float delta) {
	if (stage.getActors().size > 6) {
	    timeToClearAllElements += delta;
	    if (timeToClearAllElements > DURATION_MOVE) {
		stage.getActors().get(1).remove();
	    }
	} else if (tmpNextScreen == null) {
	    timeToClearAllElements = 0;
	}
    }

    private void clearElements() {
	tButtonHeros.clear();
	tMenuHero.clear();
	tDescriptionHero.clear();
	buttonNext.clear();
	buttonBack.clear();
	if (tButtonAllies != null) {
	    tButtonAllies.clear();
	    tMenuAllies.clear();
	    tDescriptionAllies.clear();
	    buttonPlay.clear();
	}
    }

    @Override
    public boolean onBackPressed() {
	if (tButtonHeros.getX() < stage.getWidth()) {
	    tButtonHeros.addAction(Actions.moveTo(-stage.getWidth(), 0,
		    DURATION_MOVE));
	    tMenuHero.addAction(Actions.fadeOut(DURATION_FADE_OUT));
	    tDescriptionHero.addAction(Actions.fadeOut(DURATION_FADE_OUT));
	    buttonBack.addAction(Actions.fadeOut(DURATION_FADE_OUT));
	    buttonNext.addAction(Actions.fadeOut(DURATION_FADE_OUT));
	    tmpNextScreen = new LevelMenu();
	} else {
	    buttonNext.addAction(Actions.fadeOut(DURATION_FADE_OUT));
	    buttonPlay.addAction(Actions.fadeOut(DURATION_FADE_OUT));
	    tButtonAllies.addAction(Actions.moveTo(-stage.getWidth(), 0,
		    DURATION_MOVE));
	    tMenuAllies.addAction(Actions.fadeOut(DURATION_FADE_OUT));
	    tDescriptionAllies.addAction(Actions.fadeOut(DURATION_FADE_OUT));
	    // Initialized components of heros
	    tButtonHeros.setX(stage.getWidth());
	    initComponentsHeroSelected();
	    registerListenersHeroSelected();
	}
	return true;
    }
}
