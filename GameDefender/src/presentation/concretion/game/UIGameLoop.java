/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package presentation.concretion.game;

import presentation.abstraction.IUIGameLoop;
import service.InputGame;
import service.InputGameLoop;
import service.OnBackPressedInput;
import service.data.SettingManager;
import service.data.StateManager.AllyState;
import service.data.StateManager.EnemyState;
import service.data.StateManager.GameScore;
import service.data.StateManager.GameState;
import service.data.StateManager.HeroState;
import service.data.TypeManager.EnemyType;
import service.data.TypeManager.ItemType;
import application.abstraction.IScreen;
import application.concretion.GameLoopController;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * UIGameLoop.java
 * 
 * @version May 1, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class UIGameLoop implements IUIGameLoop {

    // CAMERA LIMITS
    private final float WIDTH_WORLD;
    private float width = 604;
    private float height = 340;

    /* Fields */
    private Scene scene;
    private HUD hud;
    private Character characters;
    // Camera.
    private OrthographicCamera camera;
    private SpriteBatch batch;
    // The input
    private InputGameLoop input;

    /**
     * UIGameLoop constructor.
     */
    public UIGameLoop(GameLoopController controller, float widthWorld) {
	WIDTH_WORLD = widthWorld;
	InputGame.getInputMultiplexer().clear();

	// Create class to draw elements of this user interface.
	scene = new Scene();
	hud = new HUD();
	characters = new Character();
	// Create camera.
	camera = new OrthographicCamera();
	camera.setToOrtho(false, width, height);
	// Get sprite batch.
	this.batch = hud.getSpriteBatch();

	// Resize to centered screen in mobile phones.
	this.resize(width, height);

	// Set input processor
	input = new InputGameLoop(controller);
	InputGame.getInputMultiplexer().addProcessor(
		new OnBackPressedInput(controller));
	InputGame.getInputMultiplexer().addProcessor(input);
	Gdx.input.setCatchBackKey(true);

	// Set audio
	SettingManager.startAudio(SettingManager.battleTheme);
    }

    // =======
    // UPDATES
    // =======
    @Override
    public void updateLife(int life) {
	hud.updateLife(life);
    }

    @Override
    public void updateMagic(int magic) {
	hud.updateMagic(magic);
    }

    @Override
    public void updateCreation(int creation) {
	hud.updateCreation(creation);
    }

    @Override
    public void updateTower(int life) {
	hud.updateTower(life);
    }

    // ===============
    // DRAWS / RENDERS
    // ===============
    @Override
    public void draw(float delta) {
	input.checkInput();
	hud.draw(delta);
	camera.update();
	batch.setProjectionMatrix(camera.combined);
    }

    @Override
    public void renderHero(HeroState stateHero, float posHeroX, float posHeroY,
	    float widthHero, float heightHero, boolean isFaceRight,
	    GameState gameState) {
	// Draw hero.
	batch.begin();
	Sprite sHero = characters.loadAnimationHero(stateHero, isFaceRight,
		gameState);
	sHero.setBounds(posHeroX - sHero.getWidth() / 2 + widthHero / 2,
		posHeroY, sHero.getWidth(), sHero.getHeight());
	sHero.draw(batch);
	batch.end();

	// Camera limits.
	if (posHeroX + widthHero / 2 <= width / 2) {
	    camera.position.x = width / 2;
	} else if (posHeroX + widthHero / 2 >= WIDTH_WORLD - width / 2) {
	    camera.position.x = WIDTH_WORLD - width / 2;
	} else {
	    camera.position.x = posHeroX + widthHero / 2;
	}
    }

    @Override
    public void renderProyectiles(HeroState stateHero, EnemyState enemyState,
	    float posProyectileX, float posProyectileY, float width,
	    float height, float widthProyectile, float heightProyectile,
	    float delta) {
	width = width == widthProyectile ? 0 : width;
	height = height == heightProyectile ? 0 : height;
	batch.begin();
	// Draw proyectile heros.
	if (stateHero != null) {
	    Sprite proyectile = characters.loadAnimationProyectilesHeros(
		    stateHero, delta);
	    proyectile.setBounds(posProyectileX - width, posProyectileY
		    - height, widthProyectile, heightProyectile);
	    proyectile.draw(batch);
	}
	// Draw projectiles enemies.
	if (enemyState != null) {
	    Sprite proyectile = characters.loadAnimationProyectilesEnemy(
		    enemyState, delta);
	    proyectile.setBounds(posProyectileX - width, posProyectileY
		    - height, widthProyectile, heightProyectile);
	    proyectile.draw(batch);
	}
	batch.end();
    }

    @Override
    public void renderEnemies(float posEnemyX, float posEnemyY,
	    float widthEnemy, float heightEnemy, boolean isFaceRight,
	    float delta, EnemyState state, int code, boolean toRemove,
	    EnemyType type, GameState gameState) {
	// Draw Enemy
	batch.begin();
	Sprite sEnemy = characters.loadAnimationEnemies(isFaceRight, state,
		delta, code, toRemove, type, gameState);
	sEnemy.setBounds(posEnemyX - sEnemy.getWidth() / 2 + widthEnemy / 2,
		posEnemyY, sEnemy.getWidth(), sEnemy.getHeight());
	sEnemy.draw(batch);
	batch.end();
    }

    @Override
    public void renderAlly(float posAllyX, float posAllyY, float widthAlly,
	    float heightAlly, boolean isFaceRight, AllyState state,
	    float delta, GameState gameState) {
	// Draw Ally
	batch.begin();
	Sprite sAlly = characters.loadAnimationAlly(state, isFaceRight, delta,
		gameState);
	sAlly.setBounds(posAllyX, posAllyY, sAlly.getWidth(), sAlly.getHeight());
	sAlly.draw(batch);
	batch.end();
    }

    @Override
    public void renderItems(float posItemX, float posItemY, float widthItem,
	    float heightItem, ItemType type) {
	// Draw item
	batch.begin();
	Sprite item = characters.loadAnimationItems(type);
	item.setBounds(posItemX - widthItem / 2, posItemY - heightItem / 2,
		widthItem, heightItem);
	item.draw(batch, 10);
	batch.end();
    }

    @Override
    public void renderTower(float posTowerX, float posTowerY, float widthTower,
	    float heightTower) {
	batch.begin();
	Sprite tower = scene.updateTower();
	tower.setBounds(posTowerX, posTowerY, widthTower, heightTower);
	tower.draw(batch);
	batch.end();
    }

    @Override
    public void renderGrounds(float posGroundX, float posGroundY,
	    float widthGround, float heightGround) {
	// Draw grounds.
	batch.begin();
	Sprite ground = scene.updateGrounds();
	ground.setBounds(posGroundX, posGroundY, widthGround, heightGround);
	ground.draw(batch);
	batch.end();
    }

    public void renderTemporalWalls(float posHeroX, float posX, float posY,
	    float widthWall) {
    }

    @Override
    public void renderBackground() {
	// Draw background.
	batch.begin();
	Sprite background = scene.updateStage();
	background.draw(batch);
	batch.end();
    };

    @Override
    public void pauseGame() {
	hud.pauseGame();
    }

    @Override
    public void resumeGame() {
	hud.resumeGame();
    }

    @Override
    public void resize(float width, float height) {
	hud.resize(width, height);
    }

    @Override
    public void dispose() {
    }

    @Override
    public IScreen nextScreen() {
	return hud.nextScreen();
    }

    @Override
    public void win(final GameScore score) {
	hud.winGame(score);
    }

    @Override
    public void gameOver() {
	hud.gameOver();
    }

    @Override
    public boolean onBackPressed() {
	return this.hud.onBackPressed();
    }
}
