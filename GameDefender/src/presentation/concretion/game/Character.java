/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package presentation.concretion.game;

import java.util.HashMap;
import java.util.Map;

import service.Animator;
import service.data.StateManager.AllyState;
import service.data.StateManager.EnemyState;
import service.data.StateManager.GameState;
import service.data.StateManager.HeroState;
import service.data.TypeManager.EnemyType;
import service.data.TypeManager.ItemType;
import service.resources.ResourceAlly;
import service.resources.ResourceHeros;
import service.resources.ResourcesEnemies.EnemyBatUI;
import service.resources.ResourcesEnemies.EnemyFireLionUI;
import service.resources.ResourcesEnemies.EnemyGhostArmorUI;
import service.resources.ResourcesEnemies.EnemyUI;

import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * UICharacters.java
 * 
 * @version Apr 28, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class Character {

    /* Fields */
    private int stateTime, deadTime, attackTime;

    // Auxiliary fields
    private Map<Integer, EnemyUI> enemies;

    public Character() {
	enemies = new HashMap<Integer, EnemyUI>();
    }

    /**
     * Load animation of hero.
     * 
     * @param stateHero
     *            is the state of hero to load respective animation.
     * @return sprite to animate it.
     */
    public Sprite loadAnimationHero(HeroState stateHero, boolean isFaceRight,
	    GameState gameState) {
	Sprite s = null;
	switch (stateHero) {
	case INACTIVE:
	    s = ResourceHeros.heroStance.getKeyFrame(stateTime,
		    Animator.ANIMATION_LOOPING);
	    attackTime = 0;
	    break;
	case AIR:
	    s = ResourceHeros.heroJump;
	    attackTime = 0;
	    break;
	case AIR_ATTACK:
	    s = ResourceHeros.heroAirAttack.getKeyFrame(attackTime,
		    Animator.ANIMATION_NONLOOPING);
	    attackTime++;
	    break;
	case AIR_SP1:
	    s = ResourceHeros.heroAirSp1.getKeyFrame(attackTime,
		    Animator.ANIMATION_NONLOOPING);
	    attackTime++;
	    break;
	case AIR_SP2:
	    s = ResourceHeros.heroAirSp2.getKeyFrame(attackTime,
		    Animator.ANIMATION_NONLOOPING);
	    attackTime++;
	    break;
	case AIR_SP3:
	    s = ResourceHeros.heroAirSp3.getKeyFrame(attackTime,
		    Animator.ANIMATION_NONLOOPING);
	    attackTime++;
	    break;
	case BEHIND_DAMAGE:
	    s = ResourceHeros.heroBegindDamage;
	    break;
	case FRONT_DAMAGE:
	    s = ResourceHeros.heroFrontDamage;
	    break;
	case DEAD:
	    s = ResourceHeros.heroDead.getKeyFrame(deadTime,
		    Animator.ANIMATION_NONLOOPING);
	    deadTime++;
	    break;
	case GR_ATTACK_STANCE:
	    s = ResourceHeros.heroGrAttackStance.getKeyFrame(attackTime,
		    Animator.ANIMATION_NONLOOPING);
	    attackTime++;
	    break;
	case GR_SP1:
	    s = ResourceHeros.heroGrSp1.getKeyFrame(attackTime,
		    Animator.ANIMATION_NONLOOPING);
	    attackTime++;
	    break;
	case GR_SP2:
	    s = ResourceHeros.heroGrSp2.getKeyFrame(attackTime,
		    Animator.ANIMATION_NONLOOPING);
	    attackTime++;
	    break;
	case GR_SP3:
	    s = ResourceHeros.heroGrSp3.getKeyFrame(attackTime,
		    Animator.ANIMATION_NONLOOPING);
	    attackTime++;
	    break;
	case WALK:
	    s = ResourceHeros.heroRun.getKeyFrame(stateTime,
		    Animator.ANIMATION_LOOPING);
	    attackTime = 0;
	    break;
	case WIN:
	    s = ResourceHeros.heroWin.getKeyFrame(stateTime,
		    Animator.ANIMATION_LOOPING);
	    break;
	case LOSE:
	    s = ResourceHeros.heroLose.getKeyFrame(stateTime,
		    Animator.ANIMATION_LOOPING);
	    break;
	}
	stateTime++;

	// Flip sprite
	if (stateHero == HeroState.DEAD) {
	    if (isFaceRight && !s.isFlipX()) {
		s.flip(true, false);
	    } else if (!isFaceRight && s.isFlipX()) {
		s.flip(true, false);
	    }
	} else {
	    if (!isFaceRight && !s.isFlipX()) {
		s.flip(true, false);
	    } else if (isFaceRight && s.isFlipX()) {
		s.flip(true, false);
	    }
	}

	// when game paused
	if (gameState == GameState.PAUSED) {
	    attackTime--;
	    stateTime--;
	}
	return s;
    }

    /**
     * Load animation enemy.
     * 
     * @param isFaceRight
     *            determine if enemy is lock to right, false enemy lock to left.
     * @param state
     *            is the state of enemy.
     * @param delta
     *            is the time passed.
     * @param code
     *            is the code of enemy.
     * @param toRemove
     *            is true enemy is prepared to removed.
     * @param type
     *            is the type of enemy.
     * @param gameState
     *            is the state of game.
     * @return sprite to animate it.
     */
    public Sprite loadAnimationEnemies(boolean isFaceRight, EnemyState state,
	    float delta, int code, boolean toRemove, EnemyType type,
	    GameState gameState) {
	Sprite s = null;
	EnemyUI enemySprite = null;
	// if is new create it and add to the map
	if (!enemies.containsKey(code)) {
	    enemySprite = createEnemySprites(type);
	    enemies.put(code, enemySprite);
	} else {
	    enemySprite = enemies.get(code);
	}
	// next sprite for the frame
	switch (state) {
	case WALK:
	    s = enemySprite.getWalk().getKeyFrame(enemySprite.stateTime,
		    Animator.ANIMATION_LOOPING);
	    if (gameState != GameState.PAUSED) {
		enemySprite.stateTime += delta;
	    }
	    break;
	case DEAD:
	    s = enemySprite.getDead().getKeyFrame(enemySprite.deadTime,
		    Animator.ANIMATION_NONLOOPING);
	    if (gameState != GameState.PAUSED) {
		enemySprite.deadTime += delta;
	    }
	    if (toRemove) {
		enemies.remove(code);
	    }
	    break;
	case ATTACK:
	    s = enemySprite.getAttack();
	    break;
	case INACTIVE:
	    s = enemySprite.getInactive();
	    break;
	case KICKED:
	    s = enemySprite.getKicked();
	    break;
	}

	// Flip image.
	if (isFaceRight && !s.isFlipX()) {
	    s.flip(true, false);
	} else if (!isFaceRight && s.isFlipX()) {
	    s.flip(true, false);
	}
	return s;
    }

    private EnemyUI createEnemySprites(EnemyType type) {
	switch (type) {
	case GHOST_ARMOR:
	    return new EnemyGhostArmorUI();
	case ELF:
	    break;
	case FIRE_LION:
	    return new EnemyFireLionUI();
	case BAT:
	    return new EnemyBatUI();
	}
	return null;
    }

    private float timeAlly;

    /**
     * Load animation ally
     * 
     * @param state
     *            is the state of ally.
     * @param isFaceRight
     *            determine if enemy is lock to right, false enemy lock to left.
     * @param delta
     *            is the time passed.
     * @param gameState
     *            is the state of game.
     * @return sprite to animate it.
     */
    public Sprite loadAnimationAlly(AllyState state, boolean isFaceRight,
	    float delta, GameState gameState) {
	if (gameState != GameState.PAUSED) {
	    timeAlly += delta;
	}
	Sprite s = null;
	s = ResourceAlly.allyFront;
	//
	switch (state) {
	case BORN:
	    s = ResourceAlly.allyCreated.getKeyFrame(timeAlly,
		    Animator.ANIMATION_NONLOOPING);
	    break;
	case FRONT:
	    s = ResourceAlly.allyFront;
	    break;
	case WALK:
	    s = ResourceAlly.allyWalk.getKeyFrame(timeAlly,
		    Animator.ANIMATION_LOOPING);
	    break;
	case INACTIVE:
	    s = ResourceAlly.allyFace;
	    break;
	}
	// Flip image.
	if (!isFaceRight && !s.isFlipX()) {
	    s.flip(true, false);
	} else if (isFaceRight && s.isFlipX()) {
	    s.flip(true, false);
	}
	return s;
    }

    /**
     * Load animation items.
     * 
     * @param type
     *            is the type of item.
     * 
     * @return sprite to animate it.
     */
    public Sprite loadAnimationItems(ItemType type) {
	Sprite s = null;
	if (type == ItemType.LIFE) {
	    s = ResourceAlly.lifeItem;
	} else {

	    s = ResourceAlly.magicItem;
	}
	return s;
    }

    private float timeProyectile;

    /**
     * Load animation proyectiles of hero.
     * 
     * @param state
     *            is he state of hero.
     * @param delta
     *            is the time passed.
     * @return sprite to animate it.
     */
    public Sprite loadAnimationProyectilesHeros(HeroState state, float delta) {
	timeProyectile += delta;
	Sprite s = null;
	switch (state) {
	case AIR_ATTACK:
	    s = ResourceHeros.heroAirAttackEffects.getKeyFrame(timeProyectile,
		    Animator.ANIMATION_LOOPING);
	    break;
	case AIR_SP1:
	    s = ResourceHeros.heroAirSp1Effects.getKeyFrame(timeProyectile,
		    Animator.ANIMATION_LOOPING);
	    break;
	case AIR_SP2:
	    s = ResourceHeros.heroAirSp2Effects.getKeyFrame(timeProyectile,
		    Animator.ANIMATION_LOOPING);
	    break;
	case AIR_SP3:
	    s = ResourceHeros.heroAirSp3Effects.getKeyFrame(timeProyectile,
		    Animator.ANIMATION_LOOPING);
	    break;
	case GR_ATTACK_STANCE:
	    s = ResourceHeros.heroGrAttackStanceEffects.getKeyFrame(
		    timeProyectile, Animator.ANIMATION_LOOPING);
	    break;
	case GR_SP1:
	    s = ResourceHeros.heroGrSp1Effects.getKeyFrame(timeProyectile,
		    Animator.ANIMATION_LOOPING);
	    break;
	case GR_SP2:
	    s = ResourceHeros.heroGrSp2Effects.getKeyFrame(timeProyectile,
		    Animator.ANIMATION_LOOPING);
	    break;
	case GR_SP3:
	    s = ResourceHeros.heroGrSp3Effects.getKeyFrame(timeProyectile,
		    Animator.ANIMATION_LOOPING);
	    break;
	default:
	    break;
	}
	timeProyectile = timeProyectile > 2 ? 0 : timeProyectile;
	return s;
    }

    /**
     * Load animation proyectiles of enemy.
     * 
     * @param state
     *            is he state of enemy.
     * @param delta
     *            is the time passed.
     * @return sprite to animate it.
     */
    public Sprite loadAnimationProyectilesEnemy(EnemyState state, float delta) {
	return ResourceHeros.heroGrAttackStanceEffects.getKeyFrame(
		timeProyectile, Animator.ANIMATION_LOOPING);
    }
}
