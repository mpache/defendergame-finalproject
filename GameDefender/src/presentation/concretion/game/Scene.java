/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package presentation.concretion.game;

import service.resources.ResourcesWorld;

import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * Scene.java
 * 
 * @version May 1, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class Scene {

    /**
     * Update Stage.
     * 
     * @return sprite of stage.
     */
    public Sprite updateStage() {
	return ResourcesWorld.stage1;
    }

    /**
     * Update tower.
     * 
     * @return sprite of tower.
     */
    public Sprite updateTower() {
	return ResourcesWorld.tower;
    }

    /**
     * Update grounds.
     * 
     * @return sprite of grounds.
     */
    public Sprite updateGrounds() {
	return ResourcesWorld.ground;
    }
}
