/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package presentation.concretion.game;

import presentation.abstraction.AStage;
import presentation.concretion.LoadingScreen;
import service.InputGame;
import service.data.SettingManager;
import service.data.StateManager.GameScore;
import service.resources.ResourceHeros;
import service.resources.ResourcesHUD;
import service.resources.ResourcesMenus;
import service.resources.ResourcesWorld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;

import domain.heros.Hero;

/**
 * HUD.java
 * 
 * @version Apr 28, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class HUD extends AStage {

    /* CONSTANTS */
    private static final float DURATION_MOVE = 0.5f;

    /* Fields */
    // Tables
    private Table tAvatar, tFaceHero, tLife, tMagic, tCreation, tTower, tSp1,
	    tSp2, tSp3, tPauseButton, tCenter, tLeft, tLeftButtons, tRight,
	    tRightButtons, tTutorials, tArrowRight, tArrowLeft;
    // Buttons
    private ImageButton buttonPause, buttonTutorials, buttonHome, buttonRetry,
	    nextTutorial, prevoiusTutorial;

    /* Fields To Mobile Phone */
    private Touchpad control;
    private ImageButton buttonAttack, buttonJump;

    // Auxiliary fields
    private int numberOfTutorial, tempNumberOfTutorial;
    private boolean isPasued = false;
    private float timeToClearPauseButtons;
    private int totalLife, totalMagic, totalCreation, totalLifeTower;
    private int life, magic, creation, lifeTower;

    @Override
    public void draw(float delta) {
	stage.act(delta);
	stage.draw();
	this.clearPauseScreen(delta);
	this.updateTutorials();
    }

    @Override
    protected void initStage() {
	this.stage = new Stage();
	InputGame.getInputMultiplexer().addProcessor(stage);
    }

    /**
     * HUD constructor.
     */
    public HUD() {
	this.initStage();
	this.initComponents();
	this.registerListeners();
	if (SettingManager.isMobile) {
	    this.initComponentsMobilePhone();
	    this.registerListenersMobilePhone();
	}
    }

    private void initComponents() {
	Drawable imageUp, imageDown;

	// ---------------------------------
	// AVATAR
	// ---------------------------------
	tAvatar = new Table();
	tAvatar.setFillParent(true);
	stage.addActor(tAvatar);
	imageUp = new TextureRegionDrawable(ResourcesHUD.avatar);
	Image avatar = new Image(imageUp);
	tAvatar.top().left().toFront();
	tAvatar.add(avatar);
	// Faces of hero
	tFaceHero = new Table();
	tFaceHero.setFillParent(true);
	stage.addActor(tFaceHero);
	// Life
	tLife = new Table();
	tLife.setFillParent(true);
	stage.addActor(tLife);
	// Magic
	tMagic = new Table();
	tMagic.setFillParent(true);
	stage.addActor(tMagic);
	// Creation
	tCreation = new Table();
	tCreation.setFillParent(true);
	stage.addActor(tCreation);
	// Tower
	tTower = new Table();
	tTower.setFillParent(true);
	stage.addActor(tTower);

	// ---------------------------------
	// SPECIAL BUTTONS
	// ---------------------------------
	// SP1
	tSp1 = new Table();
	tSp1.setFillParent(true);
	stage.addActor(tSp1);
	// SP2
	tSp2 = new Table();
	tSp2.setFillParent(true);
	stage.addActor(tSp2);
	// SP3
	tSp3 = new Table();
	tSp3.setFillParent(true);
	stage.addActor(tSp3);

	// ---------------------------------
	// PAUSE, WIN OR LOSE
	// ---------------------------------
	// TABLES
	tCenter = new Table();
	tCenter.setFillParent(true);
	stage.addActor(tCenter);
	tLeft = new Table();
	tLeft.setFillParent(true);
	stage.addActor(tLeft);
	tLeft.left();
	tLeftButtons = new Table();
	tLeftButtons.setFillParent(true);
	stage.addActor(tLeftButtons);
	tLeftButtons.left();
	tRight = new Table();
	tRight.setFillParent(true);
	stage.addActor(tRight);
	tRight.right();
	tRightButtons = new Table();
	tRightButtons.setFillParent(true);
	stage.addActor(tRightButtons);
	tRightButtons.right();

	// Button Pause.
	tPauseButton = new Table();
	tPauseButton.setFillParent(true);
	stage.addActor(tPauseButton);
	tPauseButton.bottom();
	imageUp = new TextureRegionDrawable(ResourcesHUD.buttonPauseUp);
	imageDown = new TextureRegionDrawable(ResourcesHUD.buttonPauseDown);
	this.buttonPause = new ImageButton(imageUp, imageDown);
	tPauseButton.add(this.buttonPause);

	// Button Tutorials
	imageUp = new TextureRegionDrawable(ResourcesHUD.buttonTutorialsUp);
	imageDown = new TextureRegionDrawable(ResourcesHUD.buttonTutorialsDown);
	this.buttonTutorials = new ImageButton(imageUp, imageDown);
	imageUp = new TextureRegionDrawable(ResourcesMenus.arrowNextUp);
	imageDown = new TextureRegionDrawable(ResourcesMenus.arrowNextDown);
	nextTutorial = new ImageButton(imageUp, imageDown);
	imageUp = new TextureRegionDrawable(ResourcesMenus.arrowBackUp);
	imageDown = new TextureRegionDrawable(ResourcesMenus.arrowBackDown);
	prevoiusTutorial = new ImageButton(imageUp, imageDown);
	tTutorials = new Table();
	tTutorials.setFillParent(true);
	stage.addActor(tTutorials);
	tTutorials.setVisible(false);
	tArrowLeft = new Table();
	tArrowLeft.setFillParent(true);
	stage.addActor(tArrowLeft);
	tArrowRight = new Table();
	tArrowRight.setFillParent(true);
	stage.addActor(tArrowRight);

	// Button Home
	imageUp = new TextureRegionDrawable(ResourcesHUD.buttonHomeUp);
	imageDown = new TextureRegionDrawable(ResourcesHUD.buttonHomeDown);
	this.buttonHome = new ImageButton(imageUp, imageDown);

	// Button Retry
	imageUp = new TextureRegionDrawable(ResourcesHUD.buttonRetryUp);
	imageDown = new TextureRegionDrawable(ResourcesHUD.buttonRetryDown);
	this.buttonRetry = new ImageButton(imageUp, imageDown);
    }

    @Override
    protected void registerListeners() {
	// PAUSE
	this.buttonPause.addListener(new ChangeListener() {

	    @Override
	    public void changed(ChangeEvent event, Actor actor) {
		Array<InputProcessor> processors = InputGame
			.getInputMultiplexer().getProcessors();
		// Paused game.
		if (!isPasued) {
		    processors.peek().touchDown(0, 0, 0, InputGame.Keys.PAUSE);
		}
		// Resume game.
		else {
		    processors.peek().touchDown(0, 0, 0,
			    InputGame.Keys.CONTINUE);
		    buttonPause.setDisabled(true);
		}
	    }
	});
	// HOME
	this.buttonHome.addListener(new ChangeListener() {

	    @Override
	    public void changed(ChangeEvent event, Actor actor) {
		InputGame.getInputMultiplexer().clear();
		buttonAudio.setY(0);
		buttonAudio.setX(0);
		nextScreen = new LoadingScreen(false);
	    }
	});
	// RETRY
	this.buttonRetry.addListener(new InputListener() {
	    @Override
	    public boolean touchDown(InputEvent event, float x, float y,
		    int pointer, int button) {
		InputGame.getInputMultiplexer().clear();
		nextScreen = new LoadingScreen(true);
		return true;
	    }
	});
	// TUTORIALS
	this.buttonTutorials.addListener(new InputListener() {
	    @Override
	    public boolean touchDown(InputEvent event, float x, float y,
		    int pointer, int button) {
		if (!tTutorials.isVisible()) {
		    tempNumberOfTutorial = -1;
		    numberOfTutorial = 0;
		    tTutorials.setVisible(true);
		    tTutorials.toFront();
		    tArrowLeft.toFront();
		    tArrowRight.toFront();
		} else {
		    tTutorials.setVisible(false);
		    tTutorials.clear();
		    tArrowRight.clear();
		    tArrowLeft.clear();
		    tempNumberOfTutorial = -1;
		    numberOfTutorial = 0;
		}
		return true;
	    }
	});
	this.nextTutorial.addListener(new ChangeListener() {

	    @Override
	    public void changed(ChangeEvent event, Actor actor) {
		numberOfTutorial++;
	    }
	});
	this.prevoiusTutorial.addListener(new ChangeListener() {

	    @Override
	    public void changed(ChangeEvent event, Actor actor) {
		numberOfTutorial--;
	    }
	});
    }

    private void updateTutorials() {
	if (buttonPause.isPressed()) {
	    tTutorials.clear();
	    tArrowRight.clear();
	    tArrowLeft.clear();
	}
	if (tTutorials.isVisible() && tempNumberOfTutorial != numberOfTutorial) {
	    tempNumberOfTutorial = numberOfTutorial;
	    tTutorials.clear();
	    tArrowRight.clear();
	    tArrowLeft.clear();
	    Image tutorial;
	    if (numberOfTutorial == 0) {
		tutorial = new Image(ResourcesMenus.tutSistemGame);
		tTutorials.add(tutorial);
		tArrowRight.add(nextTutorial).pad(420, 650, 0, 0);
	    } else if (numberOfTutorial == 1) {
		if (Gdx.app.getType() == ApplicationType.Android
			|| Gdx.app.getType() == ApplicationType.iOS) {
		    tutorial = new Image(ResourcesMenus.tutBasicControlsMobile);
		} else {
		    tutorial = new Image(ResourcesMenus.tutBasicControls);
		}
		tTutorials.add(tutorial);
		tArrowRight.add(nextTutorial).pad(420, 650, 0, 0);
		tArrowLeft.add(prevoiusTutorial).pad(420, 0, 0, 650);
	    } else if (numberOfTutorial == 2) {
		tutorial = new Image(ResourcesMenus.tutSpecialAttacks);
		tTutorials.add(tutorial);
		tArrowRight.add(nextTutorial).pad(420, 650, 0, 0);
		tArrowLeft.add(prevoiusTutorial).pad(420, 0, 0, 650);
	    } else if (numberOfTutorial == 3) {
		tutorial = new Image(ResourcesMenus.tutHud);
		tTutorials.add(tutorial);
		tArrowRight.add(nextTutorial).pad(420, 650, 0, 0);
		tArrowLeft.add(prevoiusTutorial).pad(420, 0, 0, 650);
	    } else if (numberOfTutorial == 4) {
		tutorial = new Image(ResourcesMenus.tutItems);
		tTutorials.add(tutorial);
		tArrowLeft.add(prevoiusTutorial).pad(420, 0, 0, 650);
	    }
	}
    }

    /**
     * Update life of hero.
     * 
     * @param life
     *            is the life of hero.
     */
    public void updateLife(int life) {
	totalLife = totalLife == 0 ? life : totalLife;
	if (this.life != life) {
	    this.life = life;
	    tAvatar.toFront();
	    Image img;
	    // Update face (if life is less than 20% of total life change face)
	    img = life <= 20 * totalLife / 100 ? new Image(
		    ResourceHeros.heroFace2) : new Image(
		    ResourceHeros.heroFace1);
	    tFaceHero.clear();
	    tFaceHero.top().left().toFront();
	    tFaceHero.add(img).padTop(45).padLeft(50);
	    // Update life;
	    tLife.clear();
	    tLife.top().left();
	    life = life > 0 ? (int) ResourcesHUD.lifeBar.getWidth() * life
		    / totalLife : 0;
	    TextureRegion t = new TextureRegion(ResourcesHUD.lifeBar, 0, 0,
		    life, (int) ResourcesHUD.lifeBar.getHeight());
	    img = new Image(t);
	    tLife.add(img).padTop(100).padLeft(155);
	}
    }

    /**
     * Update magic of hero.
     * 
     * @param magic
     *            is the magic of hero.
     */
    public void updateMagic(int magic) {
	totalMagic = totalMagic == 0 ? magic : totalMagic;
	// Update button
	if (this.magic != magic) {
	    this.magic = magic;
	    Image img;
	    tAvatar.toFront();
	    tFaceHero.toFront();
	    // Update Special buttons.
	    this.updateSpButtons();
	    // Update magic bar.
	    tMagic.clear();
	    tMagic.top().left();
	    magic = magic > 0 ? (int) ResourcesHUD.magicBar.getWidth() * magic
		    / totalMagic : 0;
	    TextureRegion t = new TextureRegion(ResourcesHUD.magicBar, 0, 0,
		    magic, (int) ResourcesHUD.magicBar.getHeight());
	    img = new Image(t);
	    tMagic.add(img).padTop(125).padLeft(150);
	}
    }

    private void updateSpButtons() {
	// Sp1
	if (magic >= Hero.MAGIC_SPEND_SP1) {
	    tSp1.clear();
	    tSp1.center().right().padTop(-350).toFront();
	    Drawable image = new TextureRegionDrawable(ResourcesHUD.buttonSp1Up);
	    ImageButton sp1 = new ImageButton(image);
	    tSp1.add(sp1);
	    sp1.addListener(new InputListener() {
		@Override
		public boolean touchDown(InputEvent event, float x, float y,
			int pointer, int button) {
		    Array<InputProcessor> processors = InputGame
			    .getInputMultiplexer().getProcessors();
		    return processors.peek().touchDown(0, 0, 0,
			    InputGame.Keys.SP1);
		}
	    });
	} else if (magic < Hero.MAGIC_SPEND_SP1) {
	    tSp1.clear();
	    tSp1.center().right().padTop(-350).toFront();
	    Drawable image = new TextureRegionDrawable(
		    ResourcesHUD.buttonSp1Down);
	    ImageButton sp1 = new ImageButton(image);
	    tSp1.add(sp1);
	}
	// Sp2
	if (magic >= Hero.MAGIC_SPEND_SP2) {
	    tSp2.clear();
	    tSp2.center().right().padTop(-50).toFront();
	    Drawable image = new TextureRegionDrawable(ResourcesHUD.buttonSp2Up);
	    ImageButton sp2 = new ImageButton(image);
	    tSp2.add(sp2);
	    sp2.addListener(new InputListener() {
		@Override
		public boolean touchDown(InputEvent event, float x, float y,
			int pointer, int button) {
		    Array<InputProcessor> processors = InputGame
			    .getInputMultiplexer().getProcessors();
		    return processors.peek().touchDown(0, 0, 0,
			    InputGame.Keys.SP2);
		}
	    });
	} else if (magic < Hero.MAGIC_SPEND_SP2) {
	    tSp2.clear();
	    tSp2.center().right().padTop(-50).toFront();
	    Drawable image = new TextureRegionDrawable(
		    ResourcesHUD.buttonSp2Down);
	    ImageButton sp2 = new ImageButton(image);
	    tSp2.add(sp2);
	}
	// Sp3
	if (magic >= Hero.MAGIC_SPEND_SP3) {
	    tSp3.clear();
	    tSp3.center().right().padTop(250).toFront();
	    Drawable image = new TextureRegionDrawable(ResourcesHUD.buttonSp3Up);
	    ImageButton sp3 = new ImageButton(image);
	    tSp3.add(sp3);
	    sp3.addListener(new InputListener() {
		@Override
		public boolean touchDown(InputEvent event, float x, float y,
			int pointer, int button) {
		    Array<InputProcessor> processors = InputGame
			    .getInputMultiplexer().getProcessors();
		    return processors.peek().touchDown(0, 0, 0,
			    InputGame.Keys.SP3);
		}
	    });
	} else if (magic < Hero.MAGIC_SPEND_SP3) {
	    tSp3.clear();
	    tSp3.center().right().padTop(250).toFront();
	    Drawable image = new TextureRegionDrawable(
		    ResourcesHUD.buttonSp3Down);
	    ImageButton sp3 = new ImageButton(image);
	    tSp3.add(sp3);
	}
    }

    public void updateCreation(int creation) {
	totalCreation = totalCreation == 0 ? creation : totalCreation;
	// Update button
	if (this.creation != creation) {
	    this.creation = creation;
	    Image img;
	    tAvatar.toFront();
	    tFaceHero.toFront();
	    // Update creation bar.
	    tCreation.clear();
	    tCreation.top().left();
	    creation = creation > 0 ? (int) ResourcesHUD.creationBar
		    .getHeight() * creation / totalCreation : 0;
	    TextureRegion t = new TextureRegion(ResourcesHUD.creationBar, 0, 0,
		    (int) ResourcesHUD.creationBar.getWidth(), creation);
	    t.flip(false, true);
	    img = new Image(t);
	    int diff = (int) ResourcesHUD.creationBar.getHeight() - creation;
	    tCreation.add(img).padLeft(12f).padTop(diff);
	}
    }

    /**
     * Update life of tower
     * 
     * @param life
     *            is the life that remind to tower.
     */
    public void updateTower(int life) {
	totalLifeTower = totalLifeTower == 0 ? life : totalLifeTower;
	if (this.lifeTower != life) {
	    this.lifeTower = life;
	    Image img;
	    // Put icon of tower
	    img = new Image(ResourcesWorld.towerIcon);
	    tTower.clear();
	    tTower.top();
	    tTower.add(img);
	    // Calculate life.
	    String l = String.valueOf(life);
	    for (int i = 0; i < l.length(); i++) {
		int num = Integer.parseInt(l.charAt(i) + "");
		img = num == 0 ? new Image(ResourcesMenus.num0) : img;
		img = num == 1 ? new Image(ResourcesMenus.num1) : img;
		img = num == 2 ? new Image(ResourcesMenus.num2) : img;
		img = num == 3 ? new Image(ResourcesMenus.num3) : img;
		img = num == 4 ? new Image(ResourcesMenus.num5) : img;
		img = num == 5 ? new Image(ResourcesMenus.num4) : img;
		img = num == 6 ? new Image(ResourcesMenus.num6) : img;
		img = num == 7 ? new Image(ResourcesMenus.num7) : img;
		img = num == 8 ? new Image(ResourcesMenus.num8) : img;
		img = num == 9 ? new Image(ResourcesMenus.num9) : img;
		tTower.add(img);
	    }
	    // Put total life
	    img = new Image(ResourcesMenus.slash);
	    tTower.add(img);
	    l = String.valueOf(totalLife);
	    for (int i = 0; i < l.length(); i++) {
		int num = Integer.parseInt(l.charAt(i) + "");
		img = num == 0 ? new Image(ResourcesMenus.num0) : img;
		img = num == 1 ? new Image(ResourcesMenus.num1) : img;
		img = num == 2 ? new Image(ResourcesMenus.num2) : img;
		img = num == 3 ? new Image(ResourcesMenus.num3) : img;
		img = num == 4 ? new Image(ResourcesMenus.num5) : img;
		img = num == 5 ? new Image(ResourcesMenus.num4) : img;
		img = num == 6 ? new Image(ResourcesMenus.num6) : img;
		img = num == 7 ? new Image(ResourcesMenus.num7) : img;
		img = num == 8 ? new Image(ResourcesMenus.num8) : img;
		img = num == 9 ? new Image(ResourcesMenus.num9) : img;
		tTower.add(img);
	    }
	}
    }

    /**
     * Pause game.
     */
    public void pauseGame() {
	this.isPasued = true;
	Drawable dImage;
	Image image;
	// Text and background.
	dImage = new TextureRegionDrawable(ResourcesHUD.pausedBackground);
	tCenter.setBackground(dImage);
	dImage = new TextureRegionDrawable(ResourcesHUD.paused);
	image = new Image(dImage);
	tCenter.add(image);
	tCenter.toFront();

	// Aside bars.
	// left
	dImage = new TextureRegionDrawable(ResourcesHUD.asideBarLeft);
	image = new Image(dImage);
	tLeft.add(image);
	tLeft.setX(-image.getWidth());
	tLeft.addAction(Actions.moveTo(0, 0, DURATION_MOVE));
	tLeft.toFront();
	// right
	dImage = new TextureRegionDrawable(ResourcesHUD.asideBarRight);
	image = new Image(dImage);
	tRight.add(image);
	tRight.setX(image.getWidth());
	tRight.addAction(Actions.moveTo(0, 0, DURATION_MOVE));
	tRight.toFront();

	// Buttons
	// left
	super.createAudioButton();
	buttonAudio.setVisible(true);
	tLeftButtons.add(buttonAudio).pad(0, 20, 50, 0);
	tLeftButtons.row();
	tLeftButtons.add(buttonTutorials).pad(50, 20, 0, 0);
	tLeftButtons.setX(-image.getWidth());
	tLeftButtons.addAction(Actions.moveTo(0, 0, DURATION_MOVE));
	tLeftButtons.toFront();
	// right
	tRightButtons.add(buttonHome).pad(0, 0, 50, 20);
	tRightButtons.row();
	tRightButtons.add(buttonRetry).pad(50, 0, 0, 20);
	tRightButtons.setX(image.getWidth());
	tRightButtons.addAction(Actions.moveTo(0, 0, DURATION_MOVE));
	tRightButtons.toFront();

	// Change z-index of specific elements.
	tLife.toFront();
	tMagic.toFront();
	tCreation.toFront();
	tAvatar.toFront();
	tFaceHero.toFront();
	tPauseButton.toFront();
    }

    /**
     * Resume game.
     */
    public void resumeGame() {
	this.isPasued = false;
	tCenter.clear();
	Drawable img = null;
	tCenter.setBackground(img);
	tLeft.addAction(Actions.moveTo(-stage.getWidth(), 0, DURATION_MOVE));
	tLeftButtons.addAction(Actions.moveTo(-stage.getWidth(), 0,
		DURATION_MOVE));
	tRight.addAction(Actions.moveTo(stage.getWidth(), 0, DURATION_MOVE));
	tRightButtons.addAction(Actions.moveTo(stage.getWidth(), 0,
		DURATION_MOVE));
    }

    private void clearPauseScreen(float delta) {
	timeToClearPauseButtons += !isPasued ? delta : 0;
	if (timeToClearPauseButtons >= DURATION_MOVE) {
	    tLeft.clear();
	    tLeftButtons.clear();
	    tRight.clear();
	    tRightButtons.clear();
	    buttonPause.setDisabled(false);
	    timeToClearPauseButtons = 0;
	}
    }

    /**
     * Win game.
     */
    public void winGame(GameScore score) {
	// Save the score
	SettingManager.DAO.setScore(SettingManager.temporalWorld, score);
	SettingManager.DAO.setScore(SettingManager.temporalWorld + 1,
		GameScore.UNBLOCKED);
	// Clear buttons
	tPauseButton.clear();
	tSp1.clear();
	tSp2.clear();
	tSp3.clear();
	if (SettingManager.isMobile) {
	    control.remove();
	    buttonAttack.clear();
	    buttonJump.clear();
	}

	// Create Win screen.
	Drawable dImage;
	Image image;
	// Text and background.
	dImage = new TextureRegionDrawable(ResourcesHUD.winBackground);
	tCenter.setBackground(dImage);
	dImage = new TextureRegionDrawable(ResourcesHUD.win);
	image = new Image(dImage);
	tCenter.add(image).pad(0, 100, 0, 0);

	// Stars.
	tCenter.row();
	Table stars = new Table();
	tCenter.add(stars);
	Drawable dStar = new TextureRegionDrawable(ResourcesHUD.star);
	Drawable dnoStar = new TextureRegionDrawable(ResourcesHUD.noStar);
	switch (score) {
	case STAR1:
	    image = new Image(dStar);
	    stars.add(image).padLeft(70);
	    image = new Image(dnoStar);
	    stars.add(image);
	    image = new Image(dnoStar);
	    stars.add(image);
	    break;
	case STAR2:
	    image = new Image(dStar);
	    stars.add(image).padLeft(70);
	    image = new Image(dStar);
	    stars.add(image);
	    image = new Image(dnoStar);
	    stars.add(image);
	    break;
	case STAR3:
	    image = new Image(dStar);
	    stars.add(image).padLeft(70);
	    image = new Image(dStar);
	    stars.add(image);
	    image = new Image(dStar);
	    stars.add(image);
	    break;
	default:
	    break;
	}

	// buttons
	tCenter.row();
	Table buttons = new Table();
	tCenter.add(buttons);
	buttons.add(buttonHome).padLeft(70);
	buttons.add(buttonRetry).padLeft(20);
	if (SettingManager.temporalWorld < SettingManager.DAO.getWorldsLength() - 1) {
	    Drawable imageUp = new TextureRegionDrawable(
		    ResourcesMenus.buttonArrowNextUp);
	    Drawable imageDown = new TextureRegionDrawable(
		    ResourcesMenus.buttonArrowNextDown);
	    ImageButton buttonNext = new ImageButton(imageUp, imageDown);
	    buttons.add(buttonNext).padLeft(20);
	    buttonNext.addListener(new ChangeListener() {

		@Override
		public void changed(ChangeEvent event, Actor actor) {
		    SettingManager.temporalWorld = SettingManager.temporalWorld + 1;
		    nextScreen = new LoadingScreen(true);
		}

	    });
	}
	tCenter.toFront();

	// Change z-index of specific elements
	tLife.toFront();
	tMagic.toFront();
	tCreation.toFront();
	tAvatar.toFront();
	tFaceHero.toFront();
    }

    /**
     * Lose game.
     */
    public void gameOver() {
	// Clear buttons
	tPauseButton.clear();
	tSp1.clear();
	tSp2.clear();
	tSp3.clear();
	if (SettingManager.isMobile) {
	    control.remove();
	    buttonAttack.clear();
	    buttonJump.clear();
	}

	// Create game Over screen
	Drawable dImage;
	Image image;
	// Text and background.
	dImage = new TextureRegionDrawable(ResourcesHUD.loseBackground);
	tCenter.setBackground(dImage);
	dImage = new TextureRegionDrawable(ResourcesHUD.lose);
	image = new Image(dImage);
	tCenter.add(image).pad(0, 100, 0, 0);

	// buttons
	tCenter.row();
	Table buttons = new Table();
	tCenter.add(buttons);
	buttons.add(buttonHome).padLeft(70);
	buttons.add(buttonRetry).padLeft(20);
	tCenter.toFront();

	// Change z-index of specific elements
	tLife.toFront();
	tMagic.toFront();
	tCreation.toFront();
	tAvatar.toFront();
	tFaceHero.toFront();
    }

    /**
     * Get spriteBatch of this stage.
     * 
     * @return the sprite batch of this stage.
     */
    public SpriteBatch getSpriteBatch() {
	return this.stage.getSpriteBatch();
    }

    // -------------------------------------------
    // APP IN MOBILE PHONE
    // -------------------------------------------
    private void initComponentsMobilePhone() {
	// Define Tables
	Table tMControl = new Table();
	tMControl.setFillParent(true);
	stage.addActor(tMControl);
	Table tMButtons = new Table();
	tMButtons.setFillParent(true);
	stage.addActor(tMButtons);

	// Touch pad.
	tMControl.bottom().left().padLeft(100);
	TextureRegionDrawable backgroundDrawable = new TextureRegionDrawable(
		ResourcesHUD.bakgroundControl);
	TextureRegionDrawable knobDrawable = new TextureRegionDrawable(
		ResourcesHUD.knobControl);
	control = new Touchpad(10, new Touchpad.TouchpadStyle(
		backgroundDrawable, knobDrawable));
	control.setBounds(15, 15, 300, 300);
	tMControl.add(control);
	// Buttons
	tMButtons.bottom().right();
	Drawable imageUp, imageDown;
	imageUp = new TextureRegionDrawable(ResourcesHUD.buttonAttackUp);
	imageDown = new TextureRegionDrawable(ResourcesHUD.buttonAttackDown);
	this.buttonAttack = new ImageButton(imageUp, imageDown);
	tMButtons.add(this.buttonAttack).padRight(50);
	imageUp = new TextureRegionDrawable(ResourcesHUD.buttonJumpUp);
	imageDown = new TextureRegionDrawable(ResourcesHUD.buttonJumpDown);
	this.buttonJump = new ImageButton(imageUp, imageDown);
	tMButtons.add(this.buttonJump).padRight(50);

    }

    private void registerListenersMobilePhone() {
	// Control
	this.control.addListener(new ChangeListener() {

	    @Override
	    public void changed(ChangeEvent event, Actor actor) {
		int x = Math.round(control.getKnobPercentX());
		Array<InputProcessor> processors = InputGame
			.getInputMultiplexer().getProcessors();
		processors.peek().touchDown(x, 0, 0, InputGame.Keys.PAD);
	    }
	});

	// Attack
	this.buttonAttack.addListener(new InputListener() {
	    @Override
	    public boolean touchDown(InputEvent event, float x, float y,
		    int pointer, int button) {
		Array<InputProcessor> processors = InputGame
			.getInputMultiplexer().getProcessors();
		return processors.peek().touchDown(0, 0, 0,
			InputGame.Keys.ATTACK);
	    }
	});

	// Jump
	this.buttonJump.addListener(new InputListener() {
	    @Override
	    public boolean touchDown(InputEvent event, float x, float y,
		    int pointer, int button) {
		Array<InputProcessor> processors = InputGame
			.getInputMultiplexer().getProcessors();
		return processors.peek()
			.touchDown(0, 0, 0, InputGame.Keys.JUMP);
	    }
	});
    }

    public boolean onBackPressed() {
	if (!isPasued) {
	    this.buttonPause.toggle();
	} else {
	    this.buttonHome.toggle();
	}
	return true;
    }
}
