/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package presentation.concretion;

import presentation.abstraction.AStage;
import service.data.SettingManager;
import service.resources.ResourcesMenus;
import application.abstraction.IScreen;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * MainMenu.java
 * 
 * @version Apr 19, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class MainMenu extends AStage implements IScreen {

    /* URI */
    private static final String FACEBOOK_URI = "http://www.facebook.com";
    private static final String GOOGLE_URI = "http://www.google.com";
    private static final String TWITTER_URI = "http://www.twitter.com";

    /* CONSTANTS */
    private static final float DURATION_FADE_IN = 0.5f;
    private static final float DURATION_FADE_OUT = 0.5f;
    private static final float DURATION_MOVE = 0.5f;
    private static final float TIME_TO_CHANGE_SCREEN = 1f;
    private static final float TIME_TO_CLEAR_OPTIONS_BUTTONS = 0.2f;

    /* ELEMENTS OF THIS SCREEN TYPE */
    private enum ElementsType {
	TITLE, SWIPE, CREDITS, PLAY, OPTIONS, VIDEOS, TUTORIALS, NEXT_TUTORIAL, FOLLOW
    }

    /* FIELDS */
    public static AssetManager MANAGER;
    // Title
    private Image titleGame;
    // Swipe
    private Image swipeUp, swipeDown;
    // Credits
    private Image credits;
    // Buttons.
    private ImageButton buttonPlay, buttonOptions, buttonVideos,
	    buttonTutorials;
    // Tutorials
    private Table tTut, tRowRight, tRowLeft;
    private Image tutorial;
    private int numberOfTutorial;
    private ImageButton nextTutorial, prevoiusTutorial;
    // Follows
    private ImageButton followFacebook, followGoogle, followTwitter;
    // Auxiliary fields
    private IScreen tmpNextScreen;
    private float initY, currentY; // To create Swapping.
    private float timeToClearOptionsButtons, timeToClearAllElements;

    /**
     * MainMenu controller.
     */
    public MainMenu(AssetManager manager) {
	super.initStage();
	// Set audio
	SettingManager.startAudio(SettingManager.mainTheme);
	// initialized components.
	this.initComponents();
	this.registerListeners();
	this.setUpSwipeListener();
	// Set manager.
	MANAGER = manager;
	Gdx.input.setCatchBackKey(false);
    }

    @Override
    public void update(float delta) {
	updateButtons(delta);
	updateNextScreen(delta);
    }

    @Override
    public void draw(float delta) {
	stage.act(delta);
	super.draw(delta);
    }

    @Override
    public void resize(float width, float height) {
	super.resize(width, height);
    }

    @Override
    public void pause() {
    }

    @Override
    public void dispose() {
	super.dispose();
    }

    @Override
    public void resume() {
	while (!MANAGER.update()) {
	}
    }

    @Override
    public boolean processAction(int action) {
	return false;
    }

    // ===============
    // PRIVATE METHODS
    // ===============
    private void initComponents() {
	// Create Tables.
	// Panel to title game
	Table tTtitle = new Table();
	tTtitle.setFillParent(true);
	this.stage.addActor(tTtitle);
	// Title
	this.createElement(ElementsType.TITLE);
	tTtitle.top().add(titleGame);

	// Panel to play button.
	Table tPlay = new Table();
	tPlay.setFillParent(true);
	this.stage.addActor(tPlay);
	// Put play button.
	this.createElement(ElementsType.PLAY);
	tPlay.center().add(buttonPlay).padTop(150);

	// Panel to option button.
	Table tOptions = new Table();
	tOptions.setFillParent(true);
	this.stage.addActor(tOptions);
	// Put options button.
	this.createElement(ElementsType.OPTIONS);
	tOptions.bottom().left().setZIndex(3);
	tOptions.add(buttonOptions);

	// Panel to tutorials
	tTut = new Table();
	tTut.setFillParent(true);
	stage.addActor(tTut);
	tTut.setVisible(false);
	tRowRight = new Table();
	tRowRight.setFillParent(true);
	stage.addActor(tRowRight);
	tRowLeft = new Table();
	tRowLeft.setFillParent(true);
	stage.addActor(tRowLeft);

	// Panel to swipes.
	Table tSwipeUp = new Table();
	Table tSwipeDown = new Table();
	tSwipeUp.setFillParent(true);
	tSwipeDown.setFillParent(true);
	this.stage.addActor(tSwipeUp);
	this.stage.addActor(tSwipeDown);
	this.createElement(ElementsType.SWIPE);
	// Put swipes up.
	tSwipeUp.bottom().right();
	tSwipeUp.add(swipeUp);
	// put Swipes down.
	tSwipeDown.top().right();
	tSwipeDown.add(swipeDown);

	// Panel to follows.
	Table tFollows = new Table();
	tFollows.setFillParent(true);
	this.stage.addActor(tFollows);
	// Follow.
	this.createElement(ElementsType.FOLLOW);
	tFollows.top().left();
	tFollows.add(followFacebook);
	tFollows.add(followGoogle);
	tFollows.add(followTwitter);

	// Initialized actions Actors (Elements of this screen).
	this.createActions(ElementsType.TITLE);
	this.createActions(ElementsType.SWIPE);
	this.createActions(ElementsType.PLAY);
	this.createActions(ElementsType.OPTIONS);
	this.createActions(ElementsType.FOLLOW);

	// Initialized other buttons, that not are in tables.
	createElement(ElementsType.CREDITS);
	createElement(ElementsType.VIDEOS);
	createElement(ElementsType.TUTORIALS);
	createElement(ElementsType.NEXT_TUTORIAL);
	super.createAudioButton();
    }

    @Override
    protected void registerListeners() {
	// PLAY
	this.buttonPlay.addListener(new InputListener() {

	    @Override
	    public boolean touchDown(InputEvent event, float x, float y,
		    int pointer, int button) {
		// Put sound.
		SettingManager.startSound(SettingManager.next);
		// Create actions and put next screen.
		buttonPlay.setDisabled(true);
		createActions(ElementsType.PLAY);
		tmpNextScreen = new LevelMenu();
		return true;
	    }
	});
	// OPTIONS
	this.buttonOptions.addListener(new InputListener() {

	    @Override
	    public boolean touchDown(InputEvent event, float x, float y,
		    int pointer, int button) {
		// Put sound.
		if (!buttonVideos.isVisible())
		    SettingManager.startSound(SettingManager.next);
		else
		    SettingManager.startSound(SettingManager.back);
		// VIDEOS
		if (!buttonVideos.isVisible())
		    buttonVideos.setVisible(true);
		// TUTORIALS
		if (!buttonTutorials.isVisible()) {
		    buttonTutorials.setVisible(true);
		}
		// AUDIO
		if (!buttonAudio.isVisible())
		    buttonAudio.setVisible(true);
		// TUTORIALS
		if (tTut.isVisible()) {
		    tTut.setVisible(false);
		    createActions(ElementsType.TUTORIALS);
		}
		// ACTION
		createActions(ElementsType.OPTIONS);
		return true;
	    }
	});
	// VIDEOS

	// TUTORIALS
	this.buttonTutorials.addListener(new InputListener() {

	    @Override
	    public boolean touchDown(InputEvent event, float x, float y,
		    int pointer, int button) {
		// Put sound.
		if (!tTut.isVisible()) {
		    SettingManager.startSound(SettingManager.next);
		} else {
		    SettingManager.startSound(SettingManager.back);
		}
		createActions(ElementsType.TUTORIALS);
		return true;
	    }
	});
	this.nextTutorial.addListener(new ChangeListener() {

	    @Override
	    public void changed(ChangeEvent event, Actor actor) {
		numberOfTutorial++;
		createActions(ElementsType.NEXT_TUTORIAL);
	    }
	});
	this.prevoiusTutorial.addListener(new ChangeListener() {

	    @Override
	    public void changed(ChangeEvent event, Actor actor) {
		numberOfTutorial--;
		createActions(ElementsType.NEXT_TUTORIAL);
	    }
	});
	// FOLLOw FACEBOOK
	followFacebook.addListener(new ActorGestureListener() {

	    @Override
	    public void touchDown(InputEvent event, float x, float y,
		    int pointer, int button) {
		// TODO Auto-generated method stub
		super.touchDown(event, x, y, pointer, button);
		followFacebook.addAction(Actions.moveTo(followFacebook.getX(),
			stage.getHeight() - followFacebook.getHeight() + 20, 0f));
	    }

	    @Override
	    public void touchUp(InputEvent event, float x, float y,
		    int pointer, int button) {
		// TODO Auto-generated method stub
		super.touchUp(event, x, y, pointer, button);
		followFacebook.addAction(Actions.moveTo(followFacebook.getX(),
			stage.getHeight() - followFacebook.getHeight() + 40, 0f));
		// open page.
		openURL(FACEBOOK_URI);
	    }
	});
	// FOLLOW GOOGLE+
	followGoogle.addListener(new ActorGestureListener() {

	    @Override
	    public void touchDown(InputEvent event, float x, float y,
		    int pointer, int button) {
		// TODO Auto-generated method stub
		super.touchDown(event, x, y, pointer, button);
		followGoogle.addAction(Actions.moveTo(followGoogle.getX(),
			stage.getHeight() - followGoogle.getHeight() + 20, 0f));
	    }

	    @Override
	    public void touchUp(InputEvent event, float x, float y,
		    int pointer, int button) {
		// TODO Auto-generated method stub
		super.touchUp(event, x, y, pointer, button);
		followGoogle.addAction(Actions.moveTo(followGoogle.getX(),
			stage.getHeight() - followGoogle.getHeight() + 40, 0f));
		// open page.
		openURL(GOOGLE_URI);
	    }
	});
	// FOLLOW TWITTER
	followTwitter.addListener(new ActorGestureListener() {

	    @Override
	    public void touchDown(InputEvent event, float x, float y,
		    int pointer, int button) {
		super.touchDown(event, x, y, pointer, button);
		followTwitter.addAction(Actions.moveTo(followTwitter.getX(),
			stage.getHeight() - followTwitter.getHeight() + 20, 0f));
	    }

	    @Override
	    public void touchUp(InputEvent event, float x, float y,
		    int pointer, int button) {
		super.touchUp(event, x, y, pointer, button);
		followTwitter.addAction(Actions.moveTo(followTwitter.getX(),
			stage.getHeight() - followTwitter.getHeight() + 40, 0f));
		// open page.
		openURL(TWITTER_URI);
	    }
	});
    }

    private void setUpSwipeListener() {
	stage.addListener(new ActorGestureListener() {

	    @Override
	    public void touchDown(InputEvent event, float x, float y,
		    int pointer, int button) {
		// TODO Auto-generated method stub
		super.touchDown(event, x, y, pointer, button);
		// Set initials y axis.
		initY = y;
	    }

	    @Override
	    public void pan(InputEvent event, float x, float y, float deltaX,
		    float deltaY) {
		// TODO Auto-generated method stub
		super.pan(event, x, y, deltaX, deltaY);
		// Set current y axis.
		currentY = y;
		if (initY + stage.getHeight() / 4 < currentY
			&& titleGame.getY() < stage.getHeight()) {
		    // Hide title
		    titleGame.addAction(Actions.moveTo(70, 1000, DURATION_MOVE));
		    // Hide buttons.
		    if (buttonVideos.getY() != 0) {
			// VIDEOS
			buttonVideos.addAction(Actions.moveTo(0, 0,
				TIME_TO_CLEAR_OPTIONS_BUTTONS));
			// TUTORIALS
			buttonTutorials.addAction(Actions.moveTo(0, 0,
				TIME_TO_CLEAR_OPTIONS_BUTTONS));
			// AUDIO
			buttonAudio.addAction(Actions.moveTo(0, 0,
				TIME_TO_CLEAR_OPTIONS_BUTTONS));
		    }
		    buttonPlay.addAction(Actions.fadeOut(DURATION_FADE_OUT));
		    buttonOptions.addAction(Actions.fadeOut(DURATION_FADE_OUT));
		    // Swipes
		    swipeUp.addAction(Actions.fadeOut(DURATION_FADE_OUT));
		    swipeDown.addAction(Actions.fadeIn(DURATION_FADE_IN));
		    swipeUp.addAction(Actions.fadeOut(DURATION_FADE_OUT));
		    followFacebook.addAction(Actions.moveTo(
			    followFacebook.getX(), stage.getHeight() + 10, 1f));
		    followGoogle.addAction(Actions.moveTo(followGoogle.getX(),
			    stage.getHeight() + 10, 0.8f));
		    followTwitter.addAction(Actions.moveTo(
			    followTwitter.getX(), stage.getHeight() + 10, 0.5f));
		    // Credits
		    createActions(ElementsType.CREDITS);
		} else if (initY > currentY + stage.getHeight() / 4
			&& titleGame.getY() > stage.getHeight()
			&& !tTut.isVisible()) {
		    // title
		    titleGame.addAction(Actions.moveTo(70,
			    stage.getHeight() / 2 - 20, DURATION_MOVE));
		    // buttons
		    buttonPlay.addAction(Actions.fadeIn(DURATION_FADE_IN));
		    buttonOptions.addAction(Actions.fadeIn(DURATION_FADE_IN));
		    // Swipes.
		    swipeDown.clearActions();
		    swipeDown.addAction(Actions.fadeOut(DURATION_FADE_OUT));
		    swipeUp.addAction(Actions.fadeIn(DURATION_FADE_IN));
		    createActions(ElementsType.FOLLOW);
		    // Credits
		    credits.setX(0);
		    createActions(ElementsType.CREDITS);
		}
	    }

	    @Override
	    public void touchUp(InputEvent event, float x, float y,
		    int pointer, int button) {
		super.touchUp(event, x, y, pointer, button);
		initY = 0;
		currentY = 0;
	    }
	});
    }

    private void createElement(ElementsType type) {
	Drawable imageUp, imageDown;
	switch (type) {
	// TITLE GAME
	case TITLE:
	    imageUp = new TextureRegionDrawable(ResourcesMenus.titleGame);
	    titleGame = new Image(imageUp);
	    titleGame.setY(1000);
	    break;
	// SWIPE UP
	case SWIPE:
	    imageUp = new TextureRegionDrawable(ResourcesMenus.swipeUp);
	    swipeUp = new Image(imageUp);
	    imageUp = new TextureRegionDrawable(ResourcesMenus.swipeDown);
	    swipeDown = new Image(imageUp);
	    break;
	// CREDITS
	case CREDITS:
	    imageUp = new TextureRegionDrawable(
		    ResourcesMenus.creditsBackground);
	    credits = new Image(imageUp);
	    credits.setX(-credits.getWidth());
	    break;
	// BUTTON PLAY
	case PLAY:
	    imageUp = new TextureRegionDrawable(ResourcesMenus.buttonSlayerUp);
	    imageDown = new TextureRegionDrawable(
		    ResourcesMenus.buttonSlayerDown);
	    buttonPlay = new ImageButton(imageUp, imageDown);
	    break;
	// BUTTON OPTIONS
	case OPTIONS:
	    imageUp = new TextureRegionDrawable(ResourcesMenus.buttonOptionsUp);
	    imageDown = new TextureRegionDrawable(
		    ResourcesMenus.buttonOptionsDown);
	    buttonOptions = new ImageButton(imageUp, imageDown);
	    break;
	// BUTTON VIDEOS
	case VIDEOS:
	    imageUp = new TextureRegionDrawable(ResourcesMenus.buttonVideosUp);
	    imageDown = new TextureRegionDrawable(
		    ResourcesMenus.buttonVideosDown);
	    buttonVideos = new ImageButton(imageUp, imageDown);
	    break;
	// BUTTONS TUTORIALS
	case TUTORIALS:
	    imageUp = new TextureRegionDrawable(ResourcesMenus.buttonInfoUp);
	    imageDown = new TextureRegionDrawable(ResourcesMenus.buttonInfoDown);
	    buttonTutorials = new ImageButton(imageUp, imageDown);
	    break;
	case NEXT_TUTORIAL:
	    imageUp = new TextureRegionDrawable(ResourcesMenus.arrowNextUp);
	    imageDown = new TextureRegionDrawable(ResourcesMenus.arrowNextDown);
	    nextTutorial = new ImageButton(imageUp, imageDown);
	    imageUp = new TextureRegionDrawable(ResourcesMenus.arrowBackUp);
	    imageDown = new TextureRegionDrawable(ResourcesMenus.arrowBackDown);
	    prevoiusTutorial = new ImageButton(imageUp, imageDown);
	    break;
	// FOLLOW IT
	case FOLLOW:
	    // FOLLOW FACEBOOK
	    imageUp = new TextureRegionDrawable(ResourcesMenus.followFacebook);
	    followFacebook = new ImageButton(imageUp);
	    followFacebook.setY(stage.getHeight() + 100);
	    followFacebook.setX(20);
	    // FOLLOW GOOGLE
	    imageUp = new TextureRegionDrawable(ResourcesMenus.followGoogle);
	    followGoogle = new ImageButton(imageUp);
	    followGoogle.setY(stage.getHeight() + 100);
	    followGoogle.setX(followFacebook.getWidth() + 20);
	    // FOLLOW TWITTER
	    imageUp = new TextureRegionDrawable(ResourcesMenus.followTwitter);
	    followTwitter = new ImageButton(imageUp);
	    followTwitter.setY(stage.getHeight() + 100);
	    followTwitter.setX(followFacebook.getWidth() * 2 + 20);
	    break;
	}
    }

    private void createActions(ElementsType type) {
	switch (type) {
	case TITLE:
	    titleGame.addAction(Actions.fadeOut(0f));
	    titleGame.addAction(Actions.fadeIn(DURATION_FADE_IN));
	    titleGame.addAction(Actions.moveTo(70, stage.getHeight() / 2 - 20,
		    DURATION_MOVE));
	    break;
	case SWIPE:
	    swipeUp.addAction(Actions.fadeOut(0f));
	    swipeUp.addAction(Actions.fadeIn(DURATION_FADE_IN));
	    swipeDown.addAction(Actions.fadeOut(0f));
	    break;
	case CREDITS:
	    if (credits.getX() != 0) {
		credits.addAction(Actions.moveTo(0, 0, DURATION_MOVE));
	    } else {
		credits.addAction(Actions.moveTo(-credits.getWidth(), 0,
			DURATION_MOVE));
	    }
	    stage.addActor(credits);
	    break;
	case PLAY:
	    if (!buttonPlay.isPressed()) {
		buttonPlay.addAction(Actions.fadeOut(0f));
		buttonPlay.addAction(Actions.fadeIn(DURATION_FADE_IN));
		buttonOptions.addAction(Actions.fadeOut(0f));
		buttonOptions.addAction(Actions.fadeIn(DURATION_FADE_IN));
	    } else if (buttonPlay.isPressed()) {
		if (buttonVideos.getY() != 0) {
		    // VIDEOS
		    buttonVideos.addAction(Actions.moveTo(0, 0,
			    TIME_TO_CLEAR_OPTIONS_BUTTONS));
		    // TUTORIALS
		    buttonTutorials.addAction(Actions.moveTo(0, 0,
			    TIME_TO_CLEAR_OPTIONS_BUTTONS));
		    // AUDIO
		    buttonAudio.addAction(Actions.moveTo(0, 0,
			    TIME_TO_CLEAR_OPTIONS_BUTTONS));
		}
		buttonPlay.addAction(Actions.fadeOut(DURATION_FADE_OUT));
		buttonOptions.addAction(Actions.fadeOut(DURATION_FADE_OUT));
		titleGame.addAction(Actions.moveTo(70, 1000, DURATION_MOVE));
		swipeUp.addAction(Actions.fadeOut(DURATION_FADE_OUT));
		followFacebook.addAction(Actions.moveTo(followFacebook.getX(),
			stage.getHeight() + 10, 1f));
		followGoogle.addAction(Actions.moveTo(followGoogle.getX(),
			stage.getHeight() + 10, 0.8f));
		followTwitter.addAction(Actions.moveTo(followTwitter.getX(),
			stage.getHeight() + 10, 0.5f));
	    }
	    break;
	case OPTIONS:
	    if (buttonOptions.isPressed()) {
		if (buttonVideos.getY() == 0) {
		    // VIDEOS
		    buttonVideos.addAction(Actions.moveTo(buttonOptions.getX(),
			    buttonOptions.getY() + buttonOptions.getHeight(),
			    DURATION_MOVE));
		    // TUTORIALS
		    buttonTutorials.addAction(Actions.moveTo(
			    buttonOptions.getX(), buttonOptions.getY()
				    + buttonOptions.getHeight() * 2,
			    DURATION_MOVE));
		    // AUDIO
		    buttonAudio.addAction(Actions.moveTo(buttonOptions.getX()
			    + buttonOptions.getWidth(), buttonOptions.getY(),
			    DURATION_MOVE));
		} else {
		    // VIDEOS
		    buttonVideos.addAction(Actions.moveTo(0, 0,
			    TIME_TO_CLEAR_OPTIONS_BUTTONS));
		    // TUTORIALS
		    buttonTutorials.addAction(Actions.moveTo(0, 0,
			    TIME_TO_CLEAR_OPTIONS_BUTTONS));
		    // AUDIO
		    buttonAudio.addAction(Actions.moveTo(0, 0,
			    TIME_TO_CLEAR_OPTIONS_BUTTONS));
		}
		stage.addActor(buttonVideos);
		stage.addActor(buttonTutorials);
		stage.addActor(buttonAudio);
		buttonVideos.setZIndex(2);
		buttonTutorials.setZIndex(2);
		buttonAudio.setZIndex(2);
	    }
	    break;
	case VIDEOS:

	    break;
	case TUTORIALS:
	    if (buttonTutorials.isPressed() && !tTut.isVisible()) {
		buttonPlay.addAction(Actions.fadeOut(DURATION_FADE_OUT));
		titleGame.addAction(Actions.moveTo(70, 1000, DURATION_MOVE));
		swipeUp.addAction(Actions.fadeOut(DURATION_FADE_OUT));
		followFacebook.addAction(Actions.moveTo(followFacebook.getX(),
			stage.getHeight() + 10, 1f));
		followGoogle.addAction(Actions.moveTo(followGoogle.getX(),
			stage.getHeight() + 10, 0.8f));
		followTwitter.addAction(Actions.moveTo(followTwitter.getX(),
			stage.getHeight() + 10, 0.5f));
		// Add tutorial.
		numberOfTutorial = 0;
		tutorial = new Image(ResourcesMenus.tutSistemGame);
		tTut.add(tutorial);
		tTut.setVisible(true);
		tRowRight.add(nextTutorial).pad(420, 650, 0, 0);
	    } else {
		buttonPlay.addAction(Actions.fadeIn(DURATION_FADE_IN));
		titleGame.addAction(Actions.fadeIn(DURATION_FADE_IN));
		titleGame.addAction(Actions.moveTo(70,
			stage.getHeight() / 2 - 20, DURATION_MOVE));
		swipeUp.addAction(Actions.fadeIn(DURATION_FADE_IN));
		followFacebook.addAction(Actions.fadeOut(0f));
		followFacebook.addAction(Actions.fadeIn(0.5f));
		followFacebook
			.addAction(Actions.moveTo(followFacebook.getX(),
				stage.getHeight() - followFacebook.getHeight()
					+ 40, 1f));
		followGoogle.addAction(Actions.fadeOut(0f));
		followGoogle.addAction(Actions.fadeIn(0.5f));
		followGoogle
			.addAction(Actions.moveTo(followGoogle.getX(),
				stage.getHeight() - followGoogle.getHeight()
					+ 40, 0.8f));
		followTwitter.addAction(Actions.fadeOut(0f));
		followTwitter.addAction(Actions.fadeIn(0.5f));
		followTwitter.addAction(Actions.moveTo(followTwitter.getX(),
			stage.getHeight() - followTwitter.getHeight() + 40,
			0.5f));
		// Clear tutorials
		tTut.setVisible(false);
		tTut.clear();
		tRowLeft.clear();
		tRowRight.clear();
	    }
	    break;
	case NEXT_TUTORIAL:
	    tTut.clear();
	    tRowRight.clear();
	    tRowLeft.clear();
	    if (numberOfTutorial == 0) {
		tutorial = new Image(ResourcesMenus.tutSistemGame);
		tTut.add(tutorial);
		tRowRight.add(nextTutorial).pad(420, 650, 0, 0);
	    } else if (numberOfTutorial == 1) {
		if (Gdx.app.getType() == ApplicationType.Android
			|| Gdx.app.getType() == ApplicationType.iOS) {
		    tutorial = new Image(ResourcesMenus.tutBasicControlsMobile);
		} else {
		    tutorial = new Image(ResourcesMenus.tutBasicControls);
		}
		tTut.add(tutorial);
		tRowRight.add(nextTutorial).pad(420, 650, 0, 0);
		tRowLeft.add(prevoiusTutorial).pad(420, 0, 0, 650);
	    } else if (numberOfTutorial == 2) {
		tutorial = new Image(ResourcesMenus.tutSpecialAttacks);
		tTut.add(tutorial);
		tRowRight.add(nextTutorial).pad(420, 650, 0, 0);
		tRowLeft.add(prevoiusTutorial).pad(420, 0, 0, 650);
	    } else if (numberOfTutorial == 3) {
		tutorial = new Image(ResourcesMenus.tutHud);
		tTut.add(tutorial);
		tRowRight.add(nextTutorial).pad(420, 650, 0, 0);
		tRowLeft.add(prevoiusTutorial).pad(420, 0, 0, 650);
	    } else if (numberOfTutorial == 4) {
		tutorial = new Image(ResourcesMenus.tutItems);
		tTut.add(tutorial);
		tRowLeft.add(prevoiusTutorial).pad(420, 0, 0, 650);
	    }
	    break;
	case FOLLOW:
	    followFacebook.addAction(Actions.fadeOut(0f));
	    followFacebook.addAction(Actions.fadeIn(0.5f));
	    followFacebook.addAction(Actions.moveTo(followFacebook.getX(),
		    stage.getHeight() - followFacebook.getHeight() + 40, 1f));
	    followGoogle.addAction(Actions.fadeOut(0f));
	    followGoogle.addAction(Actions.fadeIn(0.5f));
	    followGoogle.addAction(Actions.moveTo(followGoogle.getX(),
		    stage.getHeight() - followGoogle.getHeight() + 40, 0.8f));
	    followTwitter.addAction(Actions.fadeOut(0f));
	    followTwitter.addAction(Actions.fadeIn(0.5f));
	    followTwitter.addAction(Actions.moveTo(followTwitter.getX(),
		    stage.getHeight() - followTwitter.getHeight() + 40, 0.5f));
	    break;
	}
    }

    private void updateButtons(float delta) {
	timeToClearOptionsButtons += delta;
	// BUTTON VIDEO
	if (timeToClearOptionsButtons >= TIME_TO_CLEAR_OPTIONS_BUTTONS
		&& buttonVideos.getY() == buttonOptions.getY()) {
	    buttonVideos.setVisible(false);
	    buttonTutorials.setVisible(false);
	    buttonAudio.setVisible(false);
	    timeToClearOptionsButtons = 0;
	}
	if (!buttonVideos.isVisible()) {
	    buttonVideos.setY(0);
	    buttonTutorials.setY(0);
	    buttonAudio.setX(0);
	}
    }

    private void updateNextScreen(float delta) {
	timeToClearAllElements += tmpNextScreen != null ? delta : 0;
	if (tmpNextScreen != null
		&& timeToClearAllElements >= TIME_TO_CHANGE_SCREEN) {
	    this.clearElements();
	    super.nextScreen = tmpNextScreen;
	}
    }

    private void clearElements() {
	this.titleGame.clear();
	this.credits.clear();
	this.buttonOptions.clear();
	this.buttonPlay.clear();
	this.buttonTutorials.clear();
	this.buttonVideos.clear();
	this.followFacebook.clear();
	this.followGoogle.clear();
	this.followTwitter.clear();
    }

    private void openURL(String url) {
	SettingManager.myOperation.openUrl(url);
    }

    @Override
    public boolean onBackPressed() {
	// Exit of the application
	return false;
    }
}
