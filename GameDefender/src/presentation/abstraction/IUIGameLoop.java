/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package presentation.abstraction;

import service.data.StateManager.AllyState;
import service.data.StateManager.EnemyState;
import service.data.StateManager.GameScore;
import service.data.StateManager.GameState;
import service.data.StateManager.HeroState;
import service.data.TypeManager.EnemyType;
import service.data.TypeManager.ItemType;
import application.abstraction.IScreen;

/**
 * IUIGameScreen.java
 * 
 * @version Apr 19, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public interface IUIGameLoop {

    /**
     * Update life bar.
     * 
     * @param life
     *            is the number of actual life of hero.
     */
    public abstract void updateLife(int life);

    /**
     * Update magic bar.
     * 
     * @param magic
     *            is the number of actual magic of hero.
     */
    public abstract void updateMagic(int magic);

    /**
     * Update creation bar.
     * 
     * @param creation
     *            is the number of actual creation of hero.
     */
    public abstract void updateCreation(int creation);

    /**
     * Update life of tower
     * 
     * @param life
     *            is the life that remind to tower.
     */
    public abstract void updateTower(int life);

    /** Render objects of game */
    public abstract void renderHero(HeroState stateHero, float posHeroX,
	    float posHeroY, float widthHero, float heightHero,
	    boolean isFaceRight, GameState gameState);

    public abstract void renderProyectiles(HeroState stateHero,
	    EnemyState enemyState, float posProyectileX, float posProyectileY,
	    float width, float height, float widthProyectile,
	    float heightProyectile, float delta);

    public abstract void renderEnemies(float posEnemyX, float posEnemyY,
	    float widthEnemy, float heightEnemy, boolean isFaceRight,
	    float delta, EnemyState state, int code, boolean toRemove,
	    EnemyType type, GameState gameState);

    public abstract void renderAlly(float posAllyX, float posAllyY,
	    float widthAlly, float heightAlly, boolean isFaceRight,
	    AllyState state, float delta, GameState gameState);

    public abstract void renderItems(float posItemX, float posItemY,
	    float widthItem, float heightItem, ItemType type);

    public abstract void renderTower(float posTowerX, float posTowerY,
	    float widthTower, float heightTower);

    public abstract void renderGrounds(float posGroundX, float posGroundY,
	    float widthGround, float heightGround);

    public abstract void renderTemporalWalls(float posHeroX, float posX,
	    float posY, float widthWall);
    
    public abstract void renderBackground();

    /**
     * Draw game.
     * 
     * @param delta
     *            is a float.
     */
    public abstract void draw(float delta);

    /**
     * Pause user interface.
     */
    public abstract void pauseGame();

    /**
     * Resume user interface.
     */
    public abstract void resumeGame();

    /**
     * Resize this user interface.
     * 
     * @param width
     *            indicate width of screen.
     * @param height
     *            indicate height of screen.
     */
    public abstract void resize(float width, float height);

    /**
     * Dispose this user interface.
     */
    public abstract void dispose();

    /**
     * Next screen of application.
     * 
     * @return the next screen.
     */
    public abstract IScreen nextScreen();

    /**
     * Win game.
     * 
     * @param score
     *            is the score obtained.
     */
    public abstract void win(final GameScore score);

    /**
     * Lose game.
     */
    public abstract void gameOver();

    /**
     * Check if button back of mobile phones is pushed.
     * 
     * @return true id button back is pushed, false otherwise.
     */
    public abstract boolean onBackPressed();
}
