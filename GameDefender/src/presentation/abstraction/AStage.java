/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package presentation.abstraction;

import service.InputGame;
import service.data.SettingManager;
import service.resources.ResourcesCommos;
import service.resources.ResourcesMenus;
import application.abstraction.IScreen;

import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * IScreen.java
 * 
 * @version Apr 19, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public abstract class AStage {

    /* Constants */
    protected static float WIDTH = 1280;
    protected static float HEIGHT = 720;

    /* Fields */
    protected Stage stage;
    protected IScreen nextScreen;
    protected static ImageButton buttonAudio;

    /**
     * Called when a screen should render itself.
     * 
     * @param delta
     *            is a float.
     */
    public void draw(float delta) {
	stage.draw();
    }

    /**
     * Resize screen.
     * 
     * @param width
     *            is the width of this screen.
     * @param height
     *            is the height of this screen.
     */
    public void resize(float width, float height) {
	stage.setViewport(WIDTH, HEIGHT, false);
    }

    /**
     * Dispose this screen.
     */
    public void dispose() {
	stage.dispose();
    }

    /**
     * Called to check whether the screen is done.
     * 
     * @return true when the screen is done, false otherwise
     */
    public IScreen nextScreen() {
	return this.nextScreen;
    }

    // ================
    // METHODS OF STAGE
    // ================
    /**
     * Initialized a stage.
     */
    protected void initStage() {
	stage = new Stage();
	InputMultiplexer input = InputGame.getInputMultiplexer();
	input.clear();
	input.addProcessor(stage);
	this.resize(WIDTH, HEIGHT);
	// Panel.
	Table root = new Table();
	root.setFillParent(true);
	this.stage.addActor(root);
	// Put image of background of this screen.
	Drawable image = new TextureRegionDrawable(
		ResourcesMenus.mainMenuBackground);
	root.setBackground(image);
    }

    /**
     * Put in stage sound and audio buttons.
     */
    protected void createAudioButton() {
	// Put audio buttons.
	if (buttonAudio == null) {
	    Drawable imageUp, imageDown;
	    imageUp = new TextureRegionDrawable(ResourcesCommos.buttonAudioUp);
	    imageDown = new TextureRegionDrawable(
		    ResourcesCommos.buttonAudioDown);
	    buttonAudio = new ImageButton(imageUp, imageDown, imageDown);
	    buttonAudio.addListener(new ChangeListener() {

		@Override
		public void changed(ChangeEvent event, Actor actor) {
		    SettingManager.pauseOrResumeAudio();
		}
	    });
	}
    }

    /**
     * Register buttons.
     */
    protected abstract void registerListeners();
}
