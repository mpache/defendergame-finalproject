/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package persistence;

import java.util.ArrayList;
import java.util.List;

import persistence.entities.AllyEntity;
import persistence.entities.EnemyEntity;
import persistence.entities.GroundEntity;
import persistence.entities.HeroEntity;
import persistence.entities.MenuEntity;
import persistence.entities.TowerEntity;
import persistence.entities.WorldEntity;
import service.data.StateManager.GameScore;
import service.data.TypeManager.AllyType;
import service.data.TypeManager.EnemyType;
import service.data.TypeManager.HeroType;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.files.FileHandle;

/**
 * DataAccesObjectCSV.java
 * 
 * @version May 26, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class DataAccesObjectCSV implements DataAccesObject {

    // Data Bases names
    private static final String DATABASE_NAME = "database/database.csv";
    private static final String PATTERN_SEPARATOR_LINE = "\\n";
    private static final String PATTERN_SEPARATOR_ARRAY = "~";
    private static final String PATTERN_SEPARATOR_FIELD_LINE = ":";
    private static final String PATTERN_SEPARATOR_FIELD_ARRAY = ",";
    // Score feilds
    private static final String DATABASE_SCORES_NAME = "scores";
    private static final String PATTERN_CREATED_SCORES = "created";
    // json main fields
    private static final int POSITION_KEYWORD = 0;
    private static final String FIELD_HERO = "HERO";
    private static final String FIELD_WORLD = "WORLD";
    private static final String FIELD_ALLIE = "ALLY";
    private static final String FIELD_ENEMIE = "ENEMY";
    private static final String FIELD_MENU = "MENU";
    // Menu json fields
    private static final int FIELD_MENU_BACKGROUNDS = 1;
    private static final int FIELD_MENU_ELEMENTS = 2;
    private static final int FIELD_MENU_HUD = 3;
    // Hero json fields
    private static final int FIELD_HERO_POSX = 2;
    private static final int FIELD_HERO_POSY = 3;
    private static final int FIELD_HERO_WIDTH = 4;
    private static final int FIELD_HERO_WIDTH_COLLISION = 5;
    private static final int FIELD_HERO_HEIGHT = 6;
    private static final int FIELD_HERO_HEIGHT_COLLISION = 7;
    private static final int FIELD_HERO_LIFE = 8;
    private static final int FIELD_HERO_MAGIC = 9;
    private static final int FIELD_HERO_CREATION = 10;
    private static final int FIELD_HERO_SPENDSP1 = 11;
    private static final int FIELD_HERO_SPENDSP2 = 12;
    private static final int FIELD_HERO_SPENDSP3 = 13;
    private static final int FIELD_HERO_ASSET = 14;
    private static final int POSITION_HERO_WARRIOR = 0;
    private static final int POSITION_HERO_MAGE = 1;
    // Ally json fields
    private static final int FIELD_ALLY_WIDTH = 2;
    private static final int FIELD_ALLY_HEIGHT = 3;
    private static final int FIELD_ALLY_XLEFTLIMIT = 4;
    private static final int FIELD_ALLY_XRIGHTLIMIT = 5;
    private static final int FIELD_ALLY_YBOTTOMLIMIT = 6;
    private static final int FIELD_ALLY_ASSET = 7;
    private static final int POSITION_ALLY_ATTACK = 0;
    private static final int POSITION_ALLY_DEFENDER = 1;
    private static final int POSITION_ALLY_NEUTRAL = 2;
    // enemy json fields
    private static final int FIELD_ENEMY_POSX = 2;
    private static final int FIELD_ENEMY_POSY = 3;
    private static final int FIELD_ENEMY_WIDTH = 4;
    private static final int FIELD_ENEMY_WIDTH_COLLISION = 5;
    private static final int FIELD_ENEMY_HEIGHT = 6;
    private static final int FIELD_ENEMY_HEIGHT_COLLISION = 7;
    private static final int FIELD_ENEMY_LIFE = 8;
    private static final int FIELD_ENEMY_MOVEVELOCITY = 9;
    private static final int FIELD_ENEMY_DAMAGE = 10;
    private static final int FIELD_ENEMY_ASSET = 11;
    private static final int POSITION_ENEMY_GOSHT_ARMOR = 0;
    private static final int POSITION_ENEMY_FIRE_LION = 1;
    private static final int POSITION_ENEMY_ELF = 2;
    private static final int POSITION_ENEMY_BAT = 3;
    // World json fields
    private static final int FIELD_WORLD_LEVEL = 1;
    private static final int FIELD_WORLD_WIDTH = 2;
    private static final int FIELD_WORLD_FLOOR = 3;
    private static final int FIELD_WORLD_ASSET = 4;
    private static final int FIELD_WORLD_ENEMYDELAY = 6;
    private static final int FIELD_WORLD_ENEMYDELAY_INITIAL = 0;
    private static final int FIELD_WORLD_ENEMYDELAY_DELAY = 1;
    private static final int FIELD_WORLD_ENEMYDELAY_MINIMUM = 2;
    private static final int FIELD_WORLD_ENEMIES = 7;
    private static final int FIELD_WORLD_ENEMIES_POSITION = 1;
    private static final int FIELD_WORLD_ENEMIES_QUANTITY = 2;
    // Tower json fields
    private static final int FIELD_WORLD_TOWER = 5;
    private static final int FIELD_WORLD_TOWER_POSX = 0;
    private static final int FIELD_WORLD_TOWER_POSY = 1;
    private static final int FIELD_WORLD_TOWER_WIDTH = 2;
    private static final int FIELD_WORLD_TOWER_HEIGHT = 3;
    private static final int FIELD_WORLD_TOWER_LIFE = 4;
    private static final int FIELD_WORLD_TOWER_ALLYLIMITLEFT = 5;
    private static final int FIELD_WORLD_TOWER_ALLYLIMITRIGHT = 6;
    private static final int FIELD_WORLD_TOWER_ALLYLIMITBOTTOM = 7;
    // Ground json fields
    private static final int FIELD_WORLD_GROUNDS = 8;
    private static final int FIELD_WORLD_GROUNDS_POSX = 1;
    private static final int FIELD_WORLD_GROUNDS_POSY = 2;
    private static final int FIELD_WORLD_GROUNDS_WIDTH = 3;
    private static final int FIELD_WORLD_GROUNDS_HEIGHT = 4;
    // Scores Values
    private static final int VALUE_SCORE_BLOCKED = 0;
    private static final int VALUE_SCORE_UNBLOCKED = 1;
    private static final int VALUE_SCORE_STAR1 = 2;
    private static final int VALUE_SCORE_STAR2 = 3;
    private static final int VALUE_SCORE_STAR3 = 4;

    // Data in Objects
    private List<String[]> aHeros = new ArrayList<String[]>();
    private List<String[]> aWorlds = new ArrayList<String[]>();
    private List<String[]> aAllies = new ArrayList<String[]>();
    private List<String[]> aEnemies = new ArrayList<String[]>();
    private String[] aMenu;
    private Preferences scores;

    // Singleton pattern

    private static DataAccesObjectCSV obj;

    public static DataAccesObjectCSV getInstance() {
	if (obj == null) {
	    obj = new DataAccesObjectCSV();
	}
	return obj;
    }

    /*
     * First reading Data from DATABASE_NAME
     */
    private DataAccesObjectCSV() {

	final FileHandle databaseFile = Gdx.files.internal(DATABASE_NAME);
	final String data = databaseFile.readString().trim();
	final String[] splitedData = data.split(PATTERN_SEPARATOR_LINE);

	for (int i = 0; i < splitedData.length; i++) {

	    final String line[] = splitedData[i]
		    .split(PATTERN_SEPARATOR_FIELD_LINE);
	    final String keyWord = line[POSITION_KEYWORD].trim();

	    if (keyWord.equals(FIELD_MENU)) {
		aMenu = line;
	    } else if (keyWord.equals(FIELD_HERO)) {
		aHeros.add(line);
	    } else if (keyWord.equals(FIELD_ALLIE)) {
		aAllies.add(line);
	    } else if (keyWord.equals(FIELD_ENEMIE)) {
		aEnemies.add(line);
	    } else if (keyWord.equals(FIELD_WORLD)) {
		aWorlds.add(line);
	    }
	}
	// remove the info line
	aHeros.remove(0);
	aAllies.remove(0);
	aEnemies.remove(0);
	aWorlds.remove(0);
	// load scores
	scores = Gdx.app.getPreferences(DATABASE_SCORES_NAME);
	loadScoreData();
    }

    /**
     * Fetch data of Menus assets for create MenuEntity.
     * 
     * @return a entity of MenuEntity, or null if error when fetching data.
     */
    @Override
    public MenuEntity getMenuEntity() {

	final String backgrounds = aMenu[FIELD_MENU_BACKGROUNDS].trim();
	final String elements = aMenu[FIELD_MENU_ELEMENTS].trim();
	final String hud = aMenu[FIELD_MENU_HUD].trim();

	MenuEntity entity = new MenuEntity(backgrounds, elements, hud);
	return entity;
    }

    /**
     * Fetch data of Heros for create HeroEntity.
     * 
     * @param type
     *            the type of Hero to load
     * 
     * @return a entity of HeroEntity, or null if error when fetching data.
     */
    @Override
    public HeroEntity getHeroEntity(HeroType type) {
	final int position = getPositionByHeroType(type);
	final String[] aHero = this.aHeros.get(position);

	final float posx = Float.parseFloat(aHero[FIELD_HERO_POSX].trim());
	final float posy = Float.parseFloat(aHero[FIELD_HERO_POSY].trim());
	final float width = Float.parseFloat(aHero[FIELD_HERO_WIDTH].trim());
	final float widthCollision = Float
		.parseFloat(aHero[FIELD_HERO_WIDTH_COLLISION].trim());
	final float height = Float.parseFloat(aHero[FIELD_HERO_HEIGHT].trim());
	final float heightCollision = Float
		.parseFloat(aHero[FIELD_HERO_HEIGHT_COLLISION].trim());
	final int life = Integer.parseInt(aHero[FIELD_HERO_LIFE].trim());
	final int magic = Integer.parseInt(aHero[FIELD_HERO_MAGIC].trim());
	final int creation = Integer
		.parseInt(aHero[FIELD_HERO_CREATION].trim());
	final int spendSp1 = Integer
		.parseInt(aHero[FIELD_HERO_SPENDSP1].trim());
	final int spendSp2 = Integer
		.parseInt(aHero[FIELD_HERO_SPENDSP2].trim());
	final int spendSp3 = Integer
		.parseInt(aHero[FIELD_HERO_SPENDSP3].trim());
	final String asset = aHero[FIELD_HERO_ASSET].trim();

	HeroEntity entity = new HeroEntity(type, posx, posy, width,
		widthCollision, height, heightCollision, life, magic, creation,
		spendSp1, spendSp2, spendSp3, asset);
	return entity;
    }

    /**
     * Fetch data of Allies for create AllyEntity.
     * 
     * @param type
     *            the type of Ally to load
     * 
     * @return a entity of AllyEntity, or null if error when fetching data.
     */
    @Override
    public AllyEntity getAllyEntity(AllyType type) {
	final int position = getPositionByAllyType(type);
	final String[] aAlly = this.aAllies.get(position);

	final float width = Float.parseFloat(aAlly[FIELD_ALLY_WIDTH].trim());
	final float height = Float.parseFloat(aAlly[FIELD_ALLY_HEIGHT].trim());
	final float xLeftLimit = Float.parseFloat(aAlly[FIELD_ALLY_XLEFTLIMIT]
		.trim());
	final float xRightLimit = Float
		.parseFloat(aAlly[FIELD_ALLY_XRIGHTLIMIT].trim());
	final float yBottomLimit = Float
		.parseFloat(aAlly[FIELD_ALLY_YBOTTOMLIMIT].trim());
	final String asset = aAlly[FIELD_ALLY_ASSET].trim();

	AllyEntity entity = new AllyEntity(type, xLeftLimit, xRightLimit,
		yBottomLimit, width, height, asset);
	return entity;
    }

    /**
     * Fetch data of Enemies for create EnemyEntity.
     * 
     * @param type
     *            the type of Enemy to load
     * 
     * @return a entity of EnemyEntity, or null if error when fetching data.
     */
    @Override
    public EnemyEntity getEnemyEntity(EnemyType type) {
	final int position = getPositionByEnemyType(type);
	final String[] aEnemy = this.aEnemies.get(position);

	// fetch data
	final float posx = Float.parseFloat(aEnemy[FIELD_ENEMY_POSX].trim());
	final float posy = Float.parseFloat(aEnemy[FIELD_ENEMY_POSY].trim());
	final float width = Float.parseFloat(aEnemy[FIELD_ENEMY_WIDTH].trim());
	final float widthCollision = Float
		.parseFloat(aEnemy[FIELD_ENEMY_WIDTH_COLLISION].trim());
	final float height = Float
		.parseFloat(aEnemy[FIELD_ENEMY_HEIGHT].trim());
	final float heightCollision = Float
		.parseFloat(aEnemy[FIELD_ENEMY_HEIGHT_COLLISION].trim());
	final int life = Integer.parseInt(aEnemy[FIELD_ENEMY_LIFE].trim());
	final float moveVelocity = Float
		.parseFloat(aEnemy[FIELD_ENEMY_MOVEVELOCITY].trim());
	final int damage = Integer.parseInt(aEnemy[FIELD_ENEMY_DAMAGE].trim());
	final String asset = aEnemy[FIELD_ENEMY_ASSET].trim();
	// load Data in entity
	final EnemyEntity entity = new EnemyEntity(type, posx, posy, width,
		widthCollision, height, heightCollision, moveVelocity, life,
		damage, asset);
	return entity;
    }

    /**
     * Fetch data of Worlds for create WorldEntity.
     * 
     * @param worldLevel
     *            the level.
     * 
     * @return a entity of WorldEntity, or null if error when fetching data.
     */
    @Override
    public WorldEntity getWorldEntity(int worldLevel) {

	final String[] aWorld = this.aWorlds.get(worldLevel);
	// fetch data
	final int level = Integer.parseInt(aWorld[FIELD_WORLD_LEVEL].trim());
	final float width = Float.parseFloat(aWorld[FIELD_WORLD_WIDTH].trim());
	final float floor = Float.parseFloat(aWorld[FIELD_WORLD_FLOOR].trim());
	final String asset = aWorld[FIELD_WORLD_ASSET].trim();
	final TowerEntity tower = getTowerEntity(worldLevel);
	// enemy delay
	final String[] aDelay = aWorld[FIELD_WORLD_ENEMYDELAY]
		.split(PATTERN_SEPARATOR_FIELD_ARRAY);
	final int enemyInitialDelay = Integer
		.parseInt(aDelay[FIELD_WORLD_ENEMYDELAY_INITIAL].trim());
	final int enemyDelay = Integer
		.parseInt(aDelay[FIELD_WORLD_ENEMYDELAY_DELAY].trim());
	final int enemyMinimumDelay = Integer
		.parseInt(aDelay[FIELD_WORLD_ENEMYDELAY_MINIMUM].trim());
	// entity
	final WorldEntity entity = new WorldEntity(level, width, floor, tower,
		asset, enemyInitialDelay, enemyDelay, enemyMinimumDelay);
	// Enemies Entities
	final String[] aEnemies = aWorld[FIELD_WORLD_ENEMIES]
		.split(PATTERN_SEPARATOR_ARRAY);
	for (int i = 0; i < aEnemies.length; i++) {
	    final String[] aEnemy = aEnemies[i]
		    .split(PATTERN_SEPARATOR_FIELD_ARRAY);
	    final int enemyPosition = Integer
		    .parseInt(aEnemy[FIELD_WORLD_ENEMIES_POSITION].trim());
	    final int enemyQuantity = Integer
		    .parseInt(aEnemy[FIELD_WORLD_ENEMIES_QUANTITY].trim());

	    final EnemyType type = getEnemyTypeByPosition(enemyPosition);

	    for (int j = 0; j < enemyQuantity; j++) {
		final EnemyEntity enemy = getEnemyEntity(type);
		entity.enemies.add(enemy);
	    }
	}

	// Grounds Entities
	final String[] aWorldGrounds = aWorld[FIELD_WORLD_GROUNDS]
		.split(PATTERN_SEPARATOR_ARRAY);
	for (int i = 0; i < aWorldGrounds.length; i++) {
	    final String[] aGround = aWorldGrounds[i]
		    .split(PATTERN_SEPARATOR_FIELD_ARRAY);
	    final GroundEntity ground = getGroundEntity(aGround);
	    entity.grounds.add(ground);
	}
	// return entity
	return entity;
    }

    private GroundEntity getGroundEntity(String[] aGround) {
	// fetch data
	final float posx = Float.parseFloat(aGround[FIELD_WORLD_GROUNDS_POSX]
		.trim());
	final float posy = Float.parseFloat(aGround[FIELD_WORLD_GROUNDS_POSY]
		.trim());
	final float width = Float.parseFloat(aGround[FIELD_WORLD_GROUNDS_WIDTH]
		.trim());
	final float height = Float
		.parseFloat(aGround[FIELD_WORLD_GROUNDS_HEIGHT].trim());

	final GroundEntity entity = new GroundEntity(posx, posy, width, height);
	return entity;
    }

    /**
     * Fetch data of World for fetch tower information from world and create
     * TowerEntity.
     * 
     * @param worldLevel
     *            the level.
     * 
     * @return a entity of TowerEntity, or null if error when fetching data.
     */
    @Override
    public TowerEntity getTowerEntity(int worldLevel) {
	final String[] aWorld = this.aWorlds.get(worldLevel);
	final String[] aTower = aWorld[FIELD_WORLD_TOWER]
		.split(PATTERN_SEPARATOR_FIELD_ARRAY);

	// fetch data
	final float posx = Float.parseFloat(aTower[FIELD_WORLD_TOWER_POSX]
		.trim());
	final float posy = Float.parseFloat(aTower[FIELD_WORLD_TOWER_POSY]
		.trim());
	final float width = Float.parseFloat(aTower[FIELD_WORLD_TOWER_WIDTH]
		.trim());
	final float height = Float.parseFloat(aTower[FIELD_WORLD_TOWER_HEIGHT]
		.trim());
	final int life = Integer
		.parseInt(aTower[FIELD_WORLD_TOWER_LIFE].trim());
	final float allyLimitLeft = Float
		.parseFloat(aTower[FIELD_WORLD_TOWER_ALLYLIMITLEFT].trim());
	final float allyLimitRight = Float
		.parseFloat(aTower[FIELD_WORLD_TOWER_ALLYLIMITRIGHT].trim());
	final float allyLimitBottom = Float
		.parseFloat(aTower[FIELD_WORLD_TOWER_ALLYLIMITBOTTOM].trim());

	// load Data in entity
	final TowerEntity entity = new TowerEntity(posx, posy, width, height,
		life, allyLimitLeft, allyLimitRight, allyLimitBottom);
	return entity;
    }

    /**
     * Fetch data of World for fetch ground information from world and create
     * GroundEntity.
     * 
     * @param worldLevel
     *            the level.
     * @param position
     *            the position of the ground into the world
     * 
     * @return a entity of GroundEntity, or null if error when fetching data.
     */
    @Override
    public GroundEntity getGroundEntity(int worldLevel, int position) {
	final String[] aWorld = this.aWorlds.get(worldLevel);
	final String[] aWorldGrounds = aWorld[FIELD_WORLD_GROUNDS]
		.split(PATTERN_SEPARATOR_ARRAY);
	final String[] aGround = aWorldGrounds[position]
		.split(PATTERN_SEPARATOR_FIELD_ARRAY);

	// fetch data
	final float posx = Float.parseFloat(aGround[FIELD_WORLD_GROUNDS_POSX]
		.trim());
	final float posy = Float.parseFloat(aGround[FIELD_WORLD_GROUNDS_POSY]
		.trim());
	final float width = Float.parseFloat(aGround[FIELD_WORLD_GROUNDS_WIDTH]
		.trim());
	final float height = Float
		.parseFloat(aGround[FIELD_WORLD_GROUNDS_HEIGHT].trim());

	final GroundEntity entity = new GroundEntity(posx, posy, width, height);
	return entity;
    }

    /*
     * Persistence of the Scores, file saved in local storage
     */

    private void loadScoreData() {
	// if not exist generate it
	if (!(scores.contains(PATTERN_CREATED_SCORES) && scores
		.getBoolean(PATTERN_CREATED_SCORES))) {
	    scores.clear();
	    scores.putBoolean(PATTERN_CREATED_SCORES, true);

	    // generate
	    generateScoreFile(0, true);
	}
	// if the values of world levels change
	final int scoresSize = scores.get().size() - 1;
	if (aWorlds.size() > scoresSize) {
	    System.out.println(scoresSize + " - " + aWorlds.size());

	    final GameScore lastScore = getScore(scoresSize - 1);
	    final boolean newLevel = lastScore != GameScore.BLOCKED
		    && lastScore != GameScore.UNBLOCKED;
	    generateScoreFile(scoresSize, newLevel);
	} else if (aWorlds.size() < scoresSize) {
	    System.out.println(scoresSize + " - " + aWorlds.size());

	    // if have to delete scores
	    for (int i = scoresSize; i > aWorlds.size(); i--) {
		scores.remove(String.valueOf(i - 1));
	    }
	    scores.flush();
	}
    }

    private void generateScoreFile(int initialWorld, boolean firstBlocked) {
	for (int i = initialWorld; i < getWorldsLength(); i++) {
	    if (firstBlocked) {
		scores.putInteger(String.valueOf(i), VALUE_SCORE_UNBLOCKED);
		firstBlocked = false;
	    } else {
		scores.putInteger(String.valueOf(i), VALUE_SCORE_BLOCKED);
	    }
	}
	scores.flush();
    }

    public GameScore getScore(final int worldLevel) {
	final String key = String.valueOf(worldLevel);
	if (scores.contains(key)) {
	    final int score = scores.getInteger(key);
	    return getGameScoreByValue(score);
	} else {
	    return null;
	}
    }

    public boolean setScore(final int worldLevel, final GameScore score) {
	// check world level an score
	if (worldLevel < 0 || worldLevel >= getWorldsLength() || score == null) {
	    return false;
	}
	// Check Value
	final int valueToSave = getValueByGameScore(score);
	final int currentValue = getValueByGameScore(getScore(worldLevel));
	if (valueToSave <= currentValue) {
	    return false;
	}
	// save it
	final String key = String.valueOf(worldLevel);
	scores.putInteger(key, valueToSave);
	scores.flush();
	return true;
    }

    /*
     * Values and positions of the Object Types for write and read in the csv
     * file
     */

    // Ally
    private int getPositionByAllyType(AllyType type) {
	switch (type) {
	case ATTACK:
	    return POSITION_ALLY_ATTACK;
	case DEFENSE:
	    return POSITION_ALLY_DEFENDER;
	case NEUTRAL:
	    return POSITION_ALLY_NEUTRAL;
	}
	return -1;
    }

    // Hero
    private int getPositionByHeroType(HeroType type) {
	switch (type) {
	case MAGE:
	    return POSITION_HERO_MAGE;
	case WARRIOR:
	    return POSITION_HERO_WARRIOR;
	}
	return -1;
    }

    // Enemy
    private int getPositionByEnemyType(EnemyType type) {
	switch (type) {
	case BAT:
	    return POSITION_ENEMY_BAT;
	case ELF:
	    return POSITION_ENEMY_ELF;
	case FIRE_LION:
	    return POSITION_ENEMY_FIRE_LION;
	case GHOST_ARMOR:
	    return POSITION_ENEMY_GOSHT_ARMOR;
	}
	return -1;
    }

    private EnemyType getEnemyTypeByPosition(final int position) {
	switch (position) {
	case POSITION_ENEMY_GOSHT_ARMOR:
	    return EnemyType.GHOST_ARMOR;
	case POSITION_ENEMY_FIRE_LION:
	    return EnemyType.FIRE_LION;
	case POSITION_ENEMY_ELF:
	    return EnemyType.ELF;
	case POSITION_ENEMY_BAT:
	    return EnemyType.BAT;
	}
	return null;
    }

    // GameScore
    private int getValueByGameScore(GameScore score) {
	switch (score) {
	case BLOCKED:
	    return VALUE_SCORE_BLOCKED;
	case STAR1:
	    return VALUE_SCORE_STAR1;
	case STAR2:
	    return VALUE_SCORE_STAR2;
	case STAR3:
	    return VALUE_SCORE_STAR3;
	case UNBLOCKED:
	    return VALUE_SCORE_UNBLOCKED;
	}
	return -1;
    }

    private GameScore getGameScoreByValue(final int value) {
	switch (value) {
	case VALUE_SCORE_BLOCKED:
	    return GameScore.BLOCKED;
	case VALUE_SCORE_STAR1:
	    return GameScore.STAR1;
	case VALUE_SCORE_STAR2:
	    return GameScore.STAR2;
	case VALUE_SCORE_STAR3:
	    return GameScore.STAR3;
	case VALUE_SCORE_UNBLOCKED:
	    return GameScore.UNBLOCKED;
	}
	return null;
    }

    /*
     * Getters of Length
     */

    @Override
    public int getWorldsLength() {
	return this.aWorlds.size();
    }

    @Override
    public int getHerosLength() {
	return this.aHeros.size();
    }

    @Override
    public int getAlliesLength() {
	return this.aAllies.size();
    }

    @Override
    public int getEnemiesLength() {
	return this.aEnemies.size();
    }

}
