/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package persistence;

import java.util.Iterator;
import java.util.List;

import persistence.entities.AllyEntity;
import persistence.entities.EnemyEntity;
import persistence.entities.GroundEntity;
import persistence.entities.HeroEntity;
import persistence.entities.TowerEntity;
import persistence.entities.WorldEntity;
import service.data.TypeManager.AllyType;
import service.data.TypeManager.EnemyType;
import service.data.TypeManager.HeroType;
import domain.World;
import domain.allies.Ally;
import domain.allies.AllyAttack;
import domain.allies.AllyDefense;
import domain.allies.AllyNeutral;
import domain.enemies.Enemy;
import domain.enemies.FlyEnemy;
import domain.enemies.GroundEnemy;
import domain.heros.Hero;
import domain.heros.HeroMage;
import domain.heros.HeroWarrior;
import domain.utils.Utils;
import domain.walls.Ground;
import domain.walls.Tower;

/**
 * FactoryObject.java
 * 
 * @version May 26, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class FactoryObject {

    private FactoryObject() {
    }

    /*
     * CREATION OF HEROS
     */

    /**
     * Method for create an instance of Hero from a HeroEntity.
     * 
     * @param entity
     *            the HeroEntity for create the new instance.
     * 
     * @return an instance of Hero
     */
    public static Hero getHero(HeroEntity entity) {
	// fetch data
	final float posx = entity.posx;
	final float posy = entity.posy;
	final float width = entity.width;
	final float widthCollision = entity.widthCollision;
	final float height = entity.height;
	final float heightCollision = entity.heightCollision;
	final int life = entity.life;
	final int magic = entity.magic;
	final int creation = entity.creation;
	final int sp1 = entity.spendSp1;
	final int sp2 = entity.spendSp2;
	final int sp3 = entity.spendSp3;

	// get hero
	return getHero(entity.type, posx, posy, width, widthCollision, height,
		heightCollision, life, magic, creation, sp1, sp2, sp3);
    }

    /**
     * Method for create an instance of Hero.
     * 
     * @param type
     *            the type of Hero {@link HeroType}
     * @param posx
     *            the initial position x of the Hero
     * @param posy
     *            the initial position y of the Hero
     * @param width
     *            the width of the Hero
     * @param height
     *            the height of the Hero
     * @param life
     *            the initial life of the Hero
     * @param magic
     *            the initial magic of the Hero
     * 
     * @return an instance of Hero
     */
    public static Hero getHero(HeroType type, float posx, float posy,
	    float width, float widthCollision, float height,
	    float heightCollision, int life, int magic, int creation, int sp1, int sp2,
	    int sp3) {
	// create object
	Hero hero = null;
	switch (type) {
	case WARRIOR:
	    hero = new HeroWarrior(posx, posy, width, widthCollision, height,
		    heightCollision, life, magic, creation, sp1, sp2, sp3);

	    break;
	case MAGE:
	    hero = new HeroMage(posx, posy, width, widthCollision, height,
		    heightCollision, life, magic, creation, sp1, sp2, sp3);
	    break;
	}
	return hero;
    }

    /**
     * Method for create an instance of Ally on top of a Tower.
     * 
     * @param entity
     *            the AllyEntity for create the new instance.
     * @param tower
     *            the TowerEntity for determinate the position and limits of the
     *            Ally.
     * @return an instance of Ally
     */
    public static Hero getHero(HeroEntity entity, TowerEntity tower) {
	// fetch data
	final float posx = tower.posx + entity.width;
	final float posy = entity.posy;
	final float width = entity.width;
	final float widthCollision = entity.widthCollision;
	final float height = entity.height;
	final float heightCollision = entity.heightCollision;
	final int life = entity.life;
	final int magic = entity.magic;
	final int creation = entity.creation;
	final int sp1 = entity.spendSp1;
	final int sp2 = entity.spendSp2;
	final int sp3 = entity.spendSp3;

	// get hero
	return getHero(entity.type, posx, posy, width, widthCollision, height,
		heightCollision, life, magic, creation, sp1, sp2, sp3);
    }

    /*
     * CREATION OF ALLIES
     */

    /**
     * Method for create an instance of Ally from an AllyEntity.
     * 
     * @param entity
     *            the AllyEntity for create the new instance.
     * 
     * @return an instance of Ally
     */
    public static Ally getAlly(AllyEntity entity) {
	// fetch data
	final float xLeftLimit = entity.xLeftLimit;
	final float xRightLimit = entity.xRightLimit;
	final float yBottomLimit = entity.yBottomLimit;
	final float width = entity.width;
	final float height = entity.height;
	// create object
	return getAlly(entity.type, width, height, xLeftLimit, xRightLimit,
		yBottomLimit);
    }

    /**
     * Method for create an instance of Ally on top of a Tower.
     * 
     * @param entity
     *            the AllyEntity for create the new instance.
     * @param tower
     *            the TowerEntity for determinate the position and limits of the
     *            Ally.
     * @return an instance of Ally
     */
    public static Ally getAlly(AllyEntity entity, TowerEntity tower) {
	// fetch data
	final float xLeftLimit = tower.allyLimitLeft;
	final float xRightLimit = tower.allyLimitRight;
	final float yBottomLimit = tower.allyLimitBottom;
	final float width = entity.width;
	final float height = entity.height;
	// create object
	return getAlly(entity.type, width, height, xLeftLimit, xRightLimit,
		yBottomLimit);
    }

    /**
     * Method for create an instance of Ally.
     * 
     * @param type
     *            the type of Ally {@link AllyType}
     * @param width
     *            the width of the Ally
     * @param height
     *            the height of the Ally
     * @param xLeftLimit
     *            the limit left of x axis
     * @param xRightLimit
     *            the limit right of x axis
     * @param yBottomLimit
     *            the floor of the Ally
     * 
     * @return an instance of Ally
     */
    public static Ally getAlly(AllyType type, float width, float height,
	    float xLeftLimit, float xRightLimit, float yBottomLimit) {
	// create object
	Ally ally = null;
	switch (type) {
	case ATTACK:
	    ally = new AllyAttack(width, height, xLeftLimit, xRightLimit,
		    yBottomLimit);
	    break;
	case NEUTRAL:
	    ally = new AllyNeutral(width, height, xLeftLimit, xRightLimit,
		    yBottomLimit);
	    break;
	case DEFENSE:
	    ally = new AllyDefense(width, height, xLeftLimit, xRightLimit,
		    yBottomLimit);
	    break;
	}
	return ally;
    }

    /*
     * CREATION OF ENEMIES
     */

    /**
     * Method for create an instance of Enemy from an EnemyEntity.
     * 
     * @param entity
     *            the EnemyEntity for create the new instance.
     * @return an instance of Enemy
     */
    public static Enemy getEnemy(EnemyEntity entity) {
	// fetch data
	final float posx = entity.posx;
	final float posy = entity.posy;
	final float width = entity.width;
	final float widthCollision = entity.width;
	final float height = entity.height;
	final float heightCollision = entity.height;
	final float moveVelocity = entity.moveVelocity;
	final int life = entity.life;
	final int damage = entity.damage;
	// create object
	return getEnemy(entity.type, posx, posy, width, widthCollision, height,
		heightCollision, life, moveVelocity, damage);
    }

    /**
     * Method for create a random remaining instance of Enemy in a WorldEntity.
     * 
     * @param entity
     *            the WorldEntity for create the new instance.
     * @return an instance of Enemy, or null if the world have not more enemies
     */
    public static Enemy getEnemy(WorldEntity entity) {
	List<EnemyEntity> enemies = entity.enemies;
	final int size = enemies.size();
	if (size > 0) {
	    final int randomPosition = Utils.randomInt(size);
	    // get random enemy
	    final EnemyEntity enemyEntity = enemies.get(randomPosition);
	    enemies.remove(randomPosition);
	    final Enemy enemy = getEnemy(enemyEntity);
	    return enemy;
	}
	return null;
    }

    /**
     * Method for create an instance of Enemy.
     * 
     * @param type
     *            the type of Enemy {@link EnemyType}
     * @param posx
     *            the initial position x of the Enemy
     * @param posy
     *            the initial position y of the Enemy
     * @param width
     *            the width of the Enemy
     * @param height
     *            the height of the Enemy
     * @param life
     *            the initial life of the Enemy
     * @param moveVelocity
     *            the velocity move of the Enemy
     * @param damage
     *            the damage that does
     * 
     * @return an instance of Enemy
     */
    public static Enemy getEnemy(EnemyType type, float posx, float posy,
	    float width, float widthCollision, float height,
	    float heightCollision, int life, float moveVelocity, int damage) {
	// create object
	Enemy enemy = null;
	switch (type) {
	case BAT:
	    enemy = new FlyEnemy(posx, posy, width, widthCollision, height,
		    heightCollision, life, moveVelocity, damage, type);
	    break;
	default:
	    enemy = new GroundEnemy(posx, posy, width, widthCollision, height,
		    heightCollision, life, moveVelocity, damage, type);
	    break;
	}
	return enemy;
    }

    /*
     * CREATION OF WORLDS
     */

    /**
     * Method for create an instance of world.
     * 
     * @param worldEntity
     *            is the entity of world.
     * @param hero
     *            is a hero.
     * @param ally
     *            is a ally.
     * @return the instance of world.
     */
    public static World getWorld(WorldEntity worldEntity, Hero hero, Ally ally) {
	// fetch data
	final float width = worldEntity.width;
	final float floor = worldEntity.floor;
	final Tower tower = getTower(worldEntity.tower);
	// set hero and ally positions

	// create world
	final World world = new World(hero, tower, ally, floor, width);
	// add grounds
	for (Iterator<GroundEntity> it = worldEntity.grounds.iterator(); it
		.hasNext();) {
	    GroundEntity groundEntity = it.next();
	    // fetch data
	    Ground ground = getGround(groundEntity);
	    // add
	    world.addGround(ground);
	}
	// return the world
	return world;
    }

    /**
     * Method for create an instance of world.
     * 
     * @param worldEntity
     *            is the world entity.
     * @param heroEntity
     *            is a hero entity.
     * @param allyEntity
     *            is a ally entity.
     * @return
     */
    public static World getWorld(WorldEntity worldEntity,
	    HeroEntity heroEntity, AllyEntity allyEntity) {
	// Objects
	final Hero hero = getHero(heroEntity, worldEntity.tower);
	final Ally ally = getAlly(allyEntity, worldEntity.tower);
	// World
	final World world = getWorld(worldEntity, hero, ally);
	return world;
    }

    // CREATION OF TOWERS
    /**
     * Method for create an instance of tower.
     * 
     * @param entity
     *            is the tower entity.
     * @return the instance of tower.
     */
    public static Tower getTower(TowerEntity entity) {
	// fetch data
	final float posx = entity.posx;
	final float posy = entity.posy;
	final float width = entity.width;
	final float height = entity.height;
	final int life = entity.life;
	// create object
	Tower tower = new Tower(posx, posy, width, height, life);
	return tower;
    }

    // CREATION OF GROUNDS
    private static Ground getGround(GroundEntity entity) {
	// fetch data
	final float posx = entity.posx;
	final float posy = entity.posy;
	final float width = entity.width;
	final float height = entity.height;
	Ground ground = new Ground(posx, posy, width, height);
	return ground;
    }
}
