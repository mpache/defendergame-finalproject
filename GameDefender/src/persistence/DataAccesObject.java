/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package persistence;

import persistence.entities.AllyEntity;
import persistence.entities.EnemyEntity;
import persistence.entities.GroundEntity;
import persistence.entities.HeroEntity;
import persistence.entities.MenuEntity;
import persistence.entities.TowerEntity;
import persistence.entities.WorldEntity;
import service.data.StateManager.GameScore;
import service.data.TypeManager.AllyType;
import service.data.TypeManager.EnemyType;
import service.data.TypeManager.HeroType;

/**
 * DataAccesObject.java
 * 
 * @version May 26, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public interface DataAccesObject {

    /**
     * Get menu entity.
     * 
     * @return the menu entity.
     */
    public abstract MenuEntity getMenuEntity();

    /**
     * Get hero entity.
     * 
     * @param type
     *            is the type of hero to get your entity.
     * @return the hero entity.
     */
    public abstract HeroEntity getHeroEntity(final HeroType type);

    /**
     * Get ally entity.
     * 
     * @param type
     *            is the type of ally to get your entity.
     * @return the ally entity.
     */
    public abstract AllyEntity getAllyEntity(final AllyType type);

    /**
     * Get enemy entity.
     * 
     * @param type
     *            is the type of enemy to get your entity.
     * 
     * @return the enemy entity.
     */
    public abstract EnemyEntity getEnemyEntity(final EnemyType type);

    /**
     * Get world entity.
     * 
     * @param worldLevel
     *            indicate the level of world.
     * @return the world entity.
     */
    public abstract WorldEntity getWorldEntity(final int worldLevel);

    /**
     * Get tower entity.
     * 
     * @param worldLevel
     *            indicate the level of world.
     * @return the tower entity.
     */
    public abstract TowerEntity getTowerEntity(final int worldLevel);

    /**
     * Get ground entity.
     * 
     * @param worldLevel
     *            indicate the level of world.
     * @param position
     *            is the position that is situated this ground in world.
     * @return the ground entity.
     */
    public abstract GroundEntity getGroundEntity(final int worldLevel,
	    final int position);

    /**
     * Get score of a world.
     * 
     * @param worldLevel
     *            indicate the level of world.
     * @return the score obtain in a world.
     */
    public abstract GameScore getScore(final int worldLevel);

    /**
     * Change score.
     * 
     * @param worldLevel
     *            indicate the level of world.
     * @param score
     *            the score to changed.
     * @return true if score has be changed, false otherwise.
     */
    public abstract boolean setScore(final int worldLevel, final GameScore score);

    /**
     * Get number of levels.
     * 
     * @return the number of levels.
     */
    public abstract int getWorldsLength();

    /**
     * Get the number of heros.
     * 
     * @return the number of heros.
     */
    public abstract int getHerosLength();

    /**
     * Get the number of allies.
     * 
     * @return the number of allies.
     */
    public abstract int getAlliesLength();

    /**
     * Get the number of enemies.
     * 
     * @return the number of enemies.
     */
    public abstract int getEnemiesLength();

}
