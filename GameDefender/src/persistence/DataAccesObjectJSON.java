/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package persistence;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import persistence.entities.AllyEntity;
import persistence.entities.EnemyEntity;
import persistence.entities.GroundEntity;
import persistence.entities.HeroEntity;
import persistence.entities.MenuEntity;
import persistence.entities.TowerEntity;
import persistence.entities.WorldEntity;
import service.data.StateManager.GameScore;
import service.data.TypeManager.AllyType;
import service.data.TypeManager.EnemyType;
import service.data.TypeManager.HeroType;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

/**
 * DataAccesObjectJSON.java
 * 
 * @version May 26, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class DataAccesObjectJSON implements DataAccesObject {

    /* CONSTANTS */
    // Data Bases names
    private static final String DATABASE_NAME = "database/database.json";
    private static final String DATABASE_SCORES_NAME = "scores.json";
    // json main fields
    private static final String JSON_FIELD_HEROS = "heros";
    private static final String JSON_FIELD_WORLDS = "worlds";
    private static final String JSON_FIELD_ALLIES = "allies";
    private static final String JSON_FIELD_ENEMIES = "enemies";
    private static final String JSON_FIELD_MENUS = "menus";
    // Menu json fields
    private static final String JSON_FIELD_MENU_MENUS = "menus";
    private static final String JSON_FIELD_MENU_COMMONS = "commons";
    private static final String JSON_FIELD_MENU_HUD = "HUD";
    // Hero json fields
    private static final String JSON_FIELD_HERO_POSX = "posx";
    private static final String JSON_FIELD_HERO_POSY = "posy";
    private static final String JSON_FIELD_HERO_WIDTH = "width";
    private static final String JSON_FIELD_HERO_WIDTHCOLLISION = "width collision";
    private static final String JSON_FIELD_HERO_HEIGHT = "height";
    private static final String JSON_FIELD_HERO_HEIGHTCOLLISION = "height collision";
    private static final String JSON_FIELD_HERO_LIFE = "life";
    private static final String JSON_FIELD_HERO_MAGIC = "magic";
    private static final String JSON_FIELD_HERO_CREATION = "creation";
    private static final String JSON_FIELD_HERO_SPEND_SP1 = "spend sp1";
    private static final String JSON_FIELD_HERO_SPEND_SP2 = "spend sp2";
    private static final String JSON_FIELD_HERO_SPEND_SP3 = "spend sp3";
    private static final String JSON_FIELD_HERO_ASSET = "asset";
    private static final int JSON_ARRAY_POSITION_HERO_WARRIOR = 0;
    private static final int JSON_ARRAY_POSITION_HERO_MAGE = 1;
    // Ally json fields
    private static final String JSON_FIELD_ALLY_XLEFTLIMIT = "xleftlimit";
    private static final String JSON_FIELD_ALLY_XRIGHTLIMIT = "xrightlimit";
    private static final String JSON_FIELD_ALLY_YBOTTOMLIMIT = "ybottomlimit";
    private static final String JSON_FIELD_ALLY_WIDTH = "width";
    private static final String JSON_FIELD_ALLY_HEIGHT = "height";
    private static final String JSON_FIELD_ALLY_ASSET = "asset";
    private static final int JSON_ARRAY_POSITION_ALLY_ATTACK = 0;
    private static final int JSON_ARRAY_POSITION_ALLY_DEFENDER = 1;
    private static final int JSON_ARRAY_POSITION_ALLY_NEUTRAL = 2;
    // enemy json fields
    private static final String JSON_FIELD_ENEMY_POSX = "posx";
    private static final String JSON_FIELD_ENEMY_POSY = "posy";
    private static final String JSON_FIELD_ENEMY_WIDTH = "width";
    private static final String JSON_FIELD_ENEMY_WIDTHCOLLISION = "width collision";
    private static final String JSON_FIELD_ENEMY_HEIGHT = "height";
    private static final String JSON_FIELD_ENEMY_HEIGHTCOLLISION = "height collision";
    private static final String JSON_FIELD_ENEMY_LIFE = "life";
    private static final String JSON_FIELD_ENEMY_MOVEVELOCITY = "movevelocity";
    private static final String JSON_FIELD_ENEMY_DAMAGE = "damage";
    private static final String JSON_FIELD_ENEMY_ASSET = "asset";
    private static final int JSON_ARRAY_POSITION_ENEMY_GOSHT_ARMOR = 0;
    private static final int JSON_ARRAY_POSITION_ENEMY_FIRE_LION = 1;
    private static final int JSON_ARRAY_POSITION_ENEMY_ELF = 2;
    private static final int JSON_ARRAY_POSITION_ENEMY_BAT = 3;
    // World json fields
    private static final String JSON_FIELD_WORLD_LEVEL = "level";
    private static final String JSON_FIELD_WORLD_WIDTH = "width";
    private static final String JSON_FIELD_WORLD_FLOOR = "floor";
    private static final String JSON_FIELD_WORLD_ENEMIES = "enemies";
    private static final String JSON_FIELD_WORLD_ENEMYDELAY = "enemy delay";
    private static final String JSON_FIELD_WORLD_ENEMYDELAY_INITIAL = "initial";
    private static final String JSON_FIELD_WORLD_ENEMYDELAY_DELAY = "delay";
    private static final String JSON_FIELD_WORLD_ENEMYDELAY_MINIMUM = "minimum";
    private static final String JSON_FIELD_WORLD_ENEMIES_POSITION = "position";
    private static final String JSON_FIELD_WORLD_ENEMIES_QUANTITY = "quantity";
    private static final String JSON_FIELD_WORLD_GROUNDS = "grounds";
    private static final String JSON_FIELD_WORLD_ASSET = "asset";
    // Tower json fields
    private static final String JSON_FIELD_WORLD_TOWER = "tower";
    private static final String JSON_FIELD_WORLD_TOWER_POSX = "posx";
    private static final String JSON_FIELD_WORLD_TOWER_POSY = "posy";
    private static final String JSON_FIELD_WORLD_TOWER_WIDTH = "width";
    private static final String JSON_FIELD_WORLD_TOWER_HEIGHT = "height";
    private static final String JSON_FIELD_WORLD_TOWER_LIFE = "life";
    private static final String JSON_FIELD_WORLD_TOWER_ALLYLIMITLEFT = "allylimitleft";
    private static final String JSON_FIELD_WORLD_TOWER_ALLYLIMITRIGHT = "allylimitright";
    private static final String JSON_FIELD_WORLD_TOWER_ALLYLIMITBOTTOM = "allylimitbottom";
    // Ground json fields
    private static final String JSON_FIELD_WORLD_GROUNDS_POSX = "posx";
    private static final String JSON_FIELD_WORLD_GROUNDS_POSY = "posy";
    private static final String JSON_FIELD_WORLD_GROUNDS_WIDTH = "width";
    private static final String JSON_FIELD_WORLD_GROUNDS_HEIGHT = "height";
    // Scores Values
    private static final int JSON_VALUE_SCORE_BLOCKED = 0;
    private static final int JSON_VALUE_SCORE_UNBLOCKED = 1;
    private static final int JSON_VALUE_SCORE_STAR1 = 2;
    private static final int JSON_VALUE_SCORE_STAR2 = 3;
    private static final int JSON_VALUE_SCORE_STAR3 = 4;

    // Data in Objects
    private JSONArray jHeros;
    private JSONArray jWorlds;
    private JSONArray jAllies;
    private JSONArray jEnemies;
    private JSONObject jMenu;
    private JSONObject jScores;

    // Singleton pattern
    private static DataAccesObjectJSON obj;

    /**
     * Singleton pattern.
     * 
     * @return a unique instance of DataAccesObjectJSON.
     */
    public static DataAccesObjectJSON getInstance() {
	if (obj == null) {
	    obj = new DataAccesObjectJSON();
	}
	return obj;
    }

    /*
     * First reading Data from DATABASE_NAME
     */
    private DataAccesObjectJSON() {
	try {
	    final FileHandle databaseFile = Gdx.files.internal(DATABASE_NAME);
	    final String data = databaseFile.readString();
	    final JSONObject jData = new JSONObject(data);
	    // Fetch Data
	    jHeros = jData.getJSONArray(JSON_FIELD_HEROS);
	    jWorlds = jData.getJSONArray(JSON_FIELD_WORLDS);
	    jAllies = jData.getJSONArray(JSON_FIELD_ALLIES);
	    jEnemies = jData.getJSONArray(JSON_FIELD_ENEMIES);
	    jMenu = jData.getJSONObject(JSON_FIELD_MENUS);
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	// Load DataBase Scores
	loadScoreData();
    }

    /**
     * Fetch data of Menus assets for create MenuEntity.
     * 
     * @return a entity of MenuEntity, or null if error when fetching data.
     */
    @Override
    public MenuEntity getMenuEntity() {
	try {
	    // fetch data
	    final String menus = jMenu.getString(JSON_FIELD_MENU_MENUS);
	    final String commons = jMenu.getString(JSON_FIELD_MENU_COMMONS);
	    final String HUD = jMenu.getString(JSON_FIELD_MENU_HUD);
	    // load data in entity
	    final MenuEntity entity = new MenuEntity(menus, commons, HUD);
	    return entity;
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return null;
    }

    /**
     * Fetch data of Heros for create HeroEntity.
     * 
     * @param type
     *            the type of Hero to load
     * 
     * @return a entity of HeroEntity, or null if error when fetching data.
     */
    @Override
    public HeroEntity getHeroEntity(final HeroType type) {
	try {
	    final int position = getPositionByHeroType(type);
	    final JSONObject jHero = this.jHeros.getJSONObject(position);
	    // fetch data
	    final float posx = (float) jHero.getDouble(JSON_FIELD_HERO_POSX);
	    final float posy = (float) jHero.getDouble(JSON_FIELD_HERO_POSY);
	    final float width = (float) jHero.getDouble(JSON_FIELD_HERO_WIDTH);
	    final float widthCollision = (float) jHero
		    .getDouble(JSON_FIELD_HERO_WIDTHCOLLISION);
	    final float height = (float) jHero
		    .getDouble(JSON_FIELD_HERO_HEIGHT);
	    final float heightCollision = (float) jHero
		    .getDouble(JSON_FIELD_HERO_HEIGHTCOLLISION);
	    final int life = jHero.getInt(JSON_FIELD_HERO_LIFE);
	    final int magic = jHero.getInt(JSON_FIELD_HERO_MAGIC);
	    final int creation = jHero.getInt(JSON_FIELD_HERO_CREATION);
	    final int spendSp1 = jHero.getInt(JSON_FIELD_HERO_SPEND_SP1);
	    final int spendSp2 = jHero.getInt(JSON_FIELD_HERO_SPEND_SP2);
	    final int spendSp3 = jHero.getInt(JSON_FIELD_HERO_SPEND_SP3);
	    final String asset = jHero.getString(JSON_FIELD_HERO_ASSET);
	    // load Data in entity
	    final HeroEntity entity = new HeroEntity(type, posx, posy, width,
		    widthCollision, height, heightCollision, life, magic,
		    creation, spendSp1, spendSp2, spendSp3, asset);
	    return entity;
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return null;
    }

    /**
     * Fetch data of Allies for create AllyEntity.
     * 
     * @param type
     *            the type of Ally to load
     * 
     * @return a entity of AllyEntity, or null if error when fetching data.
     */
    @Override
    public AllyEntity getAllyEntity(final AllyType type) {
	try {
	    final int position = getPositionByAllyType(type);
	    final JSONObject jAlly = this.jAllies.getJSONObject(position);
	    // fetch data
	    final float xLeftLimit = (float) jAlly
		    .getDouble(JSON_FIELD_ALLY_XLEFTLIMIT);
	    final float xRightLimit = (float) jAlly
		    .getDouble(JSON_FIELD_ALLY_XRIGHTLIMIT);
	    final float yBottomLimit = (float) jAlly
		    .getDouble(JSON_FIELD_ALLY_YBOTTOMLIMIT);
	    final float width = (float) jAlly.getDouble(JSON_FIELD_ALLY_WIDTH);
	    final float height = (float) jAlly
		    .getDouble(JSON_FIELD_ALLY_HEIGHT);
	    final String asset = jAlly.getString(JSON_FIELD_ALLY_ASSET);
	    // load Data in entity
	    final AllyEntity entity = new AllyEntity(type, xLeftLimit,
		    xRightLimit, yBottomLimit, width, height, asset);
	    return entity;
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return null;
    }

    /**
     * Fetch data of Enemies for create EnemyEntity.
     * 
     * @param type
     *            the type of Enemy to load
     * 
     * @return a entity of EnemyEntity, or null if error when fetching data.
     */
    @Override
    public EnemyEntity getEnemyEntity(final EnemyType type) {
	try {
	    final int position = getPositionByEnemyType(type);
	    final JSONObject jEnemy = this.jEnemies.getJSONObject(position);
	    // fetch data
	    final float posx = (float) jEnemy.getDouble(JSON_FIELD_ENEMY_POSX);
	    final float posy = (float) jEnemy.getDouble(JSON_FIELD_ENEMY_POSY);
	    final float width = (float) jEnemy
		    .getDouble(JSON_FIELD_ENEMY_WIDTH);
	    final float widthCollision = (float) jEnemy
		    .getDouble(JSON_FIELD_ENEMY_WIDTHCOLLISION);
	    final float height = (float) jEnemy
		    .getDouble(JSON_FIELD_ENEMY_HEIGHT);
	    final float heightCollision = (float) jEnemy
		    .getDouble(JSON_FIELD_ENEMY_HEIGHTCOLLISION);
	    final float moveVelocity = (float) jEnemy
		    .getDouble(JSON_FIELD_ENEMY_MOVEVELOCITY);
	    final int life = jEnemy.getInt(JSON_FIELD_ENEMY_LIFE);
	    final int damage = jEnemy.getInt(JSON_FIELD_ENEMY_DAMAGE);
	    final String asset = jEnemy.getString(JSON_FIELD_ENEMY_ASSET);
	    // load Data in entity
	    final EnemyEntity entity = new EnemyEntity(type, posx, posy, width,
		    widthCollision, height, heightCollision, moveVelocity,
		    life, damage, asset);
	    return entity;
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return null;
    }

    /**
     * Fetch data of Worlds for create WorldEntity.
     * 
     * @param worldLevel
     *            the level.
     * 
     * @return a entity of WorldEntity, or null if error when fetching data.
     */
    @Override
    public WorldEntity getWorldEntity(final int worldLevel) {
	try {
	    final JSONObject jWorld = this.jWorlds.getJSONObject(worldLevel);
	    // fetch data
	    final int level = jWorld.getInt(JSON_FIELD_WORLD_LEVEL);
	    final float width = (float) jWorld
		    .getDouble(JSON_FIELD_WORLD_WIDTH);
	    final float floor = (float) jWorld
		    .getDouble(JSON_FIELD_WORLD_FLOOR);
	    final String asset = jWorld.getString(JSON_FIELD_WORLD_ASSET);
	    final TowerEntity tower = getTowerEntity(worldLevel);
	    // enemy delay
	    final JSONObject jDelay = jWorld
		    .getJSONObject(JSON_FIELD_WORLD_ENEMYDELAY);
	    final int enemyInitialDelay = jDelay
		    .getInt(JSON_FIELD_WORLD_ENEMYDELAY_INITIAL);
	    final int enemyDelay = jDelay
		    .getInt(JSON_FIELD_WORLD_ENEMYDELAY_DELAY);
	    final int enemyMinimumDelay = jDelay
		    .getInt(JSON_FIELD_WORLD_ENEMYDELAY_MINIMUM);
	    // entity
	    final WorldEntity entity = new WorldEntity(level, width, floor,
		    tower, asset, enemyInitialDelay, enemyDelay,
		    enemyMinimumDelay);
	    // Enemies Entities
	    final JSONArray jWorldEnemies = jWorld
		    .getJSONArray(JSON_FIELD_WORLD_ENEMIES);
	    for (int i = 0; i < jWorldEnemies.length(); i++) {
		final JSONObject jEnemy = jWorldEnemies.getJSONObject(i);
		final int enemyPosition = jEnemy
			.getInt(JSON_FIELD_WORLD_ENEMIES_POSITION);
		final int enemyQuantity = jEnemy
			.getInt(JSON_FIELD_WORLD_ENEMIES_QUANTITY);
		final EnemyType type = getEnemyTypeByPosition(enemyPosition);
		for (int j = 0; j < enemyQuantity; j++) {
		    final EnemyEntity enemy = getEnemyEntity(type);
		    entity.enemies.add(enemy);
		}
	    }
	    // Grounds Entities
	    final JSONArray jWorldGrounds = jWorld
		    .getJSONArray(JSON_FIELD_WORLD_GROUNDS);
	    for (int i = 0; i < jWorldGrounds.length(); i++) {
		final JSONObject jGround = jWorldGrounds.getJSONObject(i);
		final GroundEntity ground = getGroundEntity(jGround);
		entity.grounds.add(ground);
	    }
	    // return entity
	    return entity;
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return null;
    }

    /**
     * Fetch data of World for fetch tower information from world and create
     * TowerEntity.
     * 
     * @param worldLevel
     *            the level.
     * 
     * @return a entity of TowerEntity, or null if error when fetching data.
     */
    @Override
    public TowerEntity getTowerEntity(final int worldLevel) {
	try {
	    final JSONObject jWorld = this.jWorlds.getJSONObject(worldLevel);
	    final JSONObject jTower = jWorld
		    .getJSONObject(JSON_FIELD_WORLD_TOWER);
	    // fetch data
	    final float posx = (float) jTower
		    .getDouble(JSON_FIELD_WORLD_TOWER_POSX);
	    final float posy = (float) jTower
		    .getDouble(JSON_FIELD_WORLD_TOWER_POSY);
	    final float width = (float) jTower
		    .getDouble(JSON_FIELD_WORLD_TOWER_WIDTH);
	    final float height = (float) jTower
		    .getDouble(JSON_FIELD_WORLD_TOWER_HEIGHT);
	    final int life = jTower.getInt(JSON_FIELD_WORLD_TOWER_LIFE);
	    final float allyLimitLeft = (float) jTower
		    .getDouble(JSON_FIELD_WORLD_TOWER_ALLYLIMITLEFT);
	    final float allyLimitRight = (float) jTower
		    .getDouble(JSON_FIELD_WORLD_TOWER_ALLYLIMITRIGHT);
	    final float allyLimitBottom = (float) jTower
		    .getDouble(JSON_FIELD_WORLD_TOWER_ALLYLIMITBOTTOM);
	    // load Data in entity
	    final TowerEntity entity = new TowerEntity(posx, posy, width,
		    height, life, allyLimitLeft, allyLimitRight,
		    allyLimitBottom);
	    return entity;
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return null;
    }

    private GroundEntity getGroundEntity(final JSONObject jGround) {
	try {
	    // fetch data
	    final float posx = (float) jGround
		    .getDouble(JSON_FIELD_WORLD_GROUNDS_POSX);
	    final float posy = (float) jGround
		    .getDouble(JSON_FIELD_WORLD_GROUNDS_POSY);
	    final float width = (float) jGround
		    .getDouble(JSON_FIELD_WORLD_GROUNDS_WIDTH);
	    final float height = (float) jGround
		    .getDouble(JSON_FIELD_WORLD_GROUNDS_HEIGHT);
	    // load Data in entity
	    final GroundEntity entity = new GroundEntity(posx, posy, width,
		    height);
	    return entity;
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return null;
    }

    /**
     * Fetch data of World for fetch ground information from world and create
     * GroundEntity.
     * 
     * @param worldLevel
     *            the level.
     * @param position
     *            the position of the ground into the world
     * 
     * @return a entity of GroundEntity, or null if error when fetching data.
     */
    @Override
    public GroundEntity getGroundEntity(int worldLevel, int position) {
	try {
	    final JSONObject jWorld = this.jWorlds.getJSONObject(worldLevel);
	    final JSONArray jWorldGrounds = jWorld
		    .getJSONArray(JSON_FIELD_WORLD_GROUNDS);
	    final JSONObject jGround = jWorldGrounds.getJSONObject(position);
	    return getGroundEntity(jGround);
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return null;
    }

    /*
     * Persistence of the Scores, file saved in local storage
     */
    private void loadScoreData() {
	try {
	    FileHandle databaseFile = Gdx.files.local(DATABASE_SCORES_NAME);
	    if (!databaseFile.exists()) {
		generateScoreFile(new JSONObject(), databaseFile, 0, true);
	    }
	    // read the data an save it in the json object
	    final String data = databaseFile.readString();
	    jScores = new JSONObject(data);
	    // If the number of the levels are different
	    if (jWorlds.length() > jScores.length()) {
		final GameScore lastScore = getScore(jScores.length() - 1);
		final boolean newLevel = lastScore != GameScore.BLOCKED
			&& lastScore != GameScore.UNBLOCKED;
		generateScoreFile(jScores, databaseFile, jScores.length(),
			newLevel);
	    } else if (jWorlds.length() < jScores.length()) {
		// if have to delete scores
		for (int i = jScores.length(); i > jWorlds.length(); i--) {
		    jScores.remove(String.valueOf(i - 1));
		}
		saveScoreData();
	    }
	} catch (JSONException e) {
	    e.printStackTrace();
	}
    }

    private boolean saveScoreData() {
	FileHandle databaseFile = Gdx.files.local(DATABASE_SCORES_NAME);
	if (!databaseFile.exists()) {
	    generateScoreFile(new JSONObject(), databaseFile, 0, true);
	    return false;
	}
	// write the new values and refresh the data
	databaseFile.writeString(jScores.toString(), false);
	return true;
    }

    private void generateScoreFile(JSONObject jObject, FileHandle databaseFile,
	    int initialWorld, boolean firstBlocked) {
	try {
	    for (int i = initialWorld; i < getWorldsLength(); i++) {
		if (firstBlocked) {
		    jObject.put(String.valueOf(i), JSON_VALUE_SCORE_UNBLOCKED);
		    firstBlocked = false;
		} else {
		    jObject.put(String.valueOf(i), JSON_VALUE_SCORE_BLOCKED);
		}
	    }
	    databaseFile.writeString(jObject.toString(), false);
	} catch (JSONException e) {
	    e.printStackTrace();
	}
    }

    @Override
    public GameScore getScore(final int worldLevel) {
	try {
	    final int score = jScores.getInt(String.valueOf(worldLevel));
	    return getGameScoreByValue(score);
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return null;
    }

    @Override
    public boolean setScore(final int worldLevel, final GameScore score) {
	try {
	    // check world level an score
	    if (worldLevel < 0 || worldLevel >= getWorldsLength()
		    || score == null) {
		return false;
	    }
	    // Check Value
	    final int valueToSave = getValueByGameScore(score);
	    final int currentValue = getValueByGameScore(getScore(worldLevel));
	    if (valueToSave <= currentValue) {
		return false;
	    }
	    // save it
	    jScores.put(String.valueOf(worldLevel), valueToSave);
	    return saveScoreData();
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	return false;
    }

    /*
     * Values of the Object Types for write and read in the json
     */

    // Ally
    private int getPositionByAllyType(AllyType type) {
	switch (type) {
	case ATTACK:
	    return JSON_ARRAY_POSITION_ALLY_ATTACK;
	case DEFENSE:
	    return JSON_ARRAY_POSITION_ALLY_DEFENDER;
	case NEUTRAL:
	    return JSON_ARRAY_POSITION_ALLY_NEUTRAL;
	}
	return -1;
    }

    // Hero
    private int getPositionByHeroType(HeroType type) {
	switch (type) {
	case MAGE:
	    return JSON_ARRAY_POSITION_HERO_MAGE;
	case WARRIOR:
	    return JSON_ARRAY_POSITION_HERO_WARRIOR;
	}
	return -1;
    }

    // Enemy
    private int getPositionByEnemyType(EnemyType type) {
	switch (type) {
	case BAT:
	    return JSON_ARRAY_POSITION_ENEMY_BAT;
	case ELF:
	    return JSON_ARRAY_POSITION_ENEMY_ELF;
	case FIRE_LION:
	    return JSON_ARRAY_POSITION_ENEMY_FIRE_LION;
	case GHOST_ARMOR:
	    return JSON_ARRAY_POSITION_ENEMY_GOSHT_ARMOR;
	}
	return -1;
    }

    private EnemyType getEnemyTypeByPosition(final int position) {
	switch (position) {
	case JSON_ARRAY_POSITION_ENEMY_GOSHT_ARMOR:
	    return EnemyType.GHOST_ARMOR;
	case JSON_ARRAY_POSITION_ENEMY_FIRE_LION:
	    return EnemyType.FIRE_LION;
	case JSON_ARRAY_POSITION_ENEMY_ELF:
	    return EnemyType.ELF;
	case JSON_ARRAY_POSITION_ENEMY_BAT:
	    return EnemyType.BAT;
	}
	return null;
    }

    // GameScore
    private int getValueByGameScore(GameScore score) {
	switch (score) {
	case BLOCKED:
	    return JSON_VALUE_SCORE_BLOCKED;
	case STAR1:
	    return JSON_VALUE_SCORE_STAR1;
	case STAR2:
	    return JSON_VALUE_SCORE_STAR2;
	case STAR3:
	    return JSON_VALUE_SCORE_STAR3;
	case UNBLOCKED:
	    return JSON_VALUE_SCORE_UNBLOCKED;
	}
	return -1;
    }

    private GameScore getGameScoreByValue(final int value) {
	switch (value) {
	case JSON_VALUE_SCORE_BLOCKED:
	    return GameScore.BLOCKED;
	case JSON_VALUE_SCORE_STAR1:
	    return GameScore.STAR1;
	case JSON_VALUE_SCORE_STAR2:
	    return GameScore.STAR2;
	case JSON_VALUE_SCORE_STAR3:
	    return GameScore.STAR3;
	case JSON_VALUE_SCORE_UNBLOCKED:
	    return GameScore.UNBLOCKED;
	}
	return null;
    }

    /*
     * Getters of Length
     */
    @Override
    public int getWorldsLength() {
	return jWorlds.length();
    }

    @Override
    public int getHerosLength() {
	return jHeros.length();
    }

    @Override
    public int getAlliesLength() {
	return jAllies.length();
    }

    @Override
    public int getEnemiesLength() {
	return jEnemies.length();
    }
}
