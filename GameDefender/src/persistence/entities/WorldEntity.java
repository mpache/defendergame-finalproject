/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package persistence.entities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * WorldEntity.java
 *
 * @version May 26, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 *
 */
public class WorldEntity {

    public final int level;
    public final float width;
    public final float floor;
    public final TowerEntity tower;
    public final String asset;
    public final int enemyInitialDelay;
    public final int enemyDelay;
    public final int enemyMinimumDelay;
    public final List<EnemyEntity> enemies;
    public final Set<GroundEntity> grounds;

    public WorldEntity(int level, float width, float floor, TowerEntity tower,
	    String asset, int enemyInitialDelay, int enemyDelay,
	    int enemyMinimumDelay) {
	this.level = level;
	this.width = width;
	this.floor = floor;
	this.tower = tower;
	this.asset = asset;
	this.enemyInitialDelay = enemyInitialDelay;
	this.enemyDelay = enemyDelay;
	this.enemyMinimumDelay = enemyMinimumDelay;
	this.enemies = new ArrayList<EnemyEntity>();
	this.grounds = new HashSet<GroundEntity>();
    }

    @Override
    public String toString() {
	return "WorldEntity [level=" + level + ", width=" + width + ", floor="
		+ floor + ", tower=" + tower + ", asset=" + asset
		+ ", delayInitial=" + enemyInitialDelay + ", delay="
		+ enemyDelay + ", delayMinimum=" + enemyMinimumDelay
		+ ", enemies=" + enemies + ", grounds=" + grounds + "]";
    }
}
