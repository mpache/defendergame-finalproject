/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package persistence.entities;

import service.data.TypeManager.AllyType;

/**
 * AllyEntity.java
 * 
 * @version May 26, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class AllyEntity {

    /* Fields */
    public final AllyType type;
    public final float xLeftLimit;
    public final float xRightLimit;
    public final float yBottomLimit;
    public final float width;
    public final float height;
    public final String asset;

    /**
     * AllyEntity contructor.
     * 
     * @param type
     *            is the type of ally.
     * @param xLeftLimit
     *            is the limit to move in left direction.
     * @param xRightLimit
     *            is the limit to move in rigth direction.
     * @param yBottomLimit
     *            is the floor of ally.
     * @param width
     *            is the width of ally.
     * @param height
     *            is the height of ally.
     * @param asset
     *            is the asset of ally.
     */
    public AllyEntity(AllyType type, float xLeftLimit, float xRightLimit,
	    float yBottomLimit, float width, float height, String asset) {
	this.type = type;
	this.xLeftLimit = xLeftLimit;
	this.xRightLimit = xRightLimit;
	this.yBottomLimit = yBottomLimit;
	this.width = width;
	this.height = height;
	this.asset = asset;
    }

    @Override
    public String toString() {
	return "AllyEntity [type=" + type + ", xLeftLimit=" + xLeftLimit
		+ ", xRightLimit=" + xRightLimit + ", yBottomLimit="
		+ yBottomLimit + ", width=" + width + ", height=" + height
		+ ", asset=" + asset + "]";
    }

}
