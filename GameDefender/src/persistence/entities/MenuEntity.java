/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package persistence.entities;

/**
 * MenuEntity.java
 * 
 * @version May 26, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class MenuEntity {

    public final String menus;
    public final String commons;
    public final String HUD;

    /**
     * MenuEntity constructor.
     * 
     * @param menus
     *            is the asset of menus.
     * @param commons
     *            is the asset of commons.
     * @param hUD
     *            is the asset of HUD.
     */
    public MenuEntity(String menus, String commons, String hUD) {
	this.menus = menus;
	this.commons = commons;
	HUD = hUD;
    }

    @Override
    public String toString() {
	return "MenuEntity [menus=" + menus + ", commons=" + commons + ", HUD="
		+ HUD + "]";
    }
}
