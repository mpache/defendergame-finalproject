/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package persistence.entities;

import service.data.TypeManager.HeroType;

/**
 * HeroEntity.java
 *
 * @version May 26, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 *
 */
public class HeroEntity {

    public final HeroType type;
    public final float posx;
    public final float posy;
    public final float width;
    public final float widthCollision;
    public final float height;
    public final float heightCollision;
    public final int life;
    public final int magic;
    public final int creation;
    public final int spendSp1;
    public final int spendSp2;
    public final int spendSp3;
    public final String asset;

    public HeroEntity(HeroType type, float posx, float posy, float width,
	    float widthCollision, float height, float heightCollision,
	    int life, int magic, int creation, int spendSp1, int spendSp2,
	    int spendSp3, String asset) {
	this.type = type;
	this.posx = posx;
	this.posy = posy;
	this.width = width;
	this.widthCollision = widthCollision;
	this.height = height;
	this.heightCollision = heightCollision;
	this.life = life;
	this.magic = magic;
	this.creation = creation;
	this.spendSp1 = spendSp1;
	this.spendSp2 = spendSp2;
	this.spendSp3 = spendSp3;
	this.asset = asset;
    }

    @Override
    public String toString() {
	return "HeroEntity [type=" + type + ", posx=" + posx + ", posy=" + posy
		+ ", width=" + width + ", widthCollision=" + widthCollision
		+ ", height=" + height + ", heightCollision=" + heightCollision
		+ ", life=" + life + ", magic=" + magic + ", creation="
		+ creation + ", spendSp1=" + spendSp1 + ", spendSp2="
		+ spendSp2 + ", spendSp3=" + spendSp3 + ", asset=" + asset
		+ "]";
    }
}
