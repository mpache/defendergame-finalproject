/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package persistence.entities;

import service.data.TypeManager.EnemyType;

/**
 * EnemyEntity.java
 *
 * @version May 26, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 *
 */
public class EnemyEntity {

    public final EnemyType type;
    public final float posx;
    public final float posy;
    public final float width;
    public final float widthCollision;
    public final float height;
    public final float heightCollision;
    public final float moveVelocity;
    public final int life;
    public final int damage;
    public final String asset;

    public EnemyEntity(EnemyType type, float posx, float posy, float width,
	    float widthCollision, float height, float heightCollision,
	    float moveVelocity, int life, int damage, String asset) {
	this.type = type;
	this.posx = posx;
	this.posy = posy;
	this.width = width;
	this.widthCollision = widthCollision;
	this.height = height;
	this.heightCollision = heightCollision;
	this.moveVelocity = moveVelocity;
	this.life = life;
	this.damage = damage;
	this.asset = asset;
    }

    @Override
    public String toString() {
	return "EnemyEntity [type=" + type + ", posx=" + posx + ", posy="
		+ posy + ", width=" + width + ", widthCollision="
		+ widthCollision + ", height=" + height + ", heightCollision="
		+ heightCollision + ", moveVelocity=" + moveVelocity
		+ ", life=" + life + ", damage=" + damage + ", asset=" + asset
		+ "]";
    }

}
