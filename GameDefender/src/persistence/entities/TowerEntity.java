/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package persistence.entities;

/**
 * TowerEntity.java
 *
 * @version May 26, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 *
 */
public class TowerEntity {

    public final float posx;
    public final float posy;
    public final float width;
    public final float height;
    public final int life;
    public final float allyLimitLeft;
    public final float allyLimitRight;
    public final float allyLimitBottom;

    public TowerEntity(float posx, float posy, float width, float height,
	    int life, float allyLimitLeft, float allyLimitRight,
	    float allyLimitBottom) {
	this.posx = posx;
	this.posy = posy;
	this.width = width;
	this.height = height;
	this.life = life;
	this.allyLimitLeft = allyLimitLeft;
	this.allyLimitRight = allyLimitRight;
	this.allyLimitBottom = allyLimitBottom;
    }

    @Override
    public String toString() {
	return "TowerEntity [posx=" + posx + ", posy=" + posy + ", width="
		+ width + ", height=" + height + ", life=" + life
		+ ", allyLimitLeft=" + allyLimitLeft + ", allyLimitRight="
		+ allyLimitRight + ", allyLimitBottom=" + allyLimitBottom + "]";
    }

}