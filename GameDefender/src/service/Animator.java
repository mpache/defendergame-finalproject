/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package service;

import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * Animator.java
 * 
 * @version Apr 30, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class Animator {

    /* Constants */
    public static final int ANIMATION_LOOPING = 0;
    public static final int ANIMATION_NONLOOPING = 1;

    /* Fields */
    private final Sprite[] keyFrames;
    private final float frameDuration;

    /**
     * Animator constructor.
     * 
     * @param frameDuration
     *            is the duration between frames.
     * @param keyFrames
     *            is the array of frames to animate it.
     */
    public Animator(float frameDuration, Sprite... keyFrames) {
	this.frameDuration = frameDuration;
	this.keyFrames = keyFrames;
    }

    /**
     * Return the frame to animate.
     * 
     * @param stateTime
     *            the frame to draw.
     * @param mode
     *            to indicate animation is looping or not.
     * @return a sprite represents frame to draw.
     */
    public Sprite getKeyFrame(float stateTime, int mode) {
	// Obtain the frame to animate.
	int frameNumber = (int) (stateTime / frameDuration);
	if (mode == ANIMATION_NONLOOPING) {
	    frameNumber = Math.min(keyFrames.length - 1, frameNumber);
	} else {
	    frameNumber = frameNumber % keyFrames.length;
	}
	return keyFrames[frameNumber];
    }
    
    /**
     * Obtain duration of frames.
     * 
     * @return the duration of frames.
     */
    public float getDuration() {
	return this.frameDuration;
    }
    
    /**
     * obtain the size of this array of frames.
     * 
     * @return the size of this animation.
     */
    public int size() {
	return this.keyFrames.length;
    }
}
