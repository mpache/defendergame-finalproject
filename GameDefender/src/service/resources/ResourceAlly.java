/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package service.resources;

import service.Animator;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

/**
 * ResourceAlly.java
 * 
 * @version May 26, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class ResourceAlly {

    public static Sprite allyFront;
    public static Sprite allyFace;
    public static Animator allyWalk, allyCreated;
    public static Sprite lifeItem;
    public static Sprite magicItem;

    private ResourceAlly() {
    }

    /**
     * Initialized resources.
     * 
     * @param manager
     *            is the asset manager.
     */
    public static void initResources(AssetManager manager) {
	TextureAtlas atlas = manager.get(ResourcesManager.ALLY);

	allyFront = atlas.createSprite("front");
	allyFace = atlas.createSprite("walk1");

	allyCreated = new Animator(0.25f, atlas.createSprite("create1"),
		atlas.createSprite("create2"), allyFront,
		atlas.createSprite("create4"), atlas.createSprite("create5"),
		atlas.createSprite("create4"), allyFront);

	allyWalk = new Animator(0.25f, atlas.createSprite("walk1"),
		atlas.createSprite("walk2"), atlas.createSprite("walk3"));

	lifeItem = atlas.createSprite("life");
	magicItem = atlas.createSprite("magic");
    }
}
