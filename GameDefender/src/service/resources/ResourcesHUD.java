/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package service.resources;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

/**
 * ResourcesHUD.java
 * 
 * @version May 26, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class ResourcesHUD {

    // Hero
    public static Sprite avatar;
    public static Sprite lifeBar, magicBar, creationBar;
    public static Sprite face1Hero1, face2Hero1;
    // Buttons.
    public static Sprite buttonPauseUp, buttonPauseDown;
    public static Sprite buttonSp1Up, buttonSp1Down;
    public static Sprite buttonSp2Up, buttonSp2Down;
    public static Sprite buttonSp3Up, buttonSp3Down;
    public static Sprite buttonTutorialsUp, buttonTutorialsDown;
    public static Sprite buttonHomeUp, buttonHomeDown, buttonRetryUp,
	    buttonRetryDown, buttonNextUp, buttonNextDown;
    // Pause
    public static Sprite paused;
    public static Sprite pausedBackground;
    public static Sprite asideBarLeft, asideBarRight;
    // Win
    public static Sprite win;
    public static Sprite winBackground;
    public static Sprite star, noStar;
    // Lose
    public static Sprite lose;
    public static Sprite loseBackground;

    // ==============
    // MOBILE CONTROL
    // ==============
    public static Sprite bakgroundControl, knobControl;
    public static Sprite buttonAttackUp, buttonAttackDown;
    public static Sprite buttonJumpUp, buttonJumpDown;

    private ResourcesHUD() {
    }

    /**
     * Initialized resources.
     * 
     * @param manager
     *            is the asset manager.
     */
    public static void initResources(AssetManager manager) {
	// Create atlas.
	TextureAtlas elementsAtlas = manager.get(ResourcesManager.HUD);

	// load buttons
	buttonPauseUp = elementsAtlas.createSprite("buttonPauseUp");
	buttonPauseDown = elementsAtlas.createSprite("buttonPauseDown");
	// Special buttons.
	buttonSp1Up = elementsAtlas.createSprite("buttonSp1Up");
	buttonSp1Down = elementsAtlas.createSprite("buttonSp1Down");
	buttonSp2Up = elementsAtlas.createSprite("buttonSp2Up");
	buttonSp2Down = elementsAtlas.createSprite("buttonSp2Down");
	buttonSp3Up = elementsAtlas.createSprite("buttonSp3Up");
	buttonSp3Down = elementsAtlas.createSprite("buttonSp3Down");
	// attack and jump
	buttonAttackUp = elementsAtlas.createSprite("buttonAttackUp");
	buttonAttackDown = elementsAtlas.createSprite("buttonAttackDown");
	buttonJumpUp = elementsAtlas.createSprite("buttonJumpUp");
	buttonJumpDown = elementsAtlas.createSprite("buttonJumpDown");
	// Mobile control
	bakgroundControl = elementsAtlas.createSprite("control");
	knobControl = elementsAtlas.createSprite("controlMove");

	// Hero
	avatar = elementsAtlas.createSprite("avatar");
	lifeBar = elementsAtlas.createSprite("lifeBar");
	magicBar = elementsAtlas.createSprite("magicBar");
	creationBar = elementsAtlas.createSprite("creationBar");

	// paused, win, lose
	buttonTutorialsUp = elementsAtlas.createSprite("tutorialUp");
	buttonTutorialsDown = elementsAtlas.createSprite("tutorialDown");
	buttonHomeUp = elementsAtlas.createSprite("homeUp");
	buttonHomeDown = elementsAtlas.createSprite("homeDown");
	buttonRetryUp = elementsAtlas.createSprite("retryUp");
	buttonRetryDown = elementsAtlas.createSprite("retryDown");
	asideBarLeft = elementsAtlas.createSprite("asideBar");
	asideBarRight = elementsAtlas.createSprite("asideBar");
	asideBarRight.flip(true, false);
	paused = elementsAtlas.createSprite("paused");
	win = elementsAtlas.createSprite("win");
	lose = elementsAtlas.createSprite("lose");
	star = elementsAtlas.createSprite("star");
	noStar = elementsAtlas.createSprite("noStar");

	// Backgrounds
	pausedBackground = elementsAtlas.createSprite("pausedBackground");
	winBackground = elementsAtlas.createSprite("winBackground");
	loseBackground = elementsAtlas.createSprite("loseBackground");

    }
}
