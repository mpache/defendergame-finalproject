/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package service.resources;

import service.Animator;
import service.data.TypeManager.HeroType;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

/**
 * ResourceHeros.java
 * 
 * @version May 11, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class ResourceHeros {

    /* Fields */
    public static Sprite heroFace1, heroFace2;
    public static Sprite heroJump, heroBegindDamage, heroFrontDamage;
    public static Animator heroStance, heroRun, heroWin, heroLose, heroDead,
	    heroGrAttackStance, heroGrAttackRun, heroAirAttack, heroGrSp1,
	    heroAirSp1, heroGrSp2, heroAirSp2, heroGrSp3, heroAirSp3;
    public static Animator heroGrAttackStanceEffects, heroAirAttackEffects,
	    heroGrSp1Effects, heroAirSp1Effects, heroGrSp2Effects,
	    heroAirSp2Effects, heroGrSp3Effects, heroAirSp3Effects;

    /**
     * Initialized resources.
     * 
     * @param manager
     *            is the asset manager.
     * @param hero
     *            is the type of hero.
     */
    public static void initResource(AssetManager manager, HeroType hero) {
	TextureAtlas atlas = manager.get(ResourcesManager.HERO);
	heroFace1 = atlas.createSprite("face1");
	heroFace2 = atlas.createSprite("face2");
	switch (hero) {
	case WARRIOR:
	    loadAnimationsWarrior(atlas);
	    break;
	case MAGE:
	    loadAnimationsMage(atlas);
	    break;
	}
    }

    // Load Animations.
    private static void loadAnimationsWarrior(TextureAtlas atlas) {
	heroJump = atlas.createSprite("jump");
	heroBegindDamage = atlas.createSprite("behindDamage");
	heroFrontDamage = atlas.createSprite("frontDamage");
	heroStance = new Animator(7f, atlas.createSprite("stance1"),
		atlas.createSprite("stance2"), atlas.createSprite("stance3"),
		atlas.createSprite("stance4"));
	heroRun = new Animator(7f, atlas.createSprite("run1"),
		atlas.createSprite("run2"), atlas.createSprite("run3"),
		atlas.createSprite("run4"), atlas.createSprite("run5"),
		atlas.createSprite("run6"), atlas.createSprite("run7"),
		atlas.createSprite("run8"));
	heroWin = new Animator(7f, atlas.createSprite("win1"),
		atlas.createSprite("win2"), atlas.createSprite("win3"),
		atlas.createSprite("win4"), atlas.createSprite("win5"),
		atlas.createSprite("win6"));
	heroDead = new Animator(7f, atlas.createSprite("dead1"),
		atlas.createSprite("dead2"), atlas.createSprite("dead3"));
	heroGrAttackStance = new Animator(7f,
		atlas.createSprite("grAttackStance1"),
		atlas.createSprite("grAttackStance2"),
		atlas.createSprite("grAttackStance3"),
		atlas.createSprite("grAttackStance4"),
		atlas.createSprite("grAttackStance5"),
		atlas.createSprite("grAttackStance6"),
		atlas.createSprite("grAttackStance7"));
	heroAirAttack = new Animator(7f, atlas.createSprite("airAttack1"),
		atlas.createSprite("airAttack2"),
		atlas.createSprite("airAttack3"),
		atlas.createSprite("airAttack4"),
		atlas.createSprite("airAttack5"),
		atlas.createSprite("airAttack6"),
		atlas.createSprite("airAttack7"),
		atlas.createSprite("airAttack8"));
    }

    private static void loadAnimationsMage(TextureAtlas atlas) {
	heroJump = atlas.createSprite("jump");
	heroBegindDamage = atlas.createSprite("behindDamage");
	heroFrontDamage = atlas.createSprite("frontDamage");
	heroAirSp1 = new Animator(7f, atlas.createSprite("airSp1-1"));
	heroStance = new Animator(7f, atlas.createSprite("stance1"),
		atlas.createSprite("stance2"), atlas.createSprite("stance3"),
		atlas.createSprite("stance4"));
	heroRun = new Animator(7f, atlas.createSprite("run1"),
		atlas.createSprite("run2"), atlas.createSprite("run3"),
		atlas.createSprite("run4"), atlas.createSprite("run5"),
		atlas.createSprite("run6"));
	heroWin = new Animator(7f, atlas.createSprite("win1"),
		atlas.createSprite("win2"), atlas.createSprite("win3"));
	heroLose = new Animator(7f, atlas.createSprite("lose1"),
		atlas.createSprite("lose2"), atlas.createSprite("lose3"),
		atlas.createSprite("lose4"));
	heroDead = new Animator(7f, atlas.createSprite("dead1"),
		atlas.createSprite("dead2"), atlas.createSprite("dead3"));
	heroGrAttackStance = new Animator(7f,
		atlas.createSprite("grAttackStance1"),
		atlas.createSprite("grAttackStance2"),
		atlas.createSprite("grAttackStance3"),
		atlas.createSprite("grAttackStance4"),
		atlas.createSprite("grAttackStance5"),
		atlas.createSprite("grAttackStance6"));
	heroGrAttackRun = new Animator(7f, atlas.createSprite("grAttackRun1"),
		atlas.createSprite("grAttackRun2"),
		atlas.createSprite("grAttackRun3"),
		atlas.createSprite("grAttackRun4"),
		atlas.createSprite("grAttackRun5"),
		atlas.createSprite("grAttackRun6"),
		atlas.createSprite("grAttackRun7"),
		atlas.createSprite("grAttackRun8"),
		atlas.createSprite("grAttackRun9"),
		atlas.createSprite("grAttackRun10"),
		atlas.createSprite("grAttackRun10"),
		atlas.createSprite("grAttackRun11"),
		atlas.createSprite("grAttackRun12"));
	heroAirAttack = new Animator(7f, atlas.createSprite("airAttack1"),
		atlas.createSprite("airAttack2"),
		atlas.createSprite("airAttack3"),
		atlas.createSprite("airAttack4"));
	heroGrSp1 = new Animator(7f, atlas.createSprite("groundSp1-1"),
		atlas.createSprite("groundSp1-1"),
		atlas.createSprite("groundSp1-2"),
		atlas.createSprite("groundSp1-3"),
		atlas.createSprite("groundSp1-4"),
		atlas.createSprite("groundSp1-5"),
		atlas.createSprite("groundSp1-6"),
		atlas.createSprite("groundSp1-7"),
		atlas.createSprite("groundSp1-8"),
		atlas.createSprite("groundSp1-9"),
		atlas.createSprite("groundSp1-10"),
		atlas.createSprite("groundSp1-11"),
		atlas.createSprite("groundSp1-12"));
	heroGrSp2 = new Animator(7f, atlas.createSprite("groundSp2-1"),
		atlas.createSprite("groundSp2-2"),
		atlas.createSprite("groundSp2-3"),
		atlas.createSprite("groundSp2-4"),
		atlas.createSprite("groundSp2-5"),
		atlas.createSprite("groundSp2-6"),
		atlas.createSprite("groundSp2-7"),
		atlas.createSprite("groundSp2-8"),
		atlas.createSprite("groundSp2-9"),
		atlas.createSprite("groundSp2-10"),
		atlas.createSprite("groundSp2-11"));
	heroAirSp2 = new Animator(7f, atlas.createSprite("airSp2-1"),
		atlas.createSprite("airSp2-2"), atlas.createSprite("airSp2-3"),
		atlas.createSprite("airSp2-4"), atlas.createSprite("airSp2-5"),
		atlas.createSprite("airSp2-6"));
	heroGrSp3 = new Animator(7f, atlas.createSprite("sp3-1"),
		atlas.createSprite("sp3-2"), atlas.createSprite("sp3-3"),
		atlas.createSprite("sp3-4"), atlas.createSprite("sp3-5"),
		atlas.createSprite("sp3-6"), atlas.createSprite("sp3-7"),
		atlas.createSprite("sp3-8"), atlas.createSprite("sp3-9"),
		atlas.createSprite("sp3-10"), atlas.createSprite("sp3-11"),
		atlas.createSprite("sp3-12"), atlas.createSprite("sp3-13"),
		atlas.createSprite("sp3-14"), atlas.createSprite("sp3-15"),
		atlas.createSprite("sp3-16"), atlas.createSprite("sp3-17"),
		atlas.createSprite("sp3-18"), atlas.createSprite("sp3-19"),
		atlas.createSprite("sp3-20"), atlas.createSprite("sp3-21"),
		atlas.createSprite("sp3-22"), atlas.createSprite("sp3-23"),
		atlas.createSprite("sp3-24"), atlas.createSprite("sp3-25"),
		atlas.createSprite("sp3-26"), atlas.createSprite("sp3-27"),
		atlas.createSprite("sp3-28"));

	// ======================
	// EFFECTS (PROYECTILES)
	// ======================
	heroGrAttackStanceEffects = new Animator(0.25f,
		atlas.createSprite("grAttackEffect1"));
	heroGrSp1Effects = new Animator(0.55f,
		atlas.createSprite("grSp1Effect4"));
	heroAirSp1Effects = new Animator(0.25f,
		atlas.createSprite("airSp1Effect1"));
	heroGrSp2Effects = new Animator(0.25f,
		atlas.createSprite("grSp2Effect1"),
		atlas.createSprite("grSp2Effect2"),
		atlas.createSprite("grSp2Effect3"),
		atlas.createSprite("grSp2Effect4"));
	heroAirSp2Effects = new Animator(0.1f,
		atlas.createSprite("airSp2Effect1"),
		atlas.createSprite("airSp2Effect2"),
		atlas.createSprite("airSp2Effect3"),
		atlas.createSprite("airSp2Effect4"),
		atlas.createSprite("airSp2Effect5"),
		atlas.createSprite("airSp2Effect6"));
    }

    private void loadAnimationsHero3(TextureAtlas atlas) {

    }

    private void loadAnimationsHero4(TextureAtlas atlas) {

    }
}
