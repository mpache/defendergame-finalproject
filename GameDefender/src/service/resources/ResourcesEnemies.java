/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package service.resources;

import service.Animator;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

/**
 * ResourcesEnemies.java
 *
 * @version May 26, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 *
 */
public class ResourcesEnemies {
    public static TextureAtlas GHOST_ARMOR_ATLAS;
    public static TextureAtlas FIRE_LION_ATLAS;
    public static TextureAtlas BAT_ATLAS;

    private ResourcesEnemies() {
    }

    public static void initGhostArmorResources(AssetManager manager) {
	GHOST_ARMOR_ATLAS = manager.get(ResourcesManager.GHOST_ARMOR_ASSET);
    }

    public static void initFireLionResources(AssetManager manager) {
	FIRE_LION_ATLAS = manager.get(ResourcesManager.FIRE_LION_ASSET);
    }

    public static void initBatResources(AssetManager manager) {
	BAT_ATLAS = manager.get(ResourcesManager.BAT_ASSET);
    }

    public static abstract class EnemyUI {
	protected Sprite inactive;
	protected Sprite kicked;
	protected Sprite attack;
	protected Animator walk, dead;
	public float stateTime;
	public float deadTime;

	public Sprite getInactive() {
	    return inactive;
	}

	public Sprite getKicked() {
	    return kicked;
	}

	public Sprite getAttack() {
	    return attack;
	}

	public Animator getWalk() {
	    return walk;
	}

	public Animator getDead() {
	    return dead;
	}

    }

    public static class EnemyGhostArmorUI extends EnemyUI {

	public EnemyGhostArmorUI() {
	    inactive = GHOST_ARMOR_ATLAS.createSprite("inactive");
	    kicked = GHOST_ARMOR_ATLAS.createSprite("kicked");
	    attack = GHOST_ARMOR_ATLAS.createSprite("hit");
	    walk = new Animator(0.4f, GHOST_ARMOR_ATLAS.createSprite("walk1"),
		    GHOST_ARMOR_ATLAS.createSprite("walk2"));
	    dead = new Animator(0.4f, GHOST_ARMOR_ATLAS.createSprite("dead1"),
		    GHOST_ARMOR_ATLAS.createSprite("dead2"),
		    GHOST_ARMOR_ATLAS.createSprite("dead3"),
		    GHOST_ARMOR_ATLAS.createSprite("dead4"),
		    GHOST_ARMOR_ATLAS.createSprite("dead5"));
	    stateTime = 0;
	    deadTime = 0;
	}
    }

    public static class EnemyFireLionUI extends EnemyUI {

	public EnemyFireLionUI() {
	    inactive = FIRE_LION_ATLAS.createSprite("walk1");
	    kicked = FIRE_LION_ATLAS.createSprite("kicked");
	    attack = FIRE_LION_ATLAS.createSprite("attack2");
	    walk = new Animator(0.15f, FIRE_LION_ATLAS.createSprite("walk1"),
		    FIRE_LION_ATLAS.createSprite("walk2"),
		    FIRE_LION_ATLAS.createSprite("walk3"),
		    FIRE_LION_ATLAS.createSprite("walk4"));
	    dead = new Animator(0.2f, FIRE_LION_ATLAS.createSprite("dead1"),
		    FIRE_LION_ATLAS.createSprite("dead2"),
		    FIRE_LION_ATLAS.createSprite("dead3"),
		    FIRE_LION_ATLAS.createSprite("dead4"),
		    FIRE_LION_ATLAS.createSprite("dead5"));
	    stateTime = 0;
	    deadTime = 0;
	}
    }

    public static class EnemyBatUI extends EnemyUI {

	public EnemyBatUI() {
	    inactive = BAT_ATLAS.createSprite("fly2");
	    kicked = BAT_ATLAS.createSprite("kicked");
	    attack = BAT_ATLAS.createSprite("attack2");
	    walk = new Animator(0.15f, BAT_ATLAS.createSprite("fly1"),
		    BAT_ATLAS.createSprite("fly2"));
	    dead = new Animator(0.4f, BAT_ATLAS.createSprite("dead1"),
		    BAT_ATLAS.createSprite("dead2"),
		    BAT_ATLAS.createSprite("dead3"));
	    stateTime = 0;
	    deadTime = 0;
	}
    }

}
