/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package service.resources;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

/**
 * ResourcesWorld.java
 * 
 * @version May 26, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class ResourcesWorld {

    // Stage
    public static Sprite stage1;
    public static Sprite tower;
    public static Sprite towerIcon;
    public static Sprite ground;

    /**
     * Initialized resources.
     * 
     * @param manager
     *            is the asset manager.
     */
    public static void initResources(AssetManager manager) {
	TextureAtlas stages = manager.get(ResourcesManager.WORLD);
	// Load stage and tower.
	stage1 = stages.createSprite("stage1");
	stage1.setPosition(0, 0);
	tower = stages.createSprite("tower");
	ground = stages.createSprite("ground");
	towerIcon = stages.createSprite("towerIcon");
    }
}
