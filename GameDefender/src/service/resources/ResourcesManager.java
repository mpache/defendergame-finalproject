/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package service.resources;

import java.util.HashSet;
import java.util.Iterator;

import persistence.entities.AllyEntity;
import persistence.entities.EnemyEntity;
import persistence.entities.HeroEntity;
import persistence.entities.MenuEntity;
import persistence.entities.WorldEntity;
import service.data.SettingManager;
import service.data.TypeManager.EnemyType;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

/**
 * ResourcesManager.java
 *
 * @version May 26, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 *
 */
public class ResourcesManager {

    private ResourcesManager() {
    }

    // Menu
    public static String MENU_COMMONS;
    public static String MENU_MENUS;

    public static AssetManager getMenuAssetManager() {
	// load Strings
	MenuEntity menu = SettingManager.DAO.getMenuEntity();
	MENU_MENUS = menu.menus;
	MENU_COMMONS = menu.commons;
	HUD = menu.HUD;

	// load manager
	AssetManager manager = new AssetManager();
	manager.load(MENU_MENUS, TextureAtlas.class);
	manager.load(MENU_COMMONS, TextureAtlas.class);
	return manager;
    }

    // Game Loop
    public static String HUD;
    public static String WORLD;
    public static String HERO;
    public static String ALLY;

    public static AssetManager getGameLoopAssetManager(HeroEntity hero,
	    AllyEntity ally, WorldEntity world) {
	// load Strings
	MenuEntity menu = SettingManager.DAO.getMenuEntity();
	HUD = menu.HUD;
	WORLD = world.asset;
	HERO = hero.asset;
	ALLY = ally.asset;

	// load manager
	AssetManager manager = new AssetManager();
	manager.load(HUD, TextureAtlas.class);
	manager.load(WORLD, TextureAtlas.class);
	manager.load(HERO, TextureAtlas.class);
	manager.load(ALLY, TextureAtlas.class);
	loadEnemiesAssets(world, manager);
	return manager;
    }

    public static String GHOST_ARMOR_ASSET;
    public static String FIRE_LION_ASSET;
    public static String BAT_ASSET;

    private static void loadEnemiesAssets(WorldEntity world,
	    AssetManager manager) {
	HashSet<EnemyType> types = new HashSet<EnemyType>();
	for (Iterator<EnemyEntity> it = world.enemies.iterator(); it.hasNext();) {
	    EnemyEntity entity = it.next();
	    if (types.add(entity.type)) {
		switch (entity.type) {
		case GHOST_ARMOR:
		    GHOST_ARMOR_ASSET = entity.asset;
		    manager.load(GHOST_ARMOR_ASSET, TextureAtlas.class);
		    break;
		case ELF:
		    break;
		case FIRE_LION:
		    FIRE_LION_ASSET = entity.asset;
		    manager.load(FIRE_LION_ASSET, TextureAtlas.class);
		    break;
		case BAT:
		    BAT_ASSET = entity.asset;
		    manager.load(BAT_ASSET, TextureAtlas.class);
		    break;
		}
	    }
	}

    }

}
