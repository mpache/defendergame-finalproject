/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package service.resources;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

/**
 * ResourcesMenus.java
 * 
 * @version May 26, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class ResourcesMenus {

    // =================
    // ELEMENTS OF MENUS
    // =================
    // --------------------------------------------
    // MAIN MENU
    // --------------------------------------------
    // background and title.
    public static Sprite mainMenuBackground, titleGame;
    // Follow
    public static Sprite followFacebook, followGoogle, followTwitter;
    // Buttons
    public static Sprite buttonSlayerUp, buttonSlayerDown;
    public static Sprite buttonPlayUp, buttonPlayDown;
    public static Sprite buttonOptionsUp, buttonOptionsDown;
    public static Sprite buttonVideosUp, buttonVideosDown;
    public static Sprite buttonInfoUp, buttonInfoDown;
    // Credits
    public static Sprite swipeUp, swipeDown;
    public static Sprite creditsBackground;
    // Back and next
    public static Sprite buttonArrowBackUp, buttonArrowBackDown;
    public static Sprite buttonArrowNextUp, buttonArrowNextDown;
    public static Sprite arrowBackUp, arrowBackDown;
    public static Sprite arrowNextUp, arrowNextDown;
    // Tutorials
    public static Sprite tutSistemGame, tutBasicControls,
	    tutBasicControlsMobile, tutSpecialAttacks, tutHud, tutItems;

    // -------------------------------------------
    // LEVEL MENU
    // -------------------------------------------
    // Level buttons
    public static Sprite buttonLevelUp, buttonLevelDown, buttonLevelBloqued;
    // Star, numbers and slash.
    public static Sprite star, noStar;
    public static Sprite num0, num1, num2, num3, num4, num5, num6, num7, num8,
	    num9, slash;
    // --------------------------------------------
    // CHARACTER MENU
    // -------------------------------------------
    // HEROS
    public static Sprite buttonHero1Up, buttonHero1Down, buttonHero2Up,
	    buttonHero2Down, buttonHero3Up, buttonHero3Down, buttonHero4Up,
	    buttonHero4Down;
    public static Sprite menuHero1, menuHero2, menuHero3, menuHero4;
    public static Sprite descriptionHero1, descriptionHero2, descriptionHero3,
	    descriptionHero4;
    // ALLIES
    public static Sprite buttonAllyAttackUp, buttonAllyAttackDown,
	    buttonAllyNeutralUp, buttonAllyNeutralDown, buttonAllyDefenseUp,
	    buttonAllyDefenseDown;
    public static Sprite menuAllyAttack, menuAllyNeutral, menuAllyDefense;
    public static Sprite descriptionAllyAttack, descriptionAllyNeutral,
	    descriptionAllyDefense;

    // private constructor to avoid instantiate this class.
    private ResourcesMenus() {
    }

    /**
     * Initialized resources.
     * 
     * @param manager
     *            is the asset manager.
     */
    public static void initResources(AssetManager manager) {
	// Create atlas.
	TextureAtlas menuAtlas = manager.get(ResourcesManager.MENU_MENUS);
	// Load resources
	// Image of background.
	mainMenuBackground = menuAtlas.createSprite("menusBackground");
	// Title.
	titleGame = menuAtlas.createSprite("title"); // TESTING
	// Follow
	followFacebook = menuAtlas.createSprite("facebook"); // TESTING
	followGoogle = menuAtlas.createSprite("google+"); // TESTING
	followTwitter = menuAtlas.createSprite("twitter"); // TESTING

	// Buttons
	// Initialized game.
	buttonSlayerUp = menuAtlas.createSprite("slayerButtonUp");
	buttonSlayerDown = menuAtlas.createSprite("slayerButtonDown");
	// Play
	buttonPlayUp = menuAtlas.createSprite("buttonPlayUp");
	buttonPlayDown = menuAtlas.createSprite("buttonPlayDown");
	// Options
	buttonOptionsUp = menuAtlas.createSprite("buttonOptionsUp");
	buttonOptionsDown = menuAtlas.createSprite("buttonOptionsDown");
	// Videos
	buttonVideosUp = menuAtlas.createSprite("buttonVideoUp");
	buttonVideosDown = menuAtlas.createSprite("buttonVideoDown");
	// Info
	buttonInfoUp = menuAtlas.createSprite("buttonInfoUp");
	buttonInfoDown = menuAtlas.createSprite("buttonInfoDown");
	// Credits
	swipeUp = menuAtlas.createSprite("swipeDown");
	swipeDown = menuAtlas.createSprite("swipeUp");
	creditsBackground = menuAtlas.createSprite("credits");
	// Tutorials
	tutSistemGame = menuAtlas.createSprite("sistemGame");
	tutBasicControls = menuAtlas.createSprite("basicControls");
	tutBasicControlsMobile = menuAtlas.createSprite("basicControlsMobile");
	tutSpecialAttacks = menuAtlas.createSprite("specialAttacks");
	tutHud = menuAtlas.createSprite("hud");
	tutItems = menuAtlas.createSprite("items");
	
	// Arrows to next screen or back screen.
	buttonArrowBackUp = menuAtlas.createSprite("buttonArrowNextUp");
	buttonArrowBackUp.flip(true, false);
	buttonArrowBackDown = menuAtlas.createSprite("buttonArrowNextDown");
	buttonArrowBackDown.flip(true, false);
	buttonArrowNextUp = menuAtlas.createSprite("buttonArrowNextUp");
	buttonArrowNextDown = menuAtlas.createSprite("buttonArrowNextDown");
	arrowBackUp = menuAtlas.createSprite("arrowNextUp");
	arrowBackUp.flip(true, false);
	arrowBackDown = menuAtlas.createSprite("arrowNextDown");
	arrowBackDown.flip(true, false);
	arrowNextUp = menuAtlas.createSprite("arrowNextUp");
	arrowNextDown = menuAtlas.createSprite("arrowNextDown");

	// LEVELS
	buttonLevelUp = menuAtlas.createSprite("levelUp");
	buttonLevelDown = menuAtlas.createSprite("levelDown");
	buttonLevelBloqued = menuAtlas.createSprite("levelBloqued");
	star = menuAtlas.createSprite("star");
	noStar = menuAtlas.createSprite("noStar");
	num0 = menuAtlas.createSprite("0");
	num1 = menuAtlas.createSprite("1");
	num2 = menuAtlas.createSprite("2");
	num3 = menuAtlas.createSprite("3");
	num4 = menuAtlas.createSprite("4");
	num5 = menuAtlas.createSprite("5");
	num6 = menuAtlas.createSprite("6");
	num7 = menuAtlas.createSprite("7");
	num8 = menuAtlas.createSprite("8");
	num9 = menuAtlas.createSprite("9");
	slash = menuAtlas.createSprite("slash");

	// CHARACTERS
	// Heros
	buttonHero1Up = menuAtlas.createSprite("buttonHero1Up");
	buttonHero1Down = menuAtlas.createSprite("buttonHero1Down");
	buttonHero2Up = menuAtlas.createSprite("buttonHero2Up");
	buttonHero2Down = menuAtlas.createSprite("buttonHero2Down");
	buttonHero3Up = menuAtlas.createSprite("buttonHero3Up");
	buttonHero3Down = menuAtlas.createSprite("buttonHero3Down");
	buttonHero4Up = menuAtlas.createSprite("buttonHero4Up");
	buttonHero4Down = menuAtlas.createSprite("buttonHero4Down");

	menuHero1 = menuAtlas.createSprite("menuHero1");
	menuHero2 = menuAtlas.createSprite("menuHero2");
	menuHero3 = menuAtlas.createSprite("menuHero3");
	menuHero4 = menuAtlas.createSprite("menuHero4");

	descriptionHero1 = menuAtlas.createSprite("descriptionHero1");
	descriptionHero2 = menuAtlas.createSprite("descriptionHero2");
	descriptionHero3 = menuAtlas.createSprite("descriptionHero3");
	descriptionHero4 = menuAtlas.createSprite("descriptionHero4");

	// Allies
	buttonAllyAttackUp = menuAtlas.createSprite("buttonAllyAttackUp");
	buttonAllyAttackDown = menuAtlas.createSprite("buttonAllyAttackDown");
	buttonAllyNeutralUp = menuAtlas.createSprite("buttonAllyNeutralUp");
	buttonAllyNeutralDown = menuAtlas.createSprite("buttonAllyNeutralDown");
	buttonAllyDefenseUp = menuAtlas.createSprite("buttonAllyDefenseUp");
	buttonAllyDefenseDown = menuAtlas.createSprite("buttonAllyDefenseDown");

	menuAllyAttack = menuAtlas.createSprite("menuAllyAttack");
	menuAllyNeutral = menuAtlas.createSprite("menuAllyNeutral");
	menuAllyDefense = menuAtlas.createSprite("menuAllyDefense");

	descriptionAllyAttack = menuAtlas.createSprite("descriptionAllyAttack");
	descriptionAllyNeutral = menuAtlas
		.createSprite("descriptionAllyNeutral");
	descriptionAllyDefense = menuAtlas
		.createSprite("descriptionAllyDefense");
    }

}
