/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package service;

import com.badlogic.gdx.InputMultiplexer;

/**
 * InputGame.java
 * 
 * @version May 26, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class InputGame {

    private static InputMultiplexer obj;

    private InputGame() {
    }

    /**
     * Singleton pattern.
     * 
     * @return a unique instance of InputMultiplexer.
     */
    public static InputMultiplexer getInputMultiplexer() {
	if (obj == null) {
	    obj = new InputMultiplexer();
	}
	return obj;
    }

    /**
     * Inner class, used to process actions.
     * 
     */
    public class Keys {
	public static final int LEFT = 1;
	public static final int RIGHT = 2;
	public static final int ATTACK = 3;
	public static final int JUMP = 4;
	public static final int SP1 = 5;
	public static final int SP2 = 6;
	public static final int SP3 = 7;
	public static final int PAUSE = 8;
	public static final int CONTINUE = 9;
	public static final int RESTURN = 10;
	public static final int RESET = 11;
	public static final int PAD = 12;
    }

}
