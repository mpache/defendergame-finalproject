/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package service.data;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;

import persistence.DataAccesObject;
import service.data.TypeManager.AllyType;
import service.data.TypeManager.HeroType;

/**
 * SettingManager.java
 * 
 * @version May 26, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class SettingManager {

    // Operation, used to open a url in a different dispositives.
    public static IOperations myOperation;

    // current type and level
    public static HeroType temporalHero;
    public static AllyType temporalAlly;
    public static int temporalWorld;

    /* Constants */
    // isMobile
    public static boolean isMobile;
    // Data Access Object
    public static DataAccesObject DAO;

    // -----------------------------------
    // MUSIC
    // -----------------------------------
    // MENUS
    // Main theme
    public static FileHandle mainTheme;
    // Button Next
    public static FileHandle next;
    // Button Back
    public static FileHandle back;
    // Button select hero or ally
    public static FileHandle select;
    // Button play
    public static FileHandle play;
    // Battle Theme
    public static FileHandle battleTheme;

    // AUDIO & SOUNDS
    private static Music audio;
    private static Sound sound;
    private static boolean isPaused;

    /**
     * Start audio.
     * 
     * @param file
     *            is the file of audio to play.
     */
    public static void startAudio(FileHandle file) {
	if (audio == null && file != null) {
	    audio = Gdx.audio.newMusic(file);
	    audio.setLooping(true);
	    if (!isPaused)
		audio.play();
	}
    }

    /**
     * Pause or resume audio.
     */
    public static void pauseOrResumeAudio() {
	if (audio != null) {
	    if (audio.isPlaying()) {
		isPaused = true;
		audio.pause();
	    } else {
		isPaused = false;
		audio.play();
	    }
	}
    }

    /**
     * Start sound.
     * 
     * @param file
     *            is the file of sound to play.
     */
    public static void startSound(FileHandle file) {
	if (file != null) {
	    if (sound != null)
		sound.dispose();
	    sound = Gdx.audio.newSound(file);
	    if (!isPaused)
		sound.play();
	}
    }

    /**
     * Dispose audio.
     */
    public static void disposeAudio() {
	if (audio != null) {
	    audio.dispose();
	    audio = null;
	}
    }
}
