/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package service.data;

/**
 * States.java
 * 
 * @version May 2, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class StateManager {

    /**
     * Hero states.
     */
    public enum HeroState {
	INACTIVE, AIR, WALK, BEHIND_DAMAGE, FRONT_DAMAGE, WIN, LOSE, DEAD, GR_ATTACK_STANCE, AIR_ATTACK, GR_SP1, AIR_SP1, GR_SP2, GR_SP3, AIR_SP2, AIR_SP3
    }

    /**
     * Ally states.
     */
    public enum AllyState {
	BORN, WALK, FRONT, INACTIVE
    }

    /**
     * Enemy states.
     */
    public enum EnemyState {
	INACTIVE, WALK, ATTACK, KICKED, DEAD
    }

    /**
     * Game states.
     */
    public enum GameState {
	PLAYING, PAUSED, WIN, GAME_OVER
    }

    /**
     * Game scores.
     */
    public enum GameScore {
	BLOCKED, UNBLOCKED, STAR1, STAR2, STAR3
    }

}
