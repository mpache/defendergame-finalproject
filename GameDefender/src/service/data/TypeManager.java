/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package service.data;

/**
 * TypeManager.java
 *
 * @version May 26, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 *
 */
public class TypeManager {

    /**
     * Class of hero types.
     * 
     * <p>
     * {@link WARRIOR}, {@link MAGE}
     * <p>
     */
    public enum HeroType {
	WARRIOR, MAGE
    }

    /**
     * Class of ally types.
     * 
     * <p>
     * {@link ATTACK}, {@link NEUTRAL}, {@link DEFENSE}
     * <p>
     */
    public enum AllyType {
	ATTACK, DEFENSE, NEUTRAL;
    }

    /**
     * Class of enemy types.
     * 
     * <p>
     * {@link MINOTAUR}, {@link FROG}, {@link ELF}, {@link RAVEN}
     * <p>
     */
    public enum EnemyType {
	GHOST_ARMOR, FIRE_LION, ELF, BAT
    }

    /**
     * Class of enemy types.
     * 
     * <p>
     * {@link LIFE}, {@link MAGIC}
     * <p>
     */
    public enum ItemType {
	LIFE, MAGIC
    }

}
