/*
 * Copyright 2013 Adrián López González <alg_18_k@hotmail.com>
 * 		  Julián Suárez alfonso <julian_0141@hotmail.com>
 *
 * This file is part of GameDefender.
 *
 * GameDefender is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GameDefender is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GameDefender.  If not, see <http://www.gnu.org/licenses/>.
 */
package service;

import service.InputGame.Keys;
import service.data.SettingManager;
import application.abstraction.IScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;

/**
 * InputGameLoop.java
 * 
 * @version May 26, 2013
 * @author Adrián López González
 * @author Julián Suárez alfonso
 * 
 */
public class InputGameLoop implements InputProcessor {

    // Variable to check if touch pad in mobile phones is moved.
    private int x;
    private IScreen controller;

    /**
     * InputGameLoop constructor.
     * 
     * @param controller
     *            is a controller.
     */
    public InputGameLoop(IScreen controller) {
	this.controller = controller;
    }

    // ===========
    // CHECK INPUT
    // ===========

    /**
     * Check input.
     */
    public void checkInput() {
	if (SettingManager.isMobile) {
	    this.controlMobileMoved(x);
	} else {
	     this.keyPressed();
	}
    }

    private void keyPressed() {
	// WALK RIGHT
	if (Gdx.input.isKeyPressed(Input.Keys.D)) {
	    controller.processAction(InputGame.Keys.RIGHT);
	}
	// WALK LEFT
	else if (Gdx.input.isKeyPressed(Input.Keys.A)) {
	    controller.processAction(InputGame.Keys.LEFT);
	}
    }

    private void controlMobileMoved(float x) {
	if (Gdx.input.isTouched()) {
	    if (x > 0) {
		controller.processAction(InputGame.Keys.RIGHT);
	    } else if (x < 0) {
		controller.processAction(InputGame.Keys.LEFT);
	    }
	}
    }

    @Override
    public boolean keyDown(int keycode) {
	// JUMP
	if (keycode == Input.Keys.SPACE) {
	    controller.processAction(InputGame.Keys.JUMP);
	    return true;
	}
	return false;
    }

    @Override
    public boolean keyUp(int keycode) {
	return false;
    }

    @Override
    public boolean keyTyped(char character) {
	return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
	// ATTACK IN DESKTOP APPLICATION (button == 1 --> right click mouse)
	if (button == 1) {
	    controller.processAction(Keys.ATTACK);
	}
	// TOUCHPAD
	else if (button == InputGame.Keys.PAD) {
	    this.x = screenX;
	    return true;
	}
	return this.controller.processAction(button);
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
	return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
	return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
	return false;
    }

    @Override
    public boolean scrolled(int amount) {
	return false;
    }

}
