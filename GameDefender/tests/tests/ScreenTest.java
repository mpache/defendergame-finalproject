package tests;

import java.util.Iterator;
import java.util.Set;

import presentation.abstraction.AStage;
import application.abstraction.IScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

import domain.World;
import domain.allies.Ally;
import domain.enemies.Enemy;
import domain.enemies.GroundEnemy;
import domain.heros.Hero;
import domain.items.Item;
import domain.walls.Ground;
import domain.walls.Tower;

public class ScreenTest extends AStage implements IScreen {

    static final int WIDTH = 480;
    static final int HEIGHT = 320;

    private OrthographicCamera camera;
    private ShapeRenderer renderer;
    private World game;
    float time;

    public ScreenTest() {
	renderer = new ShapeRenderer();
	camera = new OrthographicCamera(WIDTH, HEIGHT);
	camera.position.set(WIDTH / 2, HEIGHT / 2, 0);
	game = null;
	// Elements of World to draw
	hero = game.getHero();
	tower = game.getTower();
	grounds = game.getGrounds();
	enemies = game.getEnemies();
	ally = game.getAlly();
	items = game.getItems();
	// input
	Gdx.input.setInputProcessor(new MyInput());
    }

    @Override
    public void update(float delta) {
	checkInput();
	game.heroUpdate(delta);
	game.enemiesUpdate(delta);
	game.allyUpdate(delta);
	game.itemsUpdate(delta);
    }

    Hero hero;
    Tower tower;
    Ally ally;
    private Set<Ground> grounds;
    private Set<Enemy> enemies;
    private Set<Item> items;

    @Override
    public void draw(float delta) {
	// update camera
	camera.update();
	// update camera
	camera.update();
	renderer.setProjectionMatrix(camera.combined);
	// Draw floor
	renderer.begin(ShapeType.Filled);
	renderer.setColor(0.8f, 0.6f, 0.1f, 1);
	renderer.rect(0, 0, 1200, game.getFloor());
	renderer.end();

	// Draw tower
	renderer.begin(ShapeType.Filled);
	renderer.setColor(0, 0, 1, 1);
	renderer.rect(tower.position.x, tower.position.y, tower.getWidth(),
		tower.getHeight());
	renderer.end();

	// draw grounds
	for (Iterator<Ground> it = grounds.iterator(); it.hasNext();) {
	    Ground ground = it.next();
	    renderer.begin(ShapeType.Filled);
	    renderer.setColor(0, 1, 0, 1);
	    renderer.rect(ground.position.x, ground.position.y,
		    ground.getWidth(), ground.getHeight());
	    renderer.end();
	}

	// Draw Enemies
	for (Iterator<Enemy> it = enemies.iterator(); it.hasNext();) {
	    Enemy enemy = it.next();
	    renderer.begin(ShapeType.Filled);
	    if (enemy instanceof GroundEnemy) {
		renderer.setColor(0, 0, 0, 1);
	    } else {
		renderer.setColor(0.3f, 0.3f, 0.3f, 1);
	    }
	    renderer.rect(enemy.position.x, enemy.position.y, enemy.getWidth(),
		    enemy.getHeight());
	    renderer.end();
	    // Lines
	    renderer.begin(ShapeType.Line);
	    renderer.setColor(1, 0, 0, 1);
	    renderer.rect(enemy.position.x, enemy.position.y, enemy.getWidth(),
		    enemy.getHeight());
	    renderer.end();
	}

	// draw ally
	renderer.begin(ShapeType.Filled);
	renderer.setColor(1, 1, 0, 1);
	renderer.rect(ally.position.x, ally.position.y, ally.getWidth(),
		ally.getHeight());
	renderer.end();

	// Draw Items
	for (Iterator<Item> it = items.iterator(); it.hasNext();) {
	    Item item = it.next();
	    renderer.begin(ShapeType.Filled);
	    renderer.setColor(0.8f, 0, 1.6f, 1);
	    renderer.circle(item.getMiddleX(), item.getMiddleY(),
		    item.getWidth() / 2);
	    renderer.end();
	}

	// draw hero
	renderer.begin(ShapeType.Filled);
	renderer.setColor(1, 0, 0, 1);
	renderer.rect(hero.position.x, hero.position.y, hero.getWidth(),
		hero.getHeight());
	renderer.end();

	// Camera Limits
	if (hero.position.x > WIDTH / 2 && hero.position.x < 1200 - WIDTH / 2) {
	    camera.position.x = hero.position.x;
	}

    }

    @Override
    public IScreen nextScreen() {
	return null;
    }

    @Override
    public void resize(float width, float height) {
	super.resize(width, height);
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
    }

    // Temporal Method , method for testing
    public void checkInput() {

	if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
	    hero.jump(Hero.JUMP_VEL);
	}

	if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
	    hero.moveRight(Hero.WALK_VEL);
	}
	if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
	    hero.moveLeft(Hero.WALK_VEL);
	}
	if (Gdx.input.isTouched()) {
	    int width = Gdx.graphics.getWidth();
	    int x = Gdx.input.getX();
	    if (x < width / 4) {
		hero.moveLeft(Hero.WALK_VEL);
	    }
	    if (x > width - width / 4) {
		hero.moveRight(Hero.WALK_VEL);
	    }
	}
    }

    public class MyInput implements InputProcessor {

	@Override
	public boolean keyDown(int keycode) {
	    // TODO Auto-generated method stub
	    return false;
	}

	@Override
	public boolean keyUp(int keycode) {
	    // TODO Auto-generated method stub
	    return false;
	}

	@Override
	public boolean keyTyped(char character) {
	    // TODO Auto-generated method stub
	    return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer,
		int button) {
	    int width = Gdx.graphics.getWidth();

	    if (screenX >= width / 4 && screenX <= width - width / 4) {
		hero.jump(Hero.JUMP_VEL);
	    }
	    return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
	    // TODO Auto-generated method stub
	    return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
	    // TODO Auto-generated method stub
	    return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
	    // TODO Auto-generated method stub
	    return false;
	}

	@Override
	public boolean scrolled(int amount) {
	    // TODO Auto-generated method stub
	    return false;
	}
    }

    @Override
    public boolean processAction(int action) {
	return false;
    }

    @Override
    public boolean onBackPressed() {
	// TODO Auto-generated method stub
	return false;
    }

    @Override
    protected void registerListeners() {
	// TODO Auto-generated method stub
	
    }
}
